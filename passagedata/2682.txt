<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<if $phase is 0>>
	Sirris' concern is clearly not assuaged, but <<he>> nods and moves on.
	<br><br>
	<<link [[Next|Science Lesson]]>><<endevent>><</link>>
<<elseif $phase is 1>>
	Despair overtakes you and you burst into tears. Sirris says something, but you can't make it out. You lose consciousness.
	<br><br>
	<<endevent>>
	<<ambulance>>
<<elseif $phase is 2>>
	Sirris spends a couple of minutes helping you fill some holes in your understanding. When it's over, you feel like you understand the material much better.
	<br><br>
	<<link [[Next|Science Lesson]]>><<endevent>><</link>>
<<elseif $phase is 3>>
	Sirris nods and moves on.
	<br><br>
	<<link [[Next|Science Lesson]]>><<endevent>><</link>>
<</if>>