<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $schoolstate is "fourth">>
	<<if $worn.upper.type.includes("naked") and $worn.lower.type.includes("naked") and $rightarm isnot "bound">>
		/*Check for when the pc is only wearing and overcoat*/
		<<if $worn.under_upper.name is "naked" and $worn.under_lower.name is "naked">>
			Wearing nothing underneath over slot clothing comment
		<<elseif $worn.under_upper.name is "naked">>
			Wearing only under_lower under over slot clothing comment
		<<elseif $worn.under_lower.name is "naked">>
			Wearing only under_upper under over slot clothing comment
		<</if>>
		<br><br>
		<<link [[Next|Hallways]]>><<endevent>><</link>>
		<br><br>
	<<elseif $worn.upper.type.includes("school") and $worn.lower.type.includes("school") and $rightarm isnot "bound">>
		<<if $historyattended is 1>>
			You rejoin the history lesson,
			<<if $worn.over_upper.name isnot "naked" or $worn.over_lower.name isnot "naked" or $worn.over_head.name isnot "naked">>
				hang your coat at the back of the class
				<<undressOverClothes "historyClassroom">>
			<</if>>
			and take your seat.
			<br><br>
			<<link [[Next|History Lesson]]>><<endevent>><</link>>
			<br>
		<<elseif $time lte ($hour * 60 + 5)>>
			You enter the history classroom.
			<<npc Winter>><<person1>>
			Winter is preparing at the front of the room while the seats fill.
			<<if $worn.over_upper.name isnot "naked" or $worn.over_lower.name isnot "naked" or $worn.over_head.name isnot "naked">>
				<<undressOverClothes "historyClassroom">>
				You hang your coat at the back of the class and take your seat.
			<</if>>
			<br><br>
			<<link [[Next|History Lesson]]>><<endevent>><</link>>
			<br>
		<<else>>
			You enter the history classroom.
			<<npc Winter>><<person1>>
			Winter cuts off mid-sentence and looks at you. <<He>> taps <<his>> watch and points to your seat.
			<<gdelinquency>>
			<br><br>
			<<detention 1>>
			You
			<<if $worn.over_upper.name isnot "naked" or $worn.over_lower.name isnot "naked" or $worn.over_head.name isnot "naked">>
				hang your coat at the back of the class and
				<<undressOverClothes "historyClassroom">>
			<</if>>
			take your seat as Winter continues.
			<br><br>

			<<link [[Next|History Lesson]]>><<endevent>><</link>>
			<br>
		<</if>>

	<<elseif $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
		Winter looks at your bindings. "Whatever that's about you need to sort it out before you enter my class. Go see the head."
		<br><br>

		<<link [[Next|Hallways]]>><<endevent>><</link>>
		<br><br>
	<<else>>
		<<npc Winter>><<person1>>
		Winter looks at your clothing. "You can't have believed I wouldn't notice. Go see the head."
		<br><br>

		<<link [[Next|Hallways]]>><<endevent>><</link>>
		<br><br>
	<</if>>
<<elseif $schoolstate is "lunch">>
	You enter the history classroom. No one else has arrived yet. You could use the extra time to study.
	<br><br>

	<<schoolperiodtext>>

	<<link [[Study history|History Classroom Study]]>><</link>><<gstress>><<ghistory>>
	<br>
	<<link [[Leave|Hallways]]>><</link>>
	<br>
<<elseif $schoollesson is 1>>
	<<if $historyinterrupted is 1>>
		<<npc Winter>><<person1>>You enter the history classroom. Winter stops speaking abruptly and looks your way. "Every time you interrupt me you damage your fellow student's chances. I'll mark you down for detention." <<He>> guides you to the door.
		<<gdelinquency>>
		<br><br>
		<<detention 1>>

		<<link [[Next|Hallways]]>><<endevent>><</link>>
	<<else>>
		<<npc Winter>><<person1>>You enter the history classroom. Winter stops speaking abruptly and looks your way. "And what can I do for you?" It seems you've interrupted the lesson.
		<br><br>
		<<set $historyinterrupted to 1>>

		<<link [[Apologise|History Classroom Apology]]>><<trauma 1>><<stress 1>><</link>><<gstress>><<gtrauma>>
		<br>
		<<if $trauma gte 1>>
			<<link [[Mock|History Classroom Mock]]>><<status 1>><<stress -12>><</link>><<lstress>><<gcool>><<gdelinquency>>
			<br>
		<</if>>
	<</if>>
<<else>>
	You are in the History classroom. It has a stuffy atmosphere.
	<br><br>
	<<storeon "historyClassroom" "check">>
	<<if _store_check is 1>>
		<<storeon "historyClassroom">>
		You take your coat at the back of the class.
		<br><br>
	<</if>>
	<<exhibitionclassroom>>
	<<link [[Leave the room (0:01)|Hallways]]>><<pass 1>><</link>>
	<br>
<</if>>