<<widget "PregEventsResult">><<nobr>>
<<set $outside to 0>><<set $location to $container.lastLocation>><<effects>>
<<set _pregnancy to $sexStats.anus.pregnancy>>
<<set _container to $container[$location]>>
<<set $parasiteSelectionCount to 0>>
<<set $parasiteReplaceArray to []>>
<<set _avaliableStorage to (_container.maxCount - _container.count)>>
<<for _i to 0; _i lt $pregResult.length; _i++>>
	<<if $checkboxResult[$pregResult[_i]] is true>>
		<<set $parasiteSelectionCount += 1>>
	<</if>>
<</for>>
<<for _i to 0; _i lt _container.count; _i++>>
	<<if $checkboxReplace[_i] is true>>
		<<set $parasiteReplaceArray.push(clone(_i))>>
	<</if>>
<</for>>

<<if $debug is 1>>
	Debug - Keep selected babies result (ID - Checkbox - Result)
	<br>
	<<for _i to 0; _i lt $pregResult.length;_i++>>
		<<if $checkboxResult[$pregResult[_i]] is true>>
			<<print $pregResult[_i]>> - <<print $checkboxResult[$pregResult[_i]]>> - Keep
			<br>
		<<elseif $checkboxResult[$pregResult[_i]] is false>>
			<<print $pregResult[_i]>> - <<print $checkboxResult[$pregResult[_i]]>> - Sell
			<br>
		<</if>>
	<</for>>
	<br>
<</if>>

<<if $parasiteSelectionCount lte _avaliableStorage>>
	<<if $parasiteSelectionCount gt 0>>
		<<if _pregnancy.namesChildren is true>>
			<<link [[Keep selected babies|PregEventsResult2]]>>
				<<set $pregChoice to "KeepSelected">>
			<</link>>
		<<else>>
			<<link [[Keep selected parasites|PregEventsResult2]]>>
				<<set $pregChoice to "KeepSelected">>
			<</link>>
		<</if>>
	<</if>>
<<elseif $parasiteSelectionCount gt 0>>
	Not enough storage for selected amount. Please replace <<print $parasiteSelectionCount - _avaliableStorage>> <<if _pregnancy.namesChildren is true>><<print $parasiteSelectionCount - _avaliableStorage gt 1 ? 'babies' : 'baby'>><<else>><<print $parasiteSelectionCount - _avaliableStorage gt 1 ? 'parasites' : 'parasite'>><</if>>.
	<br>
	<<if ($parasiteSelectionCount - $parasiteReplaceArray.length) is _avaliableStorage>>
		<<if _pregnancy.namesChildren is true>>
			<<link [[Replace selected babies|PregEventsResult2]]>>
				<<set $pregChoice to "ReplaceSelected">>
			<</link>>
		<<else>>
			<<link [[Replace selected parasites|PregEventsResult2]]>>
				<<set $pregChoice to "ReplaceSelected">>
			<</link>>
		<</if>>
	<</if>>
<</if>>
<<if $pregResult.length lte _avaliableStorage>>
	<br>
	<<if _pregnancy.namesChildren is true>>
		<<link [[Keep all babies|PregEventsResult2]]>>
			<<set $pregChoice to "keepAll">>
		<</link>>
	<<else>>
		<<link [[Keep all parasites|PregEventsResult2]]>>
			<<set $pregChoice to "keepAll">>
		<</link>>
	<</if>>
<</if>>

<br><br>
<<if _pregnancy.namesChildren is true>>
	<<link [[Sell all babies|PregEventsResult2]]>>
		<<set $pregChoice to "sellAll">>
	<</link>>
<<else>>
	<<link [[Sell all parasites|PregEventsResult2]]>>
		<<set $pregChoice to "sellAll">>
	<</link>>
<</if>>
<</nobr>><</widget>>

<<widget "resetPregButtons">><<nobr>>

<<replace #pregResult>>
	<<PregEventsResult>>
<</replace>>

<</nobr>><</widget>>