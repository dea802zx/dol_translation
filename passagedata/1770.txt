<<effects>>
<<if $enemywounded is 1 and $enemyejaculated is 0>>
	The dog whimpers and flees into the alleyways.
<<elseif $enemywounded is 0 and $enemyejaculated is 1>>
	The dog leaves you lying on the pavement.
<<elseif $enemywounded gte 2 and $enemyejaculated is 0>>
	Feeling that you're more trouble than you're worth, the dogs flee into the alleyways.
<<elseif $enemywounded is 0 and $enemyejaculated gte 2>>
	The dogs leave you spent and shivering on the pavement.
<<elseif $enemywounded gte 1 and $enemyejaculated gte 1>>
	The dogs leave you spent and shivering on the pavement. One is slowed by a limp.
<</if>>
<br><br>
<<tearful>> you gather yourself.
<br><br>
<<clotheson>>
<<endcombat>>
<<set $stress -= 1000>>
<<residentialquick>>