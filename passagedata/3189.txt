<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "oxford">>

You hear heavy trucks rumbling nearby and smell acrid chemicals.
<br><br>

<<if $exposed gte 1>>
	<<covered>>
<</if>>
<br><br>

<<if $stress gte 10000>><<set $phase to 0>>
	<<passoutstreet>>
<<elseif !$worn.face.type.includes("blindfold")>>
	<span class="green">The tattered blindfold falls to pieces around your head, freeing you.</span> You squint against the light.
	<<ltrauma>><<lllstress>><<trauma -6>><<stress -24>>
	<br><br>
	<<destinationeventend>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure) and $eventskip isnot 1 or $phase is 1>><<set $phase to 0>>
		<<eventsbondageeast>>
	<<else>>
		<<link [[Run towards the sound of machinery (0:05)|Bondage Run]]>><<set $bus to "industrial">><<pass 5>><<stress 3>><</link>><<gstress>>
		<br>
		<<link [[Run towards the sound of the sea (0:05)|Bondage Run]]>><<set $bus to "starfish">><<pass 5>><<stress 3>><</link>><<gstress>>
		<br>
		<<link [[Run towards the sound of traffic (0:05)|Bondage Run]]>><<set $bus to "park">><<pass 5>><<stress 3>><</link>><<gstress>>
		<br>
		<<link [[Run away from the sounds of the town (0:05)|Bondage Run]]>><<set $bus to "nightingale">><<pass 5>><<stress 3>><</link>><<gstress>>
		<br>
		<<link [[Hide (0:05)|Bondage Hide]]>><<pass 5>><<stress 3>><</link>><<gstress>>
		<br>
	<</if>>
<</if>>

<<set $eventskip to 0>>