<<set $outside to 1>><<set $location to "forest">><<effects>>
"A giant snake tried to eat me when I found the picnic basket, but I fought it off," you say, hiding your terror beneath bravado.
<br><br>
Robin clings to your arm and gazes on you with awe. "You fought off a giant snake?" <<he>> says. "And you rescued my basket. You're so amazing."
<br><br>
<<if $exposed gte 1>>
	Robin gives you the rug to cover with and together you return to the orphanage.
	<br><br>
	<<link [[Next|Bedroom]]>><<endevent>><</link>>
	<br>
<<else>>
	Together you return to the orphanage.
	<br><br>
	<<robinoptions>>
<</if>>