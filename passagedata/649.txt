<<set $outside to 1>><<set $location to "forest">><<effects>>

You come to a small cave and see a large shape rise and fall in a steady rhythm. It's a bear, lying with its eyes shut beside a fresh kill. It looks like a deer. The pack creeps closer. They'll need the element of surprise.
<br><br>

	<<set $skulduggerydifficulty to 300>>
<<link [[Distract it|Wolf Bear Distract]]>><</link>><<skulduggerydifficulty>>
<br>
<<link [[Attack|Wolf Bear Attack]]>><</link>>
<br>