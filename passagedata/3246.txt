<<effects>>
<<fameexhibitionism 4>>
You take a deep breath. <<flaunting>> you march up the stairs. <<covered>><<promiscuity5>>
You find four people stood at the top. You seize their attention, stunning them for a moment. "See?" a <<person1>><<person>> says, the first to recover. "Told you so."
<<if $rng gte 71>>
	<<He>> steps closer, and beckons <<his>> friends follow. They form a circle around you, penning you in. "Course, we aren't paying. I trust that won't be a problem?"
	<br><br>
	<<link [[Next|Street Ex Gang Rape]]>><<set $molestationstart to 1>><</link>>
	<br>
<<else>>
	<<He>> steps closer. "How much for the four of us?"
	<br><br>
	<<link [[£50|Street Ex Solicit Seduce]]>><<set $phase to 0>><</link>>
	<br>
	<<link [[£100|Street Ex Solicit Seduce]]>><<set $phase to 1>><</link>>
	<br>
	<<link [[£200|Street Ex Solicit Seduce]]>><<set $phase to 2>><</link>>
	<br>
	<<link [[£400|Street Ex Solicit Seduce]]>><<set $phase to 3>><</link>>
	<br><br>
<</if>>