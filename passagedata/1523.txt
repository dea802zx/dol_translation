<<set $outside to 0>><<set $location to "brothel">><<effects>>
<<set $brothel_thief to 0>>
You walk out <span class="red">into the waiting arms of two police officers.</span>
Briar stands beside them.
<br><br>
The police push you against the wall, pinning your arms. "They wanted to raid me again," Briar explains. "Looking for you. They really want you this time."
<br><br>
Armlocked, the police drag you deeper into the brothel. Briar strolls alongside. "Raids cost a lot," Briar goes on. "Not just money either. Reputation. I was still ready to protect you. I look out for mine. I was ready to take the hit."
<br><br>
"Until," Briar growls. "I heard you've been stealing from my customers. Hurting the reputation of my establishment. Driving away my custom."
<br><br>
Briar throws open a secluded delivery door in the rear, well out of sight of customers. <<He>> Leads the police out. An unmarked car awaits you.
<br><br>
"There's a saying about the wisdom of shitting where you eat," Briar says. The police throw you over the bonnet.
<br><br>
<<generate1>><<generate2>>
<<link [[Next|Hospital Arrest Journey]]>><</link>>