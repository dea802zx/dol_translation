<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

Leighton continues working down the line, leaving a trail of <<person1>>ashamed boys in <<his>> wake. "Don't tell anyone about this," <<he>> says
as the last boy finishes covering himself back up. <<He>> taps the camera. "It's our secret."
<br><br>

Sirris enters the room. "Just in time," Leighton says. "Everyone, take your seats. Thank you Sirris, you have a very well-endowed," <<he>> coughs.
"Sorry. Well-behaved class."
<br><br>

<<He>> leaves Sirris to drag <<endevent>><<npc Sirris>><<person1>><<his>> desk back into position. The girls chat, still excited, while the
boys sit quietly and avoid looking at each other.
<br><br>

<<endevent>>
<br><br>
<<link [[Next|Science Lesson]]>><</link>>
<br>