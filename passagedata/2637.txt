<<effects>>

You push and pull away from Whitney. <<He>> doesn't relent, and you're forced to shove <<himstop>> "Suit yourself," <<he>> says. "Slut."
<br><br>

A few minutes later River emerges from the class. "It seems I made a mistake. Come back in." You enter the class, but Whitney finds <<himself>> blocked by River's arm. "You can stay out."
<br><br>

Whitney glares at River as the door shuts behind you.
<br><br>

<<set $whitneymaths to "sent">>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
<br>