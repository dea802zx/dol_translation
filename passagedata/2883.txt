<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You settle under the desk and set to work on the lock.
<br><br>

<<skulduggerycheck>>
<br><br>
<<if $skulduggerysuccess is 1>><<set $hacker_tasks.pushUnique("leighton")>>
	It takes some time and is far more complex than you expected. You are just about to give up when the door suddenly pops loose.
	You open the drawer and find a number of cameras and lenses as well as a box full of memory cards. Quite a collection.
	<br>
	You scoop the memory cards into your bag.
	<br><br>

		<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
		<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
		<</if>>

	<<if $hacker_tasks.length gte 2>>
	<span class="green">You have completed the hacker's tasks - you now need to talk to Landry.</span>
	<br><br>
	<</if>>
	<<link [[Next|Head's Office]]>><<endevent>><</link>>
	<br>

<<else>>
	It is far more complex than you expected. After nearly half an hour of fruitless poking and prodding at the lock, you give up.
	<span class="teal">Now you have a feel for the lock, you could try again.</span>
	<br><br>

		<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
		<<skulduggeryskilluse>>
		<<else>>
		<span class="blue">That was too easy. You didn't learn anything.</span>
		<br><br>
		<</if>>

	<<set $skulduggerydifficulty -= 200>>
	<<link [[Try again (0:30)|Head's Stash 2]]>><<pass 30>><</link>><<skulduggerydifficulty>>
	<br>
	<<link [[Give up|Head's Office]]>><<endevent>><</link>>
	<br>
<</if>>