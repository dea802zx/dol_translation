<<effects>>

<<if $danceskill gte random(1, 1200)>>
	You jump aside at the last moment, and <span class="green">the creature flies past.</span> It lands and darts into a hedge before you can get a good look at it.
	<br><br>
	
	<<link [[Next|Farm Work]]>><</link>>
	<br>
<<else>>
	You try to jump aside, <span class="red">but it's on you too fast,</span> latching onto your face. You can't see. You try to pull the creature off, but you struggle to get a grip on its slimy skin. You keep your lips sealed as something presses against your mouth.
	<br><br>
	
	<<link [[Next|Farm Tending Struggle]]>><<set $struggle_start to 1>><</link>>
	<br>
<</if>>