<<effects>>

<<if $NPCName[$NPCNameList.indexOf("Eden")].love gte 100>><<set $edenfreedom to 2>>

<<npc Eden>><<person1>>"You already get a whole day," <<he>> says. "You want to sleep in someone else's bed, is that it?"
<br><br>

	<<if $submissive gte 1150>>
	"N-no," you say. "You know I'm yours alone."
	<<elseif $submissive lte 850>>
	"I'm not a slut," you say. "But sometimes I need to be away for longer."
	<br><br>
	<<else>>
	"No!" you say. "It's just that braving the woods every day for school is dangerous."
	<</if>>
	<br><br>

<<He>> looks about to argue, but sighs in resignation. "Fine. I'm used to being alone. <span class="gold">You can leave for up to a week.</span> If you're gone longer, I'll hunt you down."
<br><br>

<<link [[Hug|Eden Freedom 2]]>><<set $phase to 2>><<npcincr Eden love 1>><</link>>
<br>
	<<if $bus is "edenclearing">>
	<<link [[Nod|Eden Clearing]]>><<endevent>><</link>>
	<br>
	<<else>>
	<<link [[Nod|Eden Cabin]]>><<endevent>><</link>>
	<br>
	<</if>>

<<else>>

<<npc Eden>><<person1>>"You already get a whole day," <<he>> says. "You want to sleep in someone else's bed, is that it?"
<br><br>

	<<if $submissive gte 1150>>
	"N-no," you say. "You know I'm yours alone."
	<<elseif $submissive lte 850>>
	"I'm not a slut," you say. "But sometimes I need to be away for longer."
	<br><br>
	<<else>>
	"No!" you say. "It's just that braving the woods every day for school is dangerous."
	<</if>>
	<br><br>

<<He>> shakes <<his>> head. "That town is dangerous. The less time you spend there the better."
<br><br>

<i>If <<he>> liked you more <<he>> might be more amenable to your request.</i>
<br><br>

	<<if $bus is "edenclearing">>
	<<link [[Next|Eden Clearing]]>><<endevent>><</link>>
	<br>
	<<else>>
	<<link [[Next|Eden Cabin]]>><<endevent>><</link>>
	<br>
	<</if>>
<</if>>