<<set $outside to 1>><<set $location to "town">><<effects>>

You leave the orphanage with Robin and walk to school.

<<if $robinromance is 1>>
<<He>> rubs the back of <<his>> hand against yours. You hold it. You chat with <<him>> as you walk.
<<ltrauma>><<lstress>><<trauma -2>><<stress -4>>
<br><br>

<<else>>
	<<if $NPCName[$NPCNameList.indexOf("Robin")].trauma gte 80>>
	<<Hes>> limping. You move slowly to let <<him>> keep up. <<He>> doesn't seem keen on talking, and stays close to you when other people are near.
	<br><br>

	<<elseif $NPCName[$NPCNameList.indexOf("Robin")].trauma gte 40>>
	You chat with <<him>> on the way, though <<he>> falls silent when there are other people near.
	<br><br>

	<<else>>
	You chat with <<him>> on the way.
	<<ltrauma>><<lstress>><<trauma -2>><<stress -4>>
	<br><br>

	<</if>>
<</if>>
<<endevent>>

<<if $NPCName[$NPCNameList.indexOf("Avery")].love gte random(20, 100) and $NPCName[$NPCNameList.indexOf("Avery")].init is 1 and $robinaverybeat isnot 1>>
<<npc Avery>><<person1>>
	<<if $NPCName[$NPCNameList.indexOf("Avery")].rage gte 20 and $averyragerevealed is 1>>
	A car pulls up beside you. Robin tenses as the door opens, revealing a <<if $pronoun is "m">>man in a suit<<else>>woman in a smart dress<</if>>. It's Avery. <<He>> scowls at you as <<he>> climbs out. <<He>> grasps your arm. "Get in. Now." <<He>> glances at Robin as <<he>> tugs you towards the waiting vehicle. "Your friend can walk."
	<br><br>

		<<if $NPCName[$NPCNameList.indexOf("Robin")].dom gte 91>><<set $robinaverybeat to 1>>
		You stumble forward, but then Avery looks over your shoulder. <<His>> rage falters a moment, and a school bag hurtles into <<his>> face. With a cry, <<he>> releases your arm and falls back against <<his>> car. Robin steps forward, retrieves <<endevent>><<npc Robin>><<person1>><<his>> bag, and brings it down on Avery's head. The weighty books inside make a solid impact. "Leave <<phim>> alone!" Robin shouts. "<<pShes>> mine!"
		<br><br>

		<<endevent>><<npc Avery>><<person1>>

		Avery holds <<his>> arms up for protection and scrambles into <<his>> vehicle. <<He>> glares at Robin. "You'll regret that. I promise." <<His>> car screeches away.
		<br><br>

		<<endevent>><<npc Robin>><<person1>>

		Robin wraps <<his>> arms around you and squeezes. <<Hes>> trembling. <<He>> doesn't stop squeezing until Avery is out of sight.
		<br><br>

		<<link [[Thank Robin|Avery Walk Thank]]>><<npcincr Robin love 3>><</link>><<gglove>>
		<br>
		<<link [[Tease Robin|Avery Walk Tease]]>><<npcincr Robin love 1>><<npcincr Robin lust 1>><</link>><<glove>><<glust>>
		<br>

		<<else>>
		<<endevent>><<npc Robin>><<person1>>
		Robin looks at <<his>> feet.
		<br><br>
		<<endevent>><<npc Avery>><<person1>>

		<<link [[Comply|Avery Walk Comply]]>><<npcincr Avery rage -5>><</link>><<larage>>
		<br>
		<<link [[Refuse|Avery Walk Refuse 2]]>><<npcincr Avery rage 5>><</link>><<garage>>
		<br>

		<</if>>

	<<else>>
		<<if $averywalkintro is 1>>
	A car pulls up beside you. Robin tenses as the window winds down, revealing a <<if $pronoun is "m">>man in a suit<<else>>woman in a smart dress<</if>>. It's Avery. "I'll give you a lift," <<he>> says. "Get in."
	<br><br>

	<<endevent>><<npc Robin>><<person1>>
	Robin frowns, but waits for your response. <<He>> looks nervous.
	<br><br>
	<<endevent>><<npc Avery>><<person1>>

	<<link [[Reassure Robin|Avery Walk School]]>><</link>>
	<br>
	<<link [[Refuse|Avery Walk Refuse]]>><<npcincr Avery rage 1>><</link>><<garage>>
	<br>
		<<else>>
	A car pulls up beside you. Robin tenses as the window winds down, revealing a <<if $pronoun is "m">>man in a suit<<else>>woman in a smart dress<</if>>. It's Avery. "I'm on my way to work," <<he>> says. "I thought I'd give you a lift." <<He>> glances at Robin. "Your friend can come too."
	<br><br>

	<<endevent>><<npc Robin>><<person1>>
	Robin frowns, but waits for your response. <<He>> looks nervous.
	<br><br>
	<<endevent>><<npc Avery>><<person1>>

	<<link [[Reassure Robin|Avery Walk School]]>><</link>>
	<br>
	<<link [[Refuse|Avery Walk Refuse]]>><<npcincr Avery rage 1>><</link>><<garage>>
	<br>
		<</if>>
	<</if>>
<<set $averywalkintro to 1>>
<<else>>
<<link [[Next|School Front Playground]]>><</link>>
<br>
<</if>>