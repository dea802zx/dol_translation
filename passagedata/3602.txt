<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
You are in the main hall of the temple. Stone walls and pillars tower into darkness. Rows of seats face an altar at the end of the room.
<<if $daystate isnot "night" and $daystate isnot "dusk">>
	A figure wearing a white cowl kneels before the altar.
<</if>>
<<if $weekday is 1 and $hour is 12 and $massattended isnot 1>>
	The seats are filled with worshippers, here the for Sunday mass.
<</if>>
<<if $exposed gte 1>>
	You hide behind one of the pillars to conceal your <<nuditystop>>
<</if>>
<br><br>
<<if $stress gte $stressmax>>
	<<passouttemple>>
<<elseif $temple_chastity_timer lte 0 and $temple_rank isnot undefined and $temple_rank isnot "prospective" and $exposed lte 0>><<set $temple_chastity_timer to 30>>
	<<npc Jordan>><<person1>>
	<<if $daystate isnot "night" and $daystate isnot "dusk">>
		The figure turns. It's Jordan. "My child," <<he>> says. "It is time for your chastity examination.
	<<else>>
		"My child," says a voice from the side. It's Jordan. "It is time for you chastity examination.
	<</if>>
	We must make sure you're still pure. You will be purified if you are not."
	<br><br>

	<<link [[Allow examination|Temple Test]]>><<set $phase to 2>><</link>>
	<br>
	<<link [[Refuse examination|Temple Test Refuse]]>><</link>>
	<br>
	<<link [[Say you are impure|Temple Test Admission]]>><</link>>
	<br>

<<elseif $temple_event is 1 and $daystate isnot "night" and $exposed lte 0>><<set $temple_event to 0>>
	<<eventstemple>>
<<else>>
	<<if $scienceproject is "ongoing" and $sciencelichentemple is 0 and $exposed lte 0>>
		<<link [[Search for lichen (0:30)|Temple Lichen]]>><<pass 30>><<set $sciencelichentemple to 1>><</link>>
		<br>
	<</if>>
	<<if $daystate isnot "night" and $daystate isnot "dusk" and $exposed is 0>>
		<<link [[Approach the figure|Temple Jordan]]>><</link>>
		<br>
	<</if>>

	<<if $weekday is 1 and $hour gte 11 and $hour lte 12 and $massattended isnot 1 and $exposed lte 0 and $NPCName[$NPCNameList.indexOf("Jordan")].init is 1>>
		<<if $angel gte 6>>
			<<link [[Attend mass (1:00)|Temple Mass Angel]]>><<purity 10>><<pass 60>><<trauma -18>><<stress -18>><<set $drunk += 10>><<set $massattended to 1>><</link>> | <span class="green">+ Purity</span><<lltrauma>><<llstress>>
			<br>
		<<elseif $fallenangel gte 2>>
			<<link [[Attend mass (1:00)|Temple Mass Fallen Angel]]>><<purity 70>><<pass 60>><<trauma 6>><<stress 6>><<pain 6>><<set $drunk += 10>><<set $massattended to 1>><</link>> | <span class="green">+ + + Purity</span><<trauma>><<gstress>><<gpain>>
			<br>
		<<elseif $demon gte 6>>
			<<link [[Attend mass (1:00)|Temple Mass Demon]]>><<purity 10>><<pass 60>><<trauma 6>><<stress 6>><<pain 6>><<set $drunk += 10>><<set $massattended to 1>><</link>> | <span class="green">+ Purity</span><<trauma>><<gstress>><<gpain>>
			<br>
		<<else>>
			<<link [[Attend mass (1:00)|Temple Mass]]>><<purity 10>><<pass 60>><<set $drunk += 10>><<set $massattended to 1>><</link>> | <span class="green">+ Purity</span>
			<br>
		<</if>>
	<<elseif $exposed lte 0>>
		<<link [[Pray (1:00)|Temple Pray]]>><<set $eventskip to 1>><<set $phase to 1>><</link>>
		<br>
	<</if>>
	<<if $exhibitionism gte 55 and $angel lte 0>>
		<<link [[Masturbate (0:20)|Temple Masturbation]]>><<pass 20>><<set $masturbationstart to 1>><</link>><<llpurity>>
		<br>
	<</if>>

	<<if $temple_rank isnot undefined and $temple_rank isnot "prospective">>
	<br>
		<<if $prayer_room_init is 1>>
			<<link [[Garden|Temple Garden]]>><</link>>
			<br>
			<<link [[Quarters|Temple Quarters]]>><</link>>
			<br>
			<<link [[Prayer Room|Temple Prayer Entrance]]>><</link>>
			<br>
		<<else>>
			<<link [[Explore Temple (0:30)|Temple Prayer Explore]]>><<pass 30>><<set $prayer_room_init to 1>><</link>>
		<</if>>
		<br>

	<</if>>
	<br>
	<<link [[Leave (0:01)|Wolf Street]]>><<pass 1>><</link>>
	<br>
<</if>>
<<set $eventskip to 0>>