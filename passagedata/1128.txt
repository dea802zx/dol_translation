<<effects>>
	
You take advantage of your initiative, and spring forward into a charge. The <<person1>><<person>> tumbles backwards before you even touch <<him>>, and the others turn and run.
<br><br>
	
<<tearful>> you catch your breath.
<br><br>
<<clotheson>>
<<endcombat>>
<<link [[Next|Livestock Field]]>><</link>>
<br>