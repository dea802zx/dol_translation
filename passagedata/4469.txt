<<set $outside to 0>><<set $location to "Underground">><<effects>>

<<danceeffects>>
<<danceaudience>>

<<if $danceevent is 0 and $exhibitionism lte 74 and $exposed gte 2>>
Being so exposed is humiliating, but there's nothing you can do.
<br><br>
<<elseif $danceevent is 0 and $exhibitionism lte 34 and $exposed gte 1>>
Being so exposed is humiliating, but there's nothing you can do.
<br><br>
<</if>>

<<danceactions>>

<<if $danceevent is "finish">>
<<elseif $danceevent is "private">>
<<link [[Next|Underground Dance Oral]]>><<set $enemyno to 1>><<set $molestationstart to 1>><</link>>
<br>
<<elseif $danceevent is "rape">>
<<link [[Next|Underground Dance Rape]]>><<set $molestationstart to 1>><</link>>
<</if>>