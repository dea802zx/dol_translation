<<set $outside to 0>><<set $location to "home">><<effects>>
<<if $robinhugging is 1>>
	You keep your arms wrapped around Robin and watch <<him>> play.
	<br><br>
<<else>>
	You sit beside Robin and watch <<him>> play.
	<br><br>
<</if>>
<<if $rng gte 81>>
	<<He>> gets stuck on a difficult part and drops the controller on your lap. "You try," <<he>> says as <<he>> lies on <<his>> back.
	<br><br>
	You succeed on your first attempt.
	<br><br>
	<<link [[Pass the controller back|Robin Watch Pass]]>><<npcincr Robin love 1>><</link>><<glove>>
	<br>
	<<link [[Tease|Robin Watch Tease]]>><<npcincr Robin love -1>><<npcincr Robin dom -1>><</link>><<llove>><<ldom>>
	<br>
<<elseif $rng gte 61>>
	<<He>> tries to get a laugh out of you by making <<his>> character do silly things. <<Hes>> successful.
	<br><br>
	<<robinoptions>>
<<elseif $rng gte 41>>
	You chat with <<himstop>> The game looks pretty easy, and <<hes>> able to hold a conversation and play at the same time.
	<br><br>
	<<robinoptions>>
<<elseif $rng gte 21>>
	You try to chat with <<himcomma>> but <<hes>> so engrossed in the game <<he>> can't look away.
	<br><br>
	<<if $NPCName[$NPCNameList.indexOf("Robin")].love gte 80>>
		<<link [[Use Robin's lap as a pillow|Robin Pillow]]>><<npcincr Robin love 1>><<npcincr Robin dom 1>><<npcincr Robin lust 1>><<tiredness -3>><<stress -3>><<trauma -3>><</link>><<glove>><<glust>><<gdom>><<ltrauma>><<lstress>><<ltiredness>>
		<br>
		<<link [[Keep watching|Robin Options]]>><</link>>
		<br>
	<<else>>
		<<robinoptions>>
	<</if>>
<<else>>
	<<He>> rests <<his>> head in your lap.
	<<garousal>><<arousal 300>>
	<br><br><br><br>
	<<link [[Stroke|Robin Stroke]]>><<npcincr Robin love 1>><<npcincr Robin lust 1>><</link>><<glove>><<glust>>
	<br>
	<<link [[Keep watching|Robin Options]]>><</link>>
	<br><br><br>
<</if>>