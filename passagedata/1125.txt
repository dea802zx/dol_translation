<<effects>>

You slap Remy's face, too quick for <<him>> to dodge. One of the farmhands shocks you again.
<br><br>

"Don't make this harder than it needs to be," Remy says, tugging your leash so hard you stumble a few steps. <<He>> pulls you along behind <<himstop>>
<br><br>

<<link [[Next|Livestock Job]]>><</link>>
<br>