<<set $outside to 0>><<set $location to "underground">><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	"That'll do," says the <<person2>><<person>> as <<he>> stops filming. You're horrified at the thought of other people seeing what happened to you, but there's nothing you can do as the <<person1>><<person>> leads you back to your cell.
	<br><br>
	<<tearful>> you sit on the mattress.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Underground Cell]]>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	The <<person1>><<person>> trips and falls against the <<person2>><<personstop>> The camera drops to the ground and shatters. "You idiot," says the <<person2>><<personstop>>
	<br><br>
	The <<person1>><<person>> struggles to <<his>> feet. "Shut up," <<he>> says. "Before <<pshe>> gets away." But you're already well ahead of them. You run down the corridor, past your cell, then into the empty theatre. You run towards the stairs on the far side, and climb as fast as you can, footsteps thundering beneath you. You emerge in the forest and run in a random direction, aware you are still being pursued.
	<br><br><br><br>
	<<clotheson>>
	<<endcombat>>
	A chill runs up your spine, a warning from some primal instinct. <span class="red">Something is hunting you.</span>
	<<set $foresthunt to 1>>
	<br><br>
	<<link [[Next|Forest]]>><<set $forest to 40>><</link>>
<<elseif $timer lte 0>>
	"That'll do," says the <<person2>><<person>> as <<he>> stops filming. You're horrified at the thought of other people seeing what happened to you, but there's nothing you can do as the <<person1>><<person>> leads you back to your cell.
	<br><br>
	<<tearful>> you sit on the mattress.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Underground Cell]]>><</link>>
	<br>
<</if>>