<<effects>>
You climb out the car and walk up the path to the orphanage. Avery watches a moment before driving away.
<br><br>
<<if $dateCount.Avery is 3>>
	<<if $loveInterest.primary is "None">>
		<<set $loveInterest.primary to "Avery">>
		<span class = "gold">Avery is now your love interest! You feel yourself moving up in the world.</span>
		<br><br>
	<<else>>
		<span class = "gold">Avery can now be claimed as your love interest! You feel yourself moving up in the world. <br>You can change your love interest in the "Attitudes" menu.</span>
		<br><br>
	<</if>>
<</if>>
<<clotheson>>
<<endcombat>>
<<link [[Next|Domus Street]]>><<set $eventskip to 1>><</link>>
<br>