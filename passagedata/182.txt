<<set $outside to 0>><<set $location to $container.lastLocation>><<effects>>
<<set _pregnancy to $sexStats.anus.pregnancy>>
<<set _pregnancy.feltMovement to false>>
<<set _container to $container[$location]>>
<<set $pregResult to "notReady">>
<<set _stressMulti to 2 - _pregnancy.motherStatus>>
<<set $checkboxResult to {}>>
<<set $checkboxReplace to {}>>
<<set $parasiteSelectionCount to 0>>

Following the directions from Doctor Harper, you get into position and attempt to deliver <<if _pregnancy.namesChildren is true>>your baby<<else>>the parasites<</if>>. <<if _stressMulti gt 0>><<gstress>><</if>><<set $stress += 250 * _stressMulti>>
<br><br>

<<for _i to 0; _i lt _pregnancy.maxCount; _i++>>
	<<if _pregnancy[_i] is null>>
		<<continue>>
	<</if>>
	<<if _pregnancy[_i].stats.gender isnot "Futa">>
		<<set _rand to random(0,100)>>
		<<if (_rand lte 80 and _pregnancy[_i].daysLeft is 0) or (_rand lte 50 and _pregnancy[_i].daysLeft lte 1) or (_rand lte 30 and _pregnancy[_i].daysLeft lte 2) or (_rand lte 10 and _pregnancy[_i].daysLeft lte 3)>>
			<<if $pregResult is "notReady">>
				<<set $pregResult to [clone(_i)]>>
			<<else>>
				<<set $pregResult.push(clone(_i))>>
			<</if>>
		<</if>>
	<</if>>
<</for>>

<<if $pregResult is "notReady">>
	Despite your efforts, none of <<if _pregnancy.namesChildren is true>>your children<<else>>the parasites<</if>> are ready to leave you.
	<br><br>

	<<link [[Next|Containers]]>><<unset $pregResult>><</link>>
<<else>>
	Your body shudders, and to the floor falls <<if _pregnancy.namesChildren is true>><<print $pregResult.length>> <<print $pregResult.length gt 1 ? 'babies' : 'baby'>><<else>><<print $pregResult.length>> <<print $pregResult.length gt 1 ? 'parasites' : 'parasite'>><</if>>.

	<ul>
		<<for _i to 0; _i lt $pregResult.length; _i++>>
			<li><label><<print '<<checkbox "$checkboxResult[' + clone($pregResult[_i]) + ']" false true checked>>'>> - <<print _pregnancy[$pregResult[_i]].creature>> - <<creatureActivity _pregnancy[$pregResult[_i]].stats.speed>></label></li>
		<</for>>
	</ul>
	<<if $pregResult.length gt (_container.maxCount - _container.count)>>
		Please select any required to replace, it will replace from top to bottom:
		<br>
		<ul>
			<<for _i to 0; _i lt _container.maxCount; _i++>>
				<<if _container.creatures[_i] is undefined>>
					<<set $checkboxReplace[_i] to true>>
					<<continue>>
				<</if>>
				<<if _container.creatures[_i] isnot null>>
					<li><label><<print '<<checkbox "$checkboxReplace[' + clone(_i) + ']" false true ($checkboxReplace[' + clone(_i) + '] ? "checked" : "")>>'>> - <<print _container.creatures[_i].creature>> - <<creatureActivity _container.creatures[_i].stats.speed>></label></li>
				<</if>>
			<</for>>
		</ul>
	<</if>>
	<div id="pregResult"><<PregEventsResult>></div>
<</if>>