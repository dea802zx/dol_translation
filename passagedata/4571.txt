<<effects>>

You hide behind tree at the edge of the park, and peek out at Oxford Street. The road is busy with cars, and the occasional pedestrian walks by, unaware of your presence.
<br><br>

A nearby staircase leads to a flyover crossing the road. You could get to the industrial district that way, but you'd need to stay low to remain concealed once up there. Getting that far would be difficult, given how exposed the stairs are.
<br><br>

The thought of crossing while so scantily clad, of such danger, sends a shiver down your spine.
<br><br>

<<link [[Cross|Flyover Ex Undies Dash]]>><<pass 5>><</link>>
<br>
<<link [[Back away|Park]]>><<set $eventskip to 1>><</link>>
<br>