<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>
You successfully pick the lock and enter the abode.
<<if $skulduggery lt 200>>
	<<skulduggeryskilluse>>
<<else>>
	<span class="blue">There's nothing more you can learn from locks this simple.</span>
<</if>>
<br><br>
You sneak around the house and look for anything of value.
<<if $rng gte 81>>
	<<if $bestialitydisable is "f">>
		A low growl interrupts you, and a dog emerges from the darkness. It starts to bark.
		<br><br>
		<<if $deviancy gte 15>>
			<<link [[Try to calm it|Domus House Dog]]>><<set $sexstart to 1>><<generate1>><<generate2>><</link>><<deviant2>>
			<br>
		<</if>>
		<<link [[Fight|Domus House Dog]]>><<set $molestationstart to 1>><<generate1>><<generate2>><</link>>
		<br>
		<<link [[Run|Domus House Run]]>><<transform cat 1>><</link>>
		<br><br>
	<<else>>
		A low growl interrupts you, and a dog emerges from the darkness. It starts to bark.
		<br><br>
		You flee out the house, the dog snapping at your heels. It doesn't chase you beyond the front door, content to bark into the night.
		<<beastescape>>
		<br><br>
		<<link [[Next|Domus Street]]>><</link>>
		<br>
	<</if>>
<<elseif $rng gte 61>>
	You find a safe with a combination lock.
	<br><br>
	<<link [[Try to open it|Domus House Safe]]>><</link>>
	<br>
	<<link [[Leave|Domus Street]]>><</link>>
	<br>
<<elseif $rng gte 56>>
	<<generate1>><<person1>>
	As you creep through a darkened room a low voice startles you.
	<br><br>
	"You're welcome to keep looking around, sweetheart, but there's nothing here. Nothing you'd want. This street is good people, but we haven't got much.
	If you want a mark that's worth something head over to Danube. Nice big places over there. Full of nice stuff."
	<br>
	A lighter sparks twice, lighting up a <<personstop>> Third time, the cigarette in <<his>> mouth catches. A point of glowing red in the dark.
	<br><br>
	"I used to be a thief," <<he>> says. "Just be careful. Thieving's good for a while. Lots of good stuff for free. Nice life. Thought I was untouchable.
	Ha. You pay for it in the end. In this town you always pay. One way or another. Pillory is brutal. Prison's worse. Far worse. Nowadays, I cut the middleman.
	Can make easy money spreading your legs in this town. Easier than penal time. And at least you get to choose who. Keep some control. Prison's brutal."
	<br><br>
	A trail of smoke winds your way. It tickles your throat making you cough.
	<br><br>
	"Sorry," <<he>> says. "Filthy habit. Need to quit. Anyway, there's nothing here. Go see for yourself. Rather that than you come stumbling around here another day.
	Go on, look around all you want. Take whatever. I can get more. Knock yourself out. Baby's sleeping in that room there though. Hasn't been down an hour yet.
	Don't wake him, okay? That's all I ask."
	<br>
	You quietly move away.
	<br>
	"Growing up in this town, the little fucker's gonna need all the rest he can get," <<he>> mutters, seemingly to <<himselfstop>> "Poor little bastard."
	<br><br>
	You find nothing and leave empty handed.
	<br><br>
	<<endevent>>
	<<link [[Leave|Domus Street]]>><</link>>
	<br>
<<elseif $rng gte 51>>
	While searching a room for valuables you find a drawer containing a number of phones. Most are used but all are fairly new.
	<<if $skulduggery gte 500>>
		Your knowledge of the skulduggerous lets you quickly determine that these are stolen phones.
		Stealing them again should not be a crime. It's not like they can report someone stealing their stolen goods.
		<br><br>
		<<link [[Take them|Domus Street]]>><<set $blackmoney += 60>><</link>>
	<<else>>
		Why would someone have so many nearly new phones? How odd.
		<br><br>
		<<link [[Take it|Domus Street]]>><<set $blackmoney += 60>><<crimeup 60>><</link>><<crime>>
	<</if>>
	<br>
	<<link [[Leave|Domus Street]]>><</link>>
	<br>
<<elseif $rng gte 41>>
	<<generate1>><<person1>>You're rummaging through the lounge when you hear footsteps approaching. You take cover behind the curtains, just before a <<person>> enters the room. <<He>> sits down and switches on the television like <<he>> plans to be there for awhile.
	<br><br>
	<<link [[Sneak out|Domus House Sneak 2]]>><</link>>
	<br>
	<<link [[Wait (0:30)|Domus House Wait]]>><<pass 30>><</link>>
	<br>
<<elseif $rng gte 31>>
	You're searching the lounge for valuables when you notice that one step sounds hollow.
	<<if $skulduggery gte 500>>Suspecting a hidden stash,<<else>>Confused,<</if>> you look under the rug and find a trap-door.
	Lifting it up and peering down, it seems there is a set of steps leading down to an underground room.
	<br>
	You cannot make out anything in the darkness.
	<br><br>
	<<link [[Climb down|Domus House Dungeon]]>><</link>>
	<br>
	<<link [[Leave|Domus Street]]>><</link>>
	<br>
<<elseif $rng gte 21>>
	<<switch $rng % 4>>
		<<case 3>>
			<<set _bigthing to "an extremely state of the art gaming system">><<set _thebits to "the curved UHD screens, the surround-speaker system, the gaming chair with integrated controller">>
			<<set _littlething to "large collection of hentai games">><<set _blackmoney to 50>>
		<<case 2>>
			<<set _bigthing to "a powerful home media centre">><<set _thebits to "the huge cinema screen, the integrated speaker system, the sofa-chair with integrated control-panel">>
			<<set _littlething to "huge porn collection">><<set _blackmoney to 80>>
		<<case 1>>
			<<set _bigthing to "an immersive VR anime system">><<set _thebits to "the VR headset, anime-themed fittings and furniture, the sensory-feedback gloves">>
			<<set _littlething to "imported box of anime-themed 'used panties'">><<set _blackmoney to 50>>
		<<default>>
			<<set _bigthing to "a covert Government monitoring station">><<set _thebits to "the spy-cam screens, the cell-phone intercept equipment, the internet monitors and taps">>
			<<set _littlething to "set of 'Top Secret' government files relating to 'Project Dark Elk'">><<set _blackmoney to 110>>
	<</switch>>
	The house is almost entirely bare. You are almost ready to leave when you stumble across an entire room dedicated to _bigthing.
	Unfortunately, most of it - _thebits - is far too difficult to extract or far too bulky to steal.
	<br>
	Thankfully, the _littlething next to the chair is far easier to steal and should be worth something on the black markets.
	<br><br>
	<<link [[Take it|Domus Street]]>><<set $blackmoney += _blackmoney>><<crimeup _blackmoney>><</link>><<crime>>
	<br>
	<<link [[Leave|Domus Street]]>><</link>>
	<br>
<<elseif $rng gte 16>>
	There's nothing of value in the entire house. Even the beds are just greasy sheets over old soiled mattresses on bare wooden floors.
	<<print either("It's actually kind of tragic.","It's almost sad.","Three kids, one adult, and one mouldy half-loaf of bread in the cupboard.")>>
	<br><br>
	You can't steal from these people. You should probably leave <i>them</i> money.
	<br><br>
	<<link [[Leave|Domus Street]]>><</link>>
	<br>
<<else>>
	<<set $rng to random(1, 100)>>
	<<if $rng gte 91>>
		You don't find much, just some loose change worth £1.
		<br><br>
		<<link [[Take it|Domus Street]]>><<set $money += 100>><<crimeup 1>><</link>><<crime>>
		<br>
		<<link [[Leave|Domus Street]]>><</link>>
		<br>
	<<elseif $rng gte 81>>
		You don't find much, just some loose change worth £2.
		<br><br>
		<<link [[Take it|Domus Street]]>><<set $money += 200>><<crimeup 2>><</link>><<crime>>
		<br>
		<<link [[Leave|Domus Street]]>><</link>>
		<br>
	<<elseif $rng gte 71>>
		You find £5 hidden in a drawer.
		<br><br>
		<<link [[Take it|Domus Street]]>><<set $money += 500>><<crimeup 5>><</link>><<crime>>
		<br>
		<<link [[Leave|Domus Street]]>><</link>>
		<br>
	<<elseif $rng gte 61>>
		You find a nice-looking brooch in a drawer.
		<br><br>
		<<link [[Take it|Domus Street]]>><<set $blackmoney += 5>><<crimeup 5>><</link>><<crime>>
		<br>
		<<link [[Leave|Domus Street]]>><</link>>
		<br>
	<<elseif $rng gte 51>>
		You find £10 under a cushion on the sofa.
		<br><br>
		<<link [[Take it|Domus Street]]>><<set $money += 1000>><<crimeup 10>><</link>><<crime>>
		<br>
		<<link [[Leave|Domus Street]]>><</link>>
		<br>
	<<elseif $rng gte 41>>
		You find a necklace on a stool beside the coat stand.
		<br><br>
		<<link [[Take it|Domus Street]]>><<set $blackmoney += 10>><<crimeup 10>><</link>><<crime>>
		<br>
		<<link [[Leave|Domus Street]]>><</link>>
		<br>
	<<elseif $rng gte 31>>
		You find £15 lying on the kitchen worktop.
		<br><br>
		<<link [[Take it|Domus Street]]>><<set $money += 1500>><<crimeup 15>><</link>><<crime>>
		<br>
		<<link [[Leave|Domus Street]]>><</link>>
		<br>
	<<elseif $rng gte 21>>
		You find a set of silverware.
		<br>
		<<link [[Take it|Domus Street]]>><<set $blackmoney += 15>><<crimeup 15>><</link>><<crime>>
		<br>
		<<link [[Leave|Domus Street]]>><</link>>
		<br>
	<<elseif $rng gte 11>>
		You find a pair of gold cufflinks in a coat pocket.
		<br><br>
		<<link [[Take them|Domus Street]]>><<set $blackmoney += 20>><<crimeup 20>><</link>><<crime>>
		<br>
		<<link [[Leave|Domus Street]]>><</link>>
		<br>
	<<else>>
		You find £30 in a pot beside the front door.
		<br><br>
		<<link [[Take it|Domus Street]]>><<set $money += 3000>><<crimeup 30>><</link>><<crime>>
		<br>
		<<link [[Leave|Domus Street]]>><</link>>
		<br>
	<</if>>
<</if>>