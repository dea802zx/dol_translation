<<effects>>

<<if $rng gte 71>>

	You crouch down, leaning closer to the plant. The smooth green tip drips a pink, sweet-smelling fluid. You feel an urge to taste it.
	<br><br>
	
	<<link [[Lick|Garden Stem Lick]]>><<set $phase to 0>><</link>><<deviant1>>
	<br>


<<elseif $rng gte 41>>
	You crouch down, leaning closer to the plant. The thin green tip drips a purple, sweet-smelling fluid. You feel an urge to taste it.
	<br><br>
	
	<<link [[Lick|Garden Stem Lick]]>><<set $phase to 1>><</link>><<deviant1>>
	<br>

<<else>>
	You crouch down, leaning closer to the plant. The bulbous green tip drips a red, sweet-smelling fluid. You feel an urge to taste it.
	<br><br>

	<<link [[Lick|Garden Stem Lick]]>><<set $phase to 2>><</link>><<deviant1>>
	<br>


<</if>>
	<<link [[Shake it off|Garden Stem Shake]]>><</link>>
	<br>