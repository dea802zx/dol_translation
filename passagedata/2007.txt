<<effects>>

<<if $phase is 0>>

You hold out £50. The <<person>> looks at it, but doesn't move. "Don't make fun of me," <<he>> says.
<br><br>

"I'm not," you reply. "I promise."
<br><br>

<<He>> takes the money. "Thank you," <<he>> sniffs. <<He>> opens <<his>> mouth to say something, but instead runs inside.
<br><br>

<<elseif $phase is 1>>

You hold out £20. "Why?" the <<person>> asks.
<br><br>

"Because I want to help," you reply. "Take it."
<br><br>

<<He>> takes the money. "Thank you," <<he>> sniffs. <<He>> opens <<his>> mouth to say something, but instead runs inside.
<br><br>

<</if>>

<<link [[Next|Orphanage]]>><<endevent>><</link>>
<br>