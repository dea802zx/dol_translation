<<set $outside to 0>><<set $location to "temple">><<temple_effects>><<effects>>
You fight through the shame and speak.
<<if $submissive gte 1150>>
	"I'm dirty," you say, dropping to your knees. "Please forgive me."
<<elseif $submissive lte 850>>
	"I've been soiled," you say, trying to steady the tremble in your voice. "I need your forgiveness."
<<else>>
	"I'm dirty," you say, voice trembling. "Please forgive me."
<</if>>
<br><br>
<<He>> considers for a moment. "Of course my child," <<he>> says. "We only require a small donation of <span class="red">£10,000</span> and everything will be forgiven."
<br><br>
<<if $submissive gte 1150>>
	You nod. That's expensive, but you don't think there's any other way.
<<elseif $submissive lte 850>>
	You repress the urge to spit in their holy water. That's a lot of money, but you can't think of any other way.
<<else>>
	That's a lot of money, but you can't think of any other way.
<</if>>
<br><br>
<<set $forgiveintro to 1>>
<<endevent>>
<<link [[Next|Temple]]>><</link>>
<br>