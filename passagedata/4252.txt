<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>><<npcincr Whitney lust -20>>

<<His>> friends applaud <<himstop>> "See you later, slut," <<he>> sneers as <<he>> pulls a cigarette from <<his>> shirt pocket.
<<llust>>
	<br><br>

<<tearful>> you climb to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<<elseif $enemyhealth lte 0>>

Whitney shouts in pain and staggers away from you.
<<npcincr Whitney dom -1>><<ldom>>
<br><br>

"I've had enough of you," Whitney says, a slight quiver in <<his>> voice. "What are you waiting for you fucks? This slut has wasted enough of our time."
<br><br>

<<tearful>> you climb to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<<elseif $alarm is 1 and $rescue is 1>>
"Stop crying, slut," Whitney says. <<He>> shoves you to the ground. <<He>> starts kicking you, so you curl up and shield your body. The crowd jeers at you. You think some of them get kicks in of their own.
<<gpain>><<set $pain += 40>>
<br><br>

You hazard a peek once the kicking stops, and see Whitney and <<his>> friends walking away.
<br><br>

<<tearful>> you climb to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>

<<else>>

"Aww," Whitney says. "Has the baby had enough?" <<He>> shoves you to the ground. "Whatever. I was bored anyway." <<He>> and <<his>> friends leave. <<tearful>> you climb to your feet.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>

<</if>>