<<set $outside to 0>><<set $location to "cafe">><<effects>>
<<set $chef_state to 9>>
<<npc Sam>><<person1>>
You enter the Ocean Breeze. Sam approaches you and clasps your hands in <<his>> own. "Are you ready?" <<he>> asks. You nod.
<br><br>
"I'll prepare things here," <<he>> says. "You only need to worry about yourself. You need to make a proper entrance." <<He>> waves a
<<endevent>><<generatey1>><<generate2>><<person1>>
<<person>> over. You recognise <<him>> from the orphanage. "We've set up somewhere for you to get ready nearby. My new employee here will show you."
<br><br>

You follow the <<person>> out the back, and cross the street. <<He>> takes you to an old house at the edge of the residential district.
<br><br>

<<link [[Next|Chef Opening 2]]>><</link>>
<br>