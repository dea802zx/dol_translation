<<effects>><<set $location to "town">>

"Are you upset with me?" you ask, looking at Bailey through the rearview mirror.
<br><br>

"No," <<he>> says. "That will change if you don't be quiet."
<br><br>

No one says anything else as Bailey drives you into town, and back to the orphanage.
<br><br>

<<link [[Next|Domus Street]]>><<endevent>><<set $eventskip to 1>><<set $stress -= 200>><</link>>
<br>