<<effects>>

You are in the dance studio's changing room. Benches line the walls, with hooks for clothes hanging above.
<br><br>

You could strip down to a leotard or similar dance clothes here.
<br><br>

<<storeon "dance_studio" "check">>
<<if _store_check is 1>>
	You left your clothes here.
	<br>
	<<link [[Wear|Dance Studio Changing Room]]>><<storeon "dance_studio">><</link>>
	<br>
<</if>>

<<if $worn.under_upper.type.includes("dance") and $worn.under_lower.type.includes("dance")>>
	<<if $worn.upper.name isnot "naked">>
		<<link [[Strip down to your dance clothes|Dance Studio Change]]>><</link>>
		<br>
	<<elseif $worn.lower.name isnot "naked">>
		<<link [[Strip down to your dance clothes|Dance Studio Change]]>><</link>>
		<br>
	<</if>>
<</if>>
<br>
<<link [[Leave|Dance Studio]]>><</link>>
<br>