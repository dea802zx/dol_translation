<<effects>><<set $location to "sewers">><<set $outside to 0>>

You feign the most polite smile you can muster, and manage to power through the first rat. The second rat proves challenging, and you gag after the third rat. Alarmed, Morgan pours the tea sludge down your throat, making it worse.
<br><br>

"There," <<he>> says after a few moments. "Now maybe you won't be so grouchy. There are chores you can help me with, or maybe we'll take a nap."
<br><br>

<<link [[Next|Sewers Morgan]]>><</link>>
<br>