<<set $outside to 0>><<set $location to "town">><<effects>>
<<set $phase to 0>>
<<if $sciencelichenchance is 0>>
	You don't have a lichen project. You draw some lichen and write down everything you know about the subject. That will have to do. A <<person2>><<person>> with a flower display beside yours looks over and smirks.
<<elseif $sciencelichenchance lt 100>>
	You set up your lichen display and step back to admire it. It could be better, but it's not the worst in the room. A <<person2>><<person>> with a flower display beside yours looks over and smirks.
<<else>>
	You set up your lichen display and step back to admire it. It couldn't be better. A <<person2>><<person>> with a flower display beside yours looks irritated.
<</if>>
<br><br>
Leighton walks on stage and taps the microphone. Sirris stands beside <<person1>><<himstop>> "Thank you, boys and girls, for coming," <<he>> says. "I've seen some fine work already. Sirris and I will judge. Be patient."
<br><br>

Leighton and Sirris leave the stage and circle around the room. Leighton criticises the projects. Sirris observes, but gives words of encouragement if Leighton was particularly scathing. Still, some students are left tearful by the headteacher's remarks.
<br><br>

The teachers draw closer. The <<person2>><<person>> with the flower display bobs on <<his>> heels, brimming with confidence.
<br><br>

<<set $skulduggerydifficulty to 400>>
<<link [[Sabotage them|Science Fair Sabotage]]>><</link>><<skulduggerydifficulty>><span class="gold"> +10% win chance</span>
<br>
<<link [[Play fair|Science Fair Fair]]>><</link>>
<br>