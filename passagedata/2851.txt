<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $phase is 0>>
	<<if $rng gte 81>>
	The <<person1>><<person>> looks at the <<person2>><<person>> and smiles. "<<pShe>> thinks <<pshe>> can ignore us." <<person1>><<He>> gags you with <<his>> hand.
	<br><br>

	<<link [[Next|School Library Molestation]]>><<set $molestationstart to 1>><</link>>
	<br>
	<<else>>
	You ignore their pestering until they tire and leave you alone.
	<br><br>

	<<link [[Next|School Library]]>><<endevent>><</link>>
	<</if>>
<<elseif $phase is 1>>
<<generate3>><<person3>>You shout for help. Several people arrive to investigate, including the librarian. <<He>> glares at the pair. "You two again. Out."
<br>
<<person2>>The <<person>> looks indignant. "We're just play-"
<br>
The librarian interrupts. "I said OUT." The pair comply, leaving you in peace.
<br><br>

<<person3>>The librarian asks if you're okay, and you nod. Assured that you're fine, <<he>> returns to <<his>> duties.
<br><br>

<<link [[Next|School Library]]>><<endevent>><</link>>

<</if>>