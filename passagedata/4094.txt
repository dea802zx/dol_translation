<<set $outside to 0>><<set $location to "home">><<effects>>

<<if $robinromance is 1>>
<<He>> makes a satisfied sound. "I feel so safe with you," <<he>> says.
<br><br>
<<else>>
You stroke <<his>> hair. "Stop," <<he>> says. "You'll make me fall asleep." You move your hand away. "I didn't mean actually stop."
<br><br>
<</if>>

<<robinoptions>>