<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $schoolstate is "second">>
	<<if $worn.upper.type.includes("naked") and $worn.lower.type.includes("naked") and $rightarm isnot "bound">>
		/*Check for when the pc is only wearing and overcoat*/
		<<if $worn.under_upper.name is "naked" and $worn.under_lower.name is "naked">>
			Wearing nothing underneath over slot clothing comment
		<<elseif $worn.under_upper.name is "naked">>
			Wearing only under_lower under over slot clothing comment
		<<elseif $worn.under_lower.name is "naked">>
			Wearing only under_upper under over slot clothing comment
		<</if>>
		<br><br>
		<<link [[Next|Hallways]]>><<endevent>><</link>>
		<br><br>
	<<elseif $worn.upper.type.includes("school") and $worn.lower.type.includes("school") and $rightarm isnot "bound">>
		<<if $mathsattended is 1>>
			You rejoin the maths lesson,
			<<if $worn.over_upper.name isnot "naked" or $worn.over_lower.name isnot "naked" or $worn.over_head.name isnot "naked">>
				hang your coat at the back of the class
				<<undressOverClothes "mathsClassroom">>
			<</if>>
			and take your seat.
			<br><br>
			<<mathsstart>>
		<<elseif $time lte ($hour * 60 + 5)>>
			You enter the maths classroom.
			<<npc River>><<person1>>
			River is preparing at the front of the room while the seats fill.
			<<if $worn.over_upper.name isnot "naked" or $worn.over_lower.name isnot "naked" or $worn.over_head.name isnot "naked">>
				<<undressOverClothes "mathsClassroom">>
				You hang your coat at the back of the class and take your seat.
			<</if>>
			<br><br>

			<<mathsstart>>
		<<else>>
			You enter the maths classroom.
			<<npc River>><<person1>>
			River cuts off mid-sentence to regard you. "You're late. Maybe some time in detention will prevent future tardiness."
			<<gdelinquency>>
			<br><br>
			<<detention 1>>
			You
			<<if $worn.over_upper.name isnot "naked" or $worn.over_lower.name isnot "naked" or $worn.over_head.name isnot "naked">>
				hang your coat at the back of the class and
				<<undressOverClothes "mathsClassroom">>
			<</if>>
			take a seat as River continues.
			<br><br>

			<<mathsstart>>
		<</if>>

	<<elseif $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
		<<npc River>><<person1>>
		River's eyes narrow as <<he>> sees your bound <<if $rightarm is "bound" or $leftarm is "bound">>arms<</if>><<if ($rightarm is "bound" or $leftarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>. "And another one. Look, whatever ever silly game you're playing needs to stop. You're not coming in here tied up like that. Go see the head."
		<br><br>

		<<link [[Next|Hallways]]>><<endevent>><</link>>
		<br><br>
	<<else>>
		<<npc River>><<person1>>
		River's eyes narrow as you enter. "You can't attend my lesson without a uniform. Go see the head."
		<br><br>

		<<link [[Next|Hallways]]>><<endevent>><</link>>
		<br><br>
	<</if>>
<<elseif $schoollesson is 1>>
	<<if $mathsinterrupted is 1>>
		<<npc River>><<person1>>You enter the maths classroom. River stops speaking abruptly and looks your way. "You again? Get out." <<He>> shuts the door on you.
		<<gdelinquency>>
		<br><br>
		<<detention 1>>

		<<link [[Next|Hallways]]>><<endevent>><</link>>
	<<else>>
		<<npc River>><<person1>>You enter the maths classroom. River stops speaking abruptly and looks your way. "Yes, what is it?" It seems you've interrupted the lesson.
		<br><br>
		<<set $mathsinterrupted to 1>>

		<<link [[Apologise|Maths Classroom Apology]]>><<trauma 1>><<stress 1>><</link>><<gstress>><<gtrauma>>
		<br>
		<<if $trauma gte 1>>
			<<link [[Mock|Maths Classroom Mock]]>><<status 1>><<stress -12>><</link>><<lstress>><<gcool>><<gdelinquency>>
			<br>
		<</if>>
	<</if>>
<<else>>
	You are in the maths classroom. Charts and graphs cover the walls.
	<br><br>
	<<storeon "mathsClassroom" "check">>
	<<if _store_check is 1>>
		<<storeon "mathsClassroom">>
		You take your coat at the back of the class.
		<br><br>
	<</if>>

	<<exhibitionclassroom>>

	<<link [[Leave the room (0:01)|Hallways]]>><<pass 1>><</link>>
	<br>
<</if>>