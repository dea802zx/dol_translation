<<effects>>

The <<monk>> and initiates watch as the coals heat. They soon glow. The initiates lift their shovels, and push the topmost coals onto the earth. They don't stop there. They continue to spread the coals. They soon have the fire spread into a thin rectangle, its foot beside yours.
<br><br>
<<if !$worn.feet.type.includes("naked") or !$worn.legs.type.includes("naked")>>
"Remove your footwear," the <<person2>><<monk>> says. You do so. <<feetstrip>><<legsstrip>>
<br><br>
<</if>>

<<person1>>"The trial of purity requires you walk across the bed before you," Jordan says. "At least one foot must touch them at all times. Don't be afraid. The fire won't hurt you, but your mind might deceive you into thinking otherwise."
<br><br>
<<if $phase is 0>>
"You can try again if you fail," <<he>> continues. "But only once a week."
<br><br>
<</if>>

<<link [[Walk|Temple Firewalk 2]]>><</link>>
<br>
<<if $phase is 0>>
<<link [[Refuse|Temple Firewalk Refuse]]>><</link>>
<br>
<</if>>