<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>

<<if $athletics gte random(1, 1000)>>

You turn and run. The cat pounces, but you <span class="green">succeed</span> in evading it. You flee into the street.
<<gstress>><<stress 6>>
<br><br>

<<link [[Next|Danube Street]]>><</link>>

<<else>>

You turn and run. The cat pounces and you <span class="red">fail</span> to evade it. Its weight forces you to the ground.
<br><br>

<<link [[Next|Danube House Cat]]>><<set $molestationstart to 1>><<generate1>><<generate2>><<set $phase to 2>><</link>>
<br>

<</if>>