<<set $outside to 1>><<set $location to "beach">><<effects>>
You are on the beach.
<<if $daystate is "day">>
	<<if $weather is "clear">>
		It is awash with visitors, children build sandcastles and play in the water while their parents bask in the sun. A group of teenagers are playing volleyball.
	<<elseif $weather is "overcast">>
		The clouds have driven away most would-be visitors, but there are still people strolling along the water's edge.
	<<elseif $weather is "rain">>
		The beach itself is mostly deserted due to the rain, but the violent waves have attracted surfers.
	<</if>>
<<elseif $daystate is "dawn">>
	<<if $weather is "clear">>
		It is a popular destination for joggers, some have dogs with them. A few families are setting up windbreakers. A group of teenagers are playing volleyball.
	<<elseif $weather is "overcast">>
		It is a popular destination for joggers, some have dogs with them. Fog blocks your view of the ocean.
	<<elseif $weather is "rain">>
		The beach itself is mostly deserted due to the rain, but the violent waves have attracted surfers.
	<</if>>
<<elseif $daystate is "dusk">>
	<<if $weather is "clear">>
		Families are leaving as the sun sets. A group of teenagers are playing volleyball.
	<<elseif $weather is "overcast">>
		It is mostly deserted, but some people are strolling along the water's edge.
	<<elseif $weather is "rain">>
		The beach itself is mostly deserted due to the rain, but the violent waves have attracted surfers.
	<</if>>
<<elseif $daystate is "night">>
	<<if $weather is "clear">>
		It appears deserted, save for a group of teenagers who are drinking around a fire.
	<<elseif $weather is "overcast">>
		It appears deserted.
	<<elseif $weather is "rain">>
		It appears deserted.
	<</if>>
<</if>>
You could go for a swim, but make sure to dress appropriately.
<br><br>
<<if $exposed gte 1>>
	<<exhibitionism "beach">>
<</if>>
<<if $arousal gte 10000>>
	<<orgasmbeach>>
<</if>>
<<if $stress gte 10000>>
	<<passoutbeach>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure) and $eventskip is 0>>
		<<eventsbeach>>
	<<else>>
		<<if $exposed lte 0>>
			<<if $scienceproject is "ongoing" and $sciencephallusknown is 1 and $sciencephallus lt 10 and $daystate isnot "night" and $weather isnot "rain" and $exposed lte 0>>
				<<link [[Find participant for phallus project (0:15)|Beach Phallus]]>><<pass 15>><</link>>
				<br>
			<</if>>
			<<link [[Go for a swim (0:02)|Sea Beach]]>><<pass 2>><</link>>
			<br>
		<<elseif $daystate is "night">>
			<<link [[Go for a swim (0:02)|Sea Beach]]>><<pass 2>><</link>>
			<br>
		<</if>>
		<<if $daystate is "day" and $weather is "clear">>
			<<if $worn.under_lower.type.includes("swim") and ($worn.under_upper.type.includes("swim") or $worn.under_upper.type.includes("naked")) and ($worn.upper.type.includes("swim") or $worn.upper.type.includes("naked")) and ($worn.lower.type.includes("swim") or $worn.lower.type.includes("naked"))>>
				<<link [[Tan on the beach (1:00)|Tanning]]>><<pass 60>><</link>><<lstress>>
				<br>
			<</if>>
			<<if def $milkshake and $milkshake gt 0>>
				<<link [[Drink your milkshake (0:10)|Beach Milkshake]]>><<pass 10>><<set $milkshake -= 1>><<stress -5>><</link>><<lstress>>
				<br>
			<</if>>
		<</if>>
		<<link [[Changing Room|Changing Room]]>><</link>>
		<br>
		<<if $exposed lte 0>>
			<<if $NPCName[$NPCNameList.indexOf("Robin")].init is 1 and $robinmissing isnot 1 and $NPCName[$NPCNameList.indexOf("Robin")].trauma lt 80>>
				<<if $weekday is 7 and $hour gte 9 and $hour lte 16 or $weekday is 1 and $hour gte 9 and $hour lte 16>>
				<<link [[Robin's Lemonade Stand|Robin's Lemonade]]>><</link>>
				<br>
				<</if>>
			<</if>>
			<<if $worn.feet.type.includes("heels")>>
				<<link [[Take a walk in heels (0:30)|Beach Heel Walk]]>><</link>><<gtiredness>><<lstress>>
			<<else>>
				<<link [[Go for a run (0:30)|Beach Run]]>><<athletics 3>><</link>><<gtiredness>><<gathletics>><<lstress>>
			<</if>>
			<br>
		<</if>>
		<<if $weather is "clear" and $daystate is "night" and $exposed lte 0>>
			<<link [[Party|Beach Party]]>><</link>>
			<br>
		<</if>>
		<<if $weather is "clear" and $daystate isnot "night" and $exposed lte 0>>
			<<link [[Volleyball|Beach Volleyball]]>><</link>>
			<br>
			<<if $exhibitionism gte 75 and $beachstrip isnot 1>>
				<<link [[Strip (0:20)|Beach Strip]]>><<pass 20>><</link>><<exhibitionist5>>
				<br>
			<</if>>
		<</if>>
		<<add_link "<br>Travel<br>">><<hideDisplay>>
		<<if $exposed lte 0 or $daystate is "night">>
			<<cliff>>
			<br>
		<</if>>
		<<if $exposed lte 0 or $daystate is "night">>
			<<starfish>>
			<br>
		<</if>>
		<<displayLinks>>
		<br>
	<</if>>
<</if>>
<<set $eventskip to 0>>