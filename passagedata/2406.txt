<<set $outside to 0>><<set $location to "park">><<effects>>
<<He>> leads you to a small cafe at the edge of the park. <<He>> orders a coffee for <<him>> and some fruit juice for you. The <<generate3>><<person3>><<if $pronoun is "m">>waiter<<else>>waitress<</if>><<person1>> thinks you're Avery's <<if $player.gender_appearance is "m">>son<<else>>daughter<</if>>. <<He>> is old enough to be your <<if $pronoun is "m">>father<<else>>mother<</if>>, but <<hes>> in good shape. <<He>> smiles at you across the table.
<br><br>
You talk with Avery over the drinks. <<He>> asks a lot of questions, but is reluctant to answer yours. <<He>> doesn't take <<his>> eyes off you.
<br><br>
<<Hes>> asking if you're happy at school when interrupted by a buzzing in <<his>> pocket. "I'm sorry," <<he>> says. "It's work. I need to go." <<He>> stands up, then hesitates a moment before leaning close.
<br><br>
<<link [[Smooch|Park Lichen Smooch]]>><<npcincr Avery love 1>><</link>><<glove>><<promiscuous1>>
<br>
<<link [[Stay still|Park Lichen Still]]>><<npcincr Avery love 1>><</link>><<glove>>
<br>
<<link [[Move away|Park Lichen Move]]>><</link>>
<br>