<<set $outside to 0>><<effects>>

You climb aboard the bus. Tickets cost £1.
<br><br>
<<if $money gte 100>>

Residential
<br>

<<domusicon>><<link [[Buy a ticket to Domus Street (Home)|Bus seat]]>><<set $bus to "domus">><<set $money -= 100>><</link>>
<br>

<<barbicon>><<link [[Buy a ticket to Barb Street (Studio)|Bus seat]]>><<set $bus to "barb">><<set $money -= 100>><</link>>
<br>

<<danubeicon>><<link [[Buy a ticket to Danube Street (Mansions)|Bus seat]]>><<set $bus to "danube">><<set $money -= 100>><</link>>
<br>

<<wolficon>><<link [[Buy a ticket to Wolf Street (Temple)|Bus seat]]>><<set $bus to "wolf">><<set $money -= 100>><</link>>
<br><br>
Commercial
<br>

<<highicon>><<link [[Buy a ticket to High Street (Shopping Centre)|Bus seat]]>><<set $bus to "high">><<set $money -= 100>><</link>>
<br>

<<connudatusicon>><<link [[Buy a ticket to Connudatus Street (Clubs)|Bus seat]]>><<set $bus to "connudatus">><<set $money -= 100>><</link>>
<br>

<<clifficon>><<link [[Buy a ticket to Cliff Street (Cafe)|Bus seat]]>><<set $bus to "cliff">><<set $money -= 100>><</link>>
<br>

<<nightingaleicon>><<link [[Buy a ticket to Nightingale Street (Hospital)|Bus seat]]>><<set $bus to "nightingale">><<set $money -= 100>><</link>>
<br>

<<starfishicon>><<link [[Buy a ticket to Starfish Street (Beach)|Bus seat]]>><<set $bus to "starfish">><<set $money -= 100>><</link>>
<br>

<<oxfordicon>><<link [[Buy a ticket to Oxford Street (School)|Bus seat]]>><<set $bus to "oxford">><<set $money -= 100>><</link>>
<br><br>
Industrial
<br>

<<elkicon>><<link [[Buy a ticket to Elk Street|Bus seat]]>><<set $bus to "elk">><<set $money -= 100>><</link>>
<br>

<<mericon>><<link [[Buy a ticket to Mer Street (Docks)|Bus seat]]>><<set $bus to "mer">><<set $money -= 100>><</link>>
<br>

<<harvesticon>><<link [[Buy a ticket to Harvest Street (Pub)|Bus seat]]>><<set $bus to "harvest">><<set $money -= 100>><</link>>
<br>

<<else>>

You cannot afford the fare.
<br><br>

<</if>>

<br><br>
Get off
<br>
<<destination>>