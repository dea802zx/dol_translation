<<set $outside to 0>><<effects>>
<<if $phase is 0>>
	"There's no charge, <<if $skulduggery gte 700 or $stealstate gte 3>>Officer,<<else>><<if $pronoun is "m">>sir,<<else>>ma'am,<</if>><</if>>" you smile. "I'm not a prostitute!"
	<br><br>
	"Right..." <<he>> says. "Then I guess you better be on your way."
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<destinationeventend>>
<</if>>
<<if $phase gte 3>>
	"You knew the deal. You said you'd pay. You can't arrest me for committing a crime you paid me to do! Pay up."
	<br><br>
	<<He>> holds up <<his>> hands: "Okay. Okay. It was worth a try."
	<br><br>
	You shake your head as <<he>> smiles innocently.
	<br><br>
<</if>>
<<if $phase gte 2>>
	"Take what you need from that bag beside you," <<he>> says. "It's all there."
	<br><br>
	You turn to get the bag, when the <<person>> roughly grabs you. You feel something lock around your arms, binding them behind you.
	<<set $rightarm to "bound">><<set $leftarm to "bound">>
	<br><br>
	"Guess what?" <<He>> says. "You're under arrest. Prostitution."
	<br><br>
<</if>>
<<if $phase gte 1>>
	<<He>> leaves you bound in the back of the car, as <<he>> goes forward to drive.
	<br><br>
	"Sit back," <<he>> says. "Enjoy the ride."
	<br><br>
	<<clotheson>>
	You sit <<if $submissive lte 500>>silently fuming.<<else>>in silence.<</if>> <<He>> taps the steering wheel in time to the music on the radio.
	<br><br>
	You notice <<him>> looking at you in the mirror. "Don't worry. It'll probably just be the pillory. Hardly a punishment at all. You'll be fine. Y'know it's my day off tomorrow. If you're still there I'll pay you a visit. Make sure you don't get cold," <<He>> leers at you in the mirror. "Would you like that? ... Sure you would."
	<br><br>
	<<He>> eventually drops you off at the station, where you are put in a holding cell.
	<br><br>
	"They'll be with you in a couple of hours," the <<person>> tells you as <<he>> leaves. "Have a nice day, citizen."
	<br><br>
	<<link [[Wait|Police Cell]]>><<pass 2 hour>><<endcombat>><</link>>
	<br>
<</if>>