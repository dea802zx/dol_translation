<<set $outside to 0>><<set $location to "docks">><<dockeffects>><<effects>>

You successfully pick the lock and open the container. The <<person>> glances around. "I didn't see anything," <<he>> says. "I need this job." <<He>> walks away.
<br><br>

<<if $skulduggery lt 500>>
<<skulduggeryskilluse>>
<<else>>
<span class="blue">There's nothing more you can learn from locks this simple.</span>
<</if>>
<br><br>

<<person1>>There are sacks full of potatoes inside. You hear the voice again, from the back. One of the sacks moves. You find a <<person>> beneath, gagged and bound. Bruising surrounds <<his>> right eye. <<He>> starts crying.
<<gstress>><<stress 6>>
<br><br>

<<link [[Get help|Docks Slave Help]]>><</link>>
<br>
<<link [[Comfort and untie|Docks Slave Untie]]>><</link>>
<br>