<<set $outside to 0>><<set $location to "home">><<effects>>

<<if $phase is 0>>
You give Robin some complaints about the things that have been happening to you, but you don't trust <<him>> enough to give the full details about what's really been going on so you are vague about the more significant stuff. <<He>> doesn't really seem to know how to respond, and winds up ducking out of your room before you even manage to get all the way through your list of issues.
<br><br>
<<endevent>>
<<link [[Next|Bedroom]]>><</link>>
<br>
<<elseif $phase is 1>><<set $robinbaileyhelp to 1>>
You bite back at <<him>> and tell <<him>> it's <<his>> fault for going to Bailey in the first place, and dismiss <<his>> concerns. You eventually send <<him>> out of your room, still embarrassed about such an episode.
<br><br>
<<endevent>>
<<link [[Next|Bedroom]]>><</link>>
<br>
<<elseif $phase is 2>><<set $robinbaileyhelp to 1>>
You lay into Robin for leaving in the middle of your breakdown and getting Bailey. You yell at <<him>> and chew <<him>> out, redirecting all of the unrealised frustrations from your previous breakdown into anger at Robin for doing something so stupid. <<He>> leaves your room in tears.
<br><br>
<<endevent>>
<<link [[Next|Bedroom]]>><</link>>
<br>
<<else>>
You shrug off Robin and refuse to speak to <<himstop>> Eventually, <<he>> takes the hint and leaves.
<br><br>
<<endevent>>
<<link [[Next|Bedroom]]>><</link>>
<br>
<</if>>