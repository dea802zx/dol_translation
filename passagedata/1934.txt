<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">>

Lacking anything to keep <<his>> attention, the <<person>> remains uninterested in <<his>> homework. The rest of the hour passes without event.
<br><br>

You earn £5.
<<pass 30>><<set $money += 500>>
<br><br>

<<link [[Next|Domus Street]]>><<endevent>><</link>>
<br>