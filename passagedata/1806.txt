<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "danube">>

<<if $molestationstart is 1>>
	<<set $molestationstart to 0>>
	<<molested>>
	<<controlloss>>

	You try to pull apart the web, but your hands get stuck. You manage to pull them free, but the rest of your body becomes tangled in the process, leaving you suspended in the air. Your struggling causes one of the egg sacs to fall from its perch. It breaks apart on the wooden floor, releasing thousands of tiny, live spiders, which start crawling up the web toward you.
	<br><br>

	<<set $combat to 1>>
	<<swarminit "spiders" "sacs" "slipping" "break" "steady" 1 9>>
	<<set $timer to 30>>
<</if>>

<<if $timer gte 25>>
The old timbers above the web creak. If this keeps up they'll break.
<<elseif $timer gte 20>>
Splinters break from the ceiling.
<<elseif $timer gte 10>>
The wood continues to break down.
<<elseif $timer gte 1>>
The wooden ceiling creaks ominously.
<<else>>
A wooden beam breaks from the ceiling and the web tumbles to the ground.
<</if>>
<br><br>

<<swarmeffects>>
<<swarm>>
<<swarmactions>>

<<if $timer lte 0>>
	<span id="next"><<link [[Next|Danube Spiders Finish]]>><</link>></span><<nexttext>>
<<else>>
	<span id="next"><<link "Next">><<script>>state.display(state.active.title, null)<</script>><</link>></span><<nexttext>>
<</if>>