<<set $outside to 0>><<effects>>
"Thank you," you say. "You saved me."
<br><br>
<<Hes>> on the verge of tears. "I was afraid you'd be hurt," <<he>> says. "I didn't know what to do."
<br><br>
<<link [[Hug|Avery Walk Hug]]>><</link>>
<br>
<<if $robinromance is 1>>
	<<link [[Kiss|Avery Walk Kiss]]>><</link>>
	<br>
<</if>>