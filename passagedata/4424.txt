<<effects>><<set $location to "sewers">><<set $outside to 0>>
<<set $sewerschased to 0>>
<<set $sewersevent to random(5, 12)>>
<<npc Morgan>><<person1>>You hide behind the waterfalls. The noise masks the sound of Morgan's footsteps, but you see their shadow cast on the stone floor. You crouch and creep around the pillar of water, keeping it between you and where you hope Morgan walks. The shadow retracts as Morgan leaves. You're safe for now.
<<lstress>><<stress -6>>
<br><br>
<<endevent>>
<<destinationsewers>>