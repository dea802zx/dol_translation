<<widget "eventsmaths">><<nobr>>
<<if $rng gte 91 and $wolfgirl gte 4>>

<<npc River>><<person1>>River marches up and down the class. <<He>> stops beside you.
  <<if $riverbite isnot 1>>
	<<set $riverbite to 1>>
		"That headband is against school regulations," <<he>> says, grabbing one of your wolf ears.
  <<else>>
		"You're still wearing that silly headband?" <<he>> says. "I thought I made myself clear." <<He>> grabs one of your wolf ears.
  <</if>>
<<He>> tugs painfully.<<gpain>><<gstress>><<set $pain += 10>><<stress 1>>
<br><br>

<<link [[Bite|Maths Event Bite]]>><<npcincr River dom -1>><<npcincr River love -1>><<detention 6>><<stress -12>><<trauma -6>><<set $submissive -= 10>><</link>><<gdelinquency>><<ltrauma>><<lstress>>
<br>
<<link [[Endure|Maths Event Endure]]>><<npcincr River dom 1>><<stress 6>><<trauma 6>><<set $pain += 20>><<set $submissive += 5>><</link>><<gpain>><<gtrauma>><<gstress>>
<br>

<<elseif $fameexhibitionism gte 500 and $delinquency gte 500 and $mathrulerin is 0 and $worn.genitals.name isnot "chastity belt">>

	<<npc River>><<person1>>
	You hear River's phone buzzing and without even checking it <<he>> suddenly says <<he>> needs to leave the classroom and will be back in a few minutes.
	<br>
	<<He>> gives the class some equations to solve while <<he>> is away.
	<br><br>
	After you don't hear <<his>> footsteps anymore a girl stands up and picks up a ruler from her desk.
	<br>
	"Hey you." She looks at you.
	<br>
	"I've heard you like to show yourself. How about a little something now?" she says. "In front of the whole class of course."
	<br>
	"I bet you to insert this ruler in your <<if $player.gender_appearance is "m">>asshole<<else>>pussy<</if>>.I'll give you £40 if you do."
	<br><br>

	<<link [[Accept the Bet|Maths Ruler]]>><<set $phase to 1>><<status +10>><<arousal 1500>><<set $mathrulerin to 1>><</link>><<exhibitionist5>><<fameexhibitionism 3>><<garousal>>
	<br>
	<<link [[Refuse|Maths Ruler]]>><<stress 2>><<set $phase to 2>><<status -10>><<set $mathrulerin to 1>><</link>><<gstress>>
	<br>

<<elseif $rng gte 81>>
<<npc River>><<person1>>
River gives the class some equations to solve while <<he>> manages paperwork.
	<<if $whitneymaths is "seat">>
		<<endevent>>
		<<npc Whitney>><<person1>>
		Whitney leans back in <<his>> seat, looking bored. <<He>> reaches up to your head and twirls your hair between <<his>> fingers.
		<br><br>

		<<link [[Make out|Maths Whitney Makeout]]>><<npcincr Whitney love 1>><</link>><<glove>>
		<br>
		<<link [[Keep working|Maths Whitney Work]]>><<mathsskill 1>><</link>><<gmaths>>
		<br>
	<<elseif $whitneymaths is "active">>
		<<endevent>>
		<br><br><<npc Whitney>><<person1>>
		A crumpled ball of paper bounces off your head. You turn and see Whitney grinning at you along with a couple of other delinquents.
		<br><br>

		<<link [[Read it|Maths Whitney Read]]>><</link>>
		<br>
		<<link [[Throw it back|Maths Whitney Throw]]>><<def 1>><</link>>
		<br>
		<<link [[Throw it in the bin|Maths Whitney Bin]]>><</link>>
		<br>
	<<else>>
		A few delinquents use the opportunity to harass you.
		<br><br>

		<<link [[Tell them to stop|Maths Event1]]>><<detention 2>><</link>><<gdelinquency>>
		<br>
		<<link [[Endure it|Maths Event1]]>><<stress 2>><<set $phase to 1>><</link>><<gstress>>
		<br>
	<</if>>
<<elseif $rng gte 71 and $whitneymaths is "active">>
	<<npc Whitney>><<person1>><<generateyv2>>
	A pen bounces off your head. It's Whitney. <<He>> reclines in <<his>> seat, rummaging through a neighbouring <<person2>><<persons>> pencil case for more stationery to chuck at you. The <<person>> is too cowed to stop <<person1>><<him>>.
	<br><br>

	<<link [[Tolerate it|Maths Whitney Tolerate]]>><<stress 6>><</link>><<gstress>>
	<br>
	<<link [[Notify River|Maths Whitney River]]>><<npcincr Whitney love -1>><<status -10>><</link>><<lcool>><<llove>>
	<br>
	<<link [[Retaliate|Maths Whitney Retaliate]]>><<def 1>><<detention 1>><<trauma -6>><</link>><<gdelinquency>><<ltrauma>>
	<br>
<<elseif $rng gte 71 and $whitneymaths is "seat">>
	<<npc Whitney>><<person1>>
	Whitney leans close, <<his>> breath tickling your ear as <<he>> speaks. "My <<if $NPCList[0].penis isnot "none">>cock<<else>>pussy<</if>> needs your mouth," <<he>> whispers. "Be a good <<girlfriend>> and get under the desk."
	<br><br>

	<<link [[Comply grudgingly|Maths Whitney Oral]]>><<set $phase to 0>><<trauma 6>><<stress 6>><<npcincr Whitney dom 1>><<set $submissive += 1>><<set $molestationstart to 1>><</link>><<gtrauma>><<gstress>><<gdom>>
	<br>
	<<if $promiscuity gte 55>>
	<<link [[Comply enthusiastically|Maths Whitney Oral]]>><<set $phase to 1>><<npcincr Whitney dom 1>><<set $submissive += 1>><<set $sexstart to 1>><</link>><<promiscuous4>><<gdom>>
	<br>
	<</if>>
	<<if $whitneyromance is 1>>
	<<link [[Comply lovingly|Maths Whitney Oral]]>><<set $phase to 2>><<trauma 6>><<npcincr Whitney dom 1>><<npcincr Whitney love 1>><<set $submissive += 1>><<set $sexstart to 1>><</link>><<promiscuous1>><<gtrauma>><<glove>><<gdom>>
	<br>
	<</if>>
	<<link [[Refuse|Maths Whitney Oral Refuse]]>><<stress 6>><<npcincr Whitney dom -1>><</link>><<gstress>><<ldom>>

<<elseif $rng gte 61>>
<<npc River>><<generates2>><<person2>>
River has you come to the front of the class to solve an equation on the whiteboard.

  <<if $worn.lower.skirt is 1>>
  As you write on the board, a <<person>> reaches over with a ruler and lifts up your skirt!
	<<if $worn.under_lower.type.includes("naked")>>
		A cheer erupts from the class as they see you're not wearing underwear. You brush the ruler away and fix your $worn.lower.name as your face heats up. River turns to the class to see what's causing the commotion. Seeing the <<person2>><<person>> holding the ruler, <<person1>><<he>> opens <<his>> mouth to chide but is interrupted. "But <<sircomma>> <<pshes>> not wearing any underwear!"
		<br><br>

		The class erupts into another cheer as River looks at you. "Is this true? That's against school rules. Sit down."
		<<gtrauma>><<gstress>><<gdelinquency>><<gcool>><<trauma 2>><<stress 2>><<detention 2>><<status 1>>
		<br><br>

		<<if $whitneymaths is "active" or $whitneymaths is "seat">>
			Whitney smirks.
			<br><br>
		<</if>>

	<<else>>
		You brush the ruler away and fix your $worn.lower.name, but not before the class get a good look at your <<if $worn.under_lower.anus_exposed is 1>> bare ass<<else>>$worn.under_lower.name<</if>>.<<gtrauma>><<gstress>><<trauma 1>><<stress 1>>
		<br><br>
		
		<<if $whitneypantiescheck is 1 and $whitneymaths is "active">>
			Whitney glares at you, but doesn't say anything. This won't be the end of it.
			<<set $whitneypantiesmaths to "seen">>
			<br><br>
		<</if>>

	<</if>>
  <<else>>
  As you write on the board, a <<person>> reaches over with a ruler and tugs down your $worn.lower.name!
	<<if $worn.under_lower.type.includes("naked")>>
		A cheer erupts from the class as they see you're not wearing underwear. You brush the ruler away and fix your $worn.lower.name as your face heats up. River turns to the class to see what's causing the commotion. Seeing the <<person2>><<person>> holding the ruler, <<person1>><<he>> opens <<his>> mouth to chide but is interrupted. "But <<sircomma>> <<pshes>> not wearing any underwear!"
		<br><br>

		The class erupts into another cheer as River looks at you. "Is this true? That's against school rules. Sit down."<<gtrauma>><<gstress>><<gdelinquency>><<gcool>><<trauma 2>><<stress 4>><<detention 2>><<status 1>>
		<br><br>

		<<if $whitneymaths is "active" or $whitneymaths is "seat">>
			Whitney smirks.
			<br><br>
		<</if>>

	<<else>>
		You brush the ruler away and fix your $worn.lower.name, but not before the class get a good look at your <<if $worn.under_lower.anus_exposed is 1>> bare ass<<else>>$worn.under_lower.name<</if>>.<<gtrauma>><<gstress>><<trauma 1>><<stress 2>>
		<br><br>
		
		<<if $whitneypantiescheck is 1 and $whitneymaths is "active">>
			Whitney glares at you, but doesn't say anything. This won't be the end of it.
			<<set $whitneypantiesmaths to "seen">>
			<br><br>
		<</if>>

	<</if>>
  <</if>>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>

<<elseif $rng gte 51 and $whitneymaths is "active">>
	River is demonstrating equations on the whiteboard. Whitney uses the opportunity to force a <<generatey1>><<person1>><<person>> behind you to relocate, before taking <<his>> seat.
	<br><br>

	<<endevent>><<npc Whitney>><<person1>>

	Something jabs your waist. Whitney sits with an innocent smile and a protractor compass in <<his>> hand. You turn back to the front, and <<he>> jabs you again.<<gpain>><<pain 6>>
	<br><br>

	<<link [[Endure|Maths Whitney Protractor Endure]]>><<stress 6>><<pain 6>><</link>><<gstress>><<gpain>>
	<br>
	<<link [[Tell River|Maths Whitney Protractor Tell]]>><<status -10>><<mathsskill 6>><<npcincr Whitney love -1>><</link>><<llove>><<lcool>><<gmaths>>
	<br>
	<<link [[Retaliate|Maths Whitney Protractor Retaliate]]>><<detention 4>><<status 1>><<npcincr Whitney dom -1>><</link>><<gdelinquency>><<ldom>><<gcool>>
	<br>

<<elseif $rng gte 51 and $whitneymaths is "seat">>
	You hear shouting outside the class. River leaves to investigate.
	<br><br>

	<<npc Whitney>><<person1>><<generatey2>>
	"You looking at my <<girlfriend>>?" Whitney says to your side. <<Hes>> glaring at a <<person2>><<person>> behind you.
	<br>
	"N-no," the <<person2>><<person>> replies. Whitney isn't impressed. <<person1>><<He>> jumps to <<his>> feet and lunges over the <<person2>><<persons>> desk, grasping <<him>> by the collar.
	<br>
	"<<pShes>> mine, <<person2>><<if $pronoun is "m">>pervert<<else>>slut<</if>>," <<person1>><<he>> hisses. "Now I'm gonna make sure you remember it."
	<br><br>

	<<set $rng to random(1, 100)>>
	<<if $rng gte 81>>
		The <<person2>><<person>> looks terrified.
	<<elseif $rng gte 61>>
		The <<person2>><<person>> looks defiant.
	<<elseif $rng gte 41>>
		The <<person2>><<person>> looks angry.
	<<elseif $rng gte 21>>
		The <<person2>><<person>> looks scared.
	<<else>>
		The <<person2>><<person>> looks confused.
	<</if>>
	<br><br>

	<<link [[Watch|Maths Whitney Watch]]>><<npcincr Whitney dom 1>><</link>><<gdom>>
	<br>
	<<link [[Intervene|Maths Whitney Intervene]]>><<famegood 10>><<npcincr Whitney dom -1>><<trauma -6>><</link>><<ldom>><<ltrauma>>
	<br>
	<<link [[Encourage|Maths Whitney Encourage]]>><<npcincr Whitney love 1>><<detention 30>><</link>><<glove>><<gdelinquency>>
	<br>

<<elseif $rng gte 41>><<set $rng to random(1, 100)>>

<<npc River>><<person1>>

River stands beside your table. "I need you to deliver a message for me. To the head." <<He>> hands you a sealed envelope.
<br><br>

You leave the classroom.
<br><br>
<<endevent>>
<<link [[Open the envelope|Maths Note]]>><<detention 6>><<set $phase to 1>><</link>><<gdelinquency>>
<br>
<<link [[Just deliver it|Maths Note]]>><</link>>
<br>

<<elseif $rng gte 31 and ($whitneymaths is "active" or $whitneymaths is "seat")>>

	<<npc Whitney>><<person1>>
	"It's too hot," Whitney insists, pulling <<his>> shirt over <<his>> head,
	<<if $pronoun is "f">>
		leaving <<him>> with just a white bra covering <<his>> $NPCList[0].breastsdesc.
	<<else>>
		leaving <<him>> bare chested.
	<</if>>
	"Much better." <<He>> says. Someone giggles.
	<br><br>

	<<if !$worn.upper.type.includes("naked") and $whitneymaths is "seat">>
		<<He>> turns to face you, and grasps the hem of your $worn.upper.name. "Let me help you," <<he>> says, trying to pull it up.
		<br><br>

		<<if $worn.under_upper.type.includes("naked") and ($player.gender is "f" or $player.gender_appearance is "f")>>
			You blush. You're not wearing anything underneath!
			<br><br>
			<<if $exhibitionism gte 15>>
				<<link [[Allow|Maths Whitney Shirt Allow]]>><<npcincr Whitney love 1>><<npcincr Whitney dom 1>><<npcincr Whitney lust 1>><<detention 4>><</link>><<exhibitionist3>><<gdelinquency>><<glove>><<gdom>><<glust>>
				<br>
			<</if>>
		<<elseif $player.gender is "f" or $player.gender_appearance is "f">>
			You blush.
			<br><br>
			<<link [[Allow|Maths Whitney Shirt Allow]]>><<npcincr Whitney love 1>><<npcincr Whitney dom 1>><<npcincr Whitney lust 1>><<detention 4>><</link>><<exhibitionist1>><<gdelinquency>><<glove>><<gdom>><<glust>>
			<br>
		<<else>>
			<<link [[Allow|Maths Whitney Shirt Allow]]>><<npcincr Whitney love 1>><<npcincr Whitney dom 1>><<npcincr Whitney lust 1>><<detention 4>><</link>><<gdelinquency>><<glove>><<gdom>><<glust>>
			<br>
		<</if>>
		<<link [[Resist|Maths Whitney Shirt Resist]]>><<stress 6>><</link>><<gstress>>
		<br>
	<<else>>
		<<endevent>><<npc River>><<person1>>
		River turns to investigate the source of the giggles. <<He>> almost swoons when <<he>> sees Whitney without a school shirt, and steadies <<himself>> against <<his>> desk. "O-out," <<he>> manages. "And put your shirt on."
		<br><br>
		<<endevent>>
		<<npc Whitney>><<person1>>
		Whitney rolls <<his>> eyes, but rises to <<his>> feet. <<He>> doesn't wear the shirt, instead slinging it over <<his>> shoulder as <<he>> swaggers from the room.
		<br><br>

		<<set $whitneymaths to "sent">>

		<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
		<br>
	<</if>>
<<elseif $rng gte 21>><<set $rng to random(1, 100)>>
  <<if $maths lte 499>>
  <<generates1>><<person1>>
River has a studious <<person>> help you with the material.
	<<if $cool gte (($coolmax / 5) * 2)>>
	<<He>> is happy to help, and your understanding of the material improves.<<lstress>><<stress -4>><<gmaths>><<mathsskill>>
	<br><br>

	<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
	<<elseif $cool gte ($coolmax / 5)>>
	<<He>> doesn't seem particularly happy with the arrangement, but you do learn something.<<gmaths>><<mathsskill>>
	<br><br>

	<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
	<<else>>
	<<He>> is clearly embarrassed to be seen with you; <<he>> never looks at you, and rarely speaks. You don't learn anything.
	<br><br>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
	<</if>>
  <<else>><<generates1>>
	<<if $eventsmaths4 is 1 and $rng gte 71>>
As you are doing well in this class, River has you sit with a <<person1>><<person>> who isn't doing so well, and instructs you to help them. The <<person>> is not at all happy with the arrangement. After a few minutes of trying to explain the material to them, you feel a hand grope your butt.
<br><br>

<<link [[Ignore it|Maths Event3]]>><<trauma 2>><<stress 2>><<arousal 200>><<set $phase to 3>><<set $eventsmaths4 to 1>><</link>><<gtrauma>><<gstress>><<garousal>>
<br>
<<link [[Move away|Maths Event3]]>><<detention 2>><<set $phase to 1>><</link>><<gdelinquency>>
<br>
<<if $whitneymaths is "seat">>
	<<link [[Tell Whitney|Maths Event3 Whitney]]>><<npcincr Whitney dom 1>><</link>> | <span class="purple">+ Whitney's Dominance</span>
	<br>
<</if>>

	<<elseif $rng gte 41>><<set $eventsmaths4 to 1>>
As you are doing well in this class, River has you sit with a <<person1>><<person>> who isn't doing so well, and instructs you to help them. The <<person>> is not at all happy with the arrangement. After a few minutes of trying to explain the material to them, you feel a hand grope your butt.
<br><br>

<<link [[Ignore it|Maths Event3]]>><<trauma 2>><<stress 2>><<arousal 200>><<set $phase to 2>><</link>><<gtrauma>><<gstress>><<garousal>>
<br>
<<link [[Move away|Maths Event3]]>><<detention 2>><<set $phase to 1>><</link>><<gdelinquency>>
<br>
<<if $whitneymaths is "seat">>
	<<link [[Tell Whitney|Maths Event3 Whitney]]>><<npcincr Whitney dom 1>><</link>> | <span class="purple">+ Whitney's Dominance</span>
	<br>
<</if>>

	<<elseif $rng gte 1>>
As you are doing well in this class, River has you sit with a <<person1>><<person>> who isn't doing so well, and instructs you to help them. The <<person>> is not at all happy with the arrangement, but they listen to you without incident.
<br><br>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
	<</if>>
  <</if>>
<<else>>
<<npc River>><<person1>><<generates2>>
While River's back is turned, someone throws a pen at <<his>> back. <<He>> stops speaking, and slowly turns to face the class. <<person2>>The <<person>> sat behind you points at you. "<<pShe>> did it!"
<br><br>
  <<if $delinquency gte 400>>
	<<if $whitneymaths is "seat">>
		River looks at you. <<person1>>"I should have known it would be you. Wait outside." You know arguing won't help, so you stand to leave.<<gstress>><<stress 2>>
		<br><br>

		Whitney stands beside you. "Sit down," River says. Whitney ignores <<him>> and leaves the class behind you.
		<br><br>
	<<else>>
		River looks at you. <<person1>>"I should have known it would be you. Wait outside." You know arguing won't help, so you stand and leave.<<gstress>><<stress 2>>
		<br><br>
	<</if>>

  <<link [[Next|Maths Event4]]>><<endevent>><</link>>
  <<else>>
  <<person1>>River is not fooled by the <<person2>><<personcomma>> and sends them out.
  <br><br>

  <<link [[Next|Maths Lesson]]>><<endevent>><</link>>
  <</if>>
<</if>>
<</nobr>><</widget>>

<<widget "eventsmathssafe">><<nobr>>
<<if $mathsproject is "none" and $yeardays gte 23 and $scienceprojectdays isnot (21 + (7 - $weekday))>><<set $mathsproject to "ongoing">><<set $mathsprojectdays to (21 + (7 - $weekday))>>
<<mathsprojectstart>>
<<npc River>><<person1>><<generatey2>>"Everyone listen," River announces. <<He>> knocks on <<his>> desk. "That includes you at the back. I have something special for you today. We hold an annual competition for students in your year." This grabs a <<person2>><<person>> at the front's attention. <<He>> sits upright.
<<if $whitneymaths is "active" or $whitneymaths is "seat">>
	Whitney is less impressed, and reclines in
	<<if $NPCName[$NPCNameList.indexOf("Whitney")].pronoun is "m">>
		his seat with an open textbook resting on his face.
	<<else>>
		her seat with an open textbook resting on her face.
	<</if>>
<</if>>
<br><br>

"You have a few weeks to solve it," River continues as <<person1>><<he>> hands out booklets. "Whoever provides the most intelligent answer wins. We don't expect anyone to find the solution. That's not the point." <<He>> places a black and white booklet in front of you. You open it. It's incomprehensible.
<br><br>

The <<person2>><<persons>> arm shoots up. "Yes?" River asks.
<br><br>

"You haven't covered this," the <<person>> says. <<His>> voice quivers. You're relieved that you're not alone.
<br><br>

River smiles. "Well observed," <<person1>><<he>> says. "You're all on equal footing. Even if you haven't paid attention in the past. Participation is optional. There's a reward for the winner, but don't prioritise it over your school work."
<br><br>

<<He>> hands out the last of the booklets. "The problem was made for this competition, so don't think you can find the answer somewhere. And don't ask your parents for help," <<he>> adds. <<He>> avoids looking in your direction. "We can always tell. The competition will be held at the town hall, where you will be judged. Good luck."
<br><br>

<span class="gold">Maths competition added to journal.</span>
<br><br>

<<endevent>>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>

<<elseif $rng gte 81>>
<<npc River>><<person1>>
River gives the class some equations to solve while <<he>> manages paperwork.

<<if $whitneymaths is "seat" or $whitneymaths is "active">>
	<<endevent>>
	<<npc Whitney>><<person1>>
	Even Whitney keeps quiet, though <<he>> might just be asleep.
<<else>>
	Even the delinquents keep quiet, careful to avoid River's attention.
<</if>>
<br><br>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>

<<elseif $rng gte 71>>
  <<generates1>><<generates2>><<person1>><<set _coinflip to random(0,1)>>
  River assigns a high-performing student, a <<person>>, to support one of the struggling delinquents near you.
  <br><br>
  <<if $rng % 3 is 0>>
	It goes well. Both students get along and seem to benefit from the arrangement.
  <<else>>
	It is a disaster. The delinquent, a <<person2>><<person>>, resents the <<person1>><<persons>> help, and soon starts bullying <<himstop>> Out of the corner of your eye you watch as things escalate out of control.
	<<if _coinflip>>
	River calls an end to the session before things go too far.
	<<else>>
	River investigates when the <<person1>><<person>> screams. <<if $rng % 2>>River sends out the perpetrator.<<else>>Somehow the <<person1>><<person>> ends up being blamed and is given detention.
	<</if>>
	<</if>>
  <</if>>
  <br><br>

  <<link [[Next|Maths Lesson]]>><<endevent>><</link>>

<<elseif $rng gte 61>>
<<npc River>><<person1>>
River demonstrates equations on a whiteboard and occasionally asks questions, picking on students <<he>> thinks aren't paying attention.
<br><br>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>

<<elseif $rng gte 51>>
	<<if $whitneymaths is "seat" or $whitneymaths is "active">>
		Whitney goads a <<generatey1>><<person1>><<person>> into flinging a pen at River's back. The <<person>> is sent out of the room after some argument.
	<<else>>
		Someone across the class flings a pen at River's back. After some argument, the perpetrator is sent out of the room and class resumes.
	<</if>>
	<br><br>

	<<link [[Next|Maths Lesson]]>><<endevent>><</link>>

<<elseif $rng gte 41>>
<<npc River>><<person1>>
River demonstrates equations on a whiteboard and occasionally asks questions, picking on students <<he>> thinks aren't paying attention. This time, <<he>> picks on you.
<br><br>

  <<if $maths gte 500>>
  You know the answer, but answering such a tough question correctly will make the other students think you strange.
  <br><br>

  <<link [[Answer|Maths Event2]]>><<status -10>><</link>><<lcool>>
  <br>
  <<link [[Pretend you don't know|Maths Event2]]>><<detention 2>><<stress 2>><<set $phase to 1>><</link>><<gstress>><<gdelinquency>>
  <<else>>
  The question is quite difficult and beyond you, so you tell <<him>> you don't know.
  <br><br>

"Of course you don't." Fortunately <<he>> doesn't belabour the point, and continues lecturing.
<<set $stress += 20>><<gstress>>
<br><br>

  <<link [[Next|Maths Lesson]]>><<endevent>><</link>>
  <</if>>

<<elseif $rng gte 35>>

You're called up to the front to answer a question. You walk up and write your answer on the board. River seems satisfied. You sit back down, but feel a sharp pain. Standing up and reaching back, you feel a tack stuck in your <<bottomstop>>
<<pain 2>><<stress 6>><<gpain>><<gstress>>
<br><br>

	<<if $whitneymaths is "active">>
		You remove it and sit back down. You hear Whitney laugh behind you.
	<<elseif $whitneymaths is "seat">>
		You remove it and sit back down. Whitney wears an innocent smile.
	<<else>>
		You remove it and sit back down. You hear a couple of delinquents laughing behind you.
	<</if>>
	<br><br>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>

<<elseif $rng gte 31>>

	<<if $whitneymaths is "seat">>
		The class studies trigonometry, filling worksheets while River paces the room. Whitney starts a covert war with another student. They stab each other with compass points.
	<<elseif $whitneymaths is "active">>
		The class studies trigonometry, filling worksheets while River paces the room. Whitney and the other delinquents covertly stab each other with compass points.
	<<else>>
		The class studies trigonometry, filling worksheets while River paces the room. Some students covertly stab each other with compass points.
	<</if>>
<br><br>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>

<<elseif $rng gte 21>>
<<npc River>><<person1>>
The class studies geometry, filling worksheets while River paces around the room, looking over shoulders and pointing out every little mistake.
<br><br>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>

<<elseif $rng gte 11>>
<<generates1>><<generates2>><<person1>>
A <<person>> is called to the front of the class to solve an equation on the board. As <<he>> is writing, a <<person2>><<person>> leans forward and uses a ruler to flash the unwitting <<person1>><<if $pronoun is "m">>boy's<<else>>girl's<</if>> underwear to the class.
<<if ($rng % 4) is 0>>
However it turns out the <<person>> isn't wearing any, leading to lots of cheering, and a detention for both the humiliated <<if $pronoun is "m">>boy<<else>>girl<</if>> and the <<person2>><<person>> with the ruler.
<<else>>The <<if $pronoun is "m">>boy<<else>>girl<</if>> slaps away the ruler, and River fixes the <<person2>><<person>> with a glare.
<br><br>
The class goes on.
<</if>>
<br><br>
<<link [[Next|Maths Lesson]]>><<endevent>><</link>>

<<else>>
<<npc River>><<person1>>
River demonstrates equations on a whiteboard before handing out worksheets for the class to complete.
<br><br>

<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
<</if>>
<</nobr>><</widget>>

<<widget "mathsstart">><<nobr>>
<<endevent>>
<<if $NPCName[$NPCNameList.indexOf("Whitney")].state is "active" or $NPCName[$NPCNameList.indexOf("Whitney")].state is "rescued">>
	<<set _whitney_maths_chance to (($NPCName[$NPCNameList.indexOf("Whitney")].love / 3) + ($NPCName[$NPCNameList.indexOf("Whitney")].dom * 3) + ($NPCName[$NPCNameList.indexOf("Whitney")].lust / 3))>>
<</if>>
<<if ($NPCName[$NPCNameList.indexOf("Whitney")].state is "active" or $NPCName[$NPCNameList.indexOf("Whitney")].state is "rescued") and $whitneymaths is undefined>>

	<<npc River>><<person1>>Whitney swaggers in. "So you decided to show up," River says, pointing at an empty seat at the front. "Tuck your shirt in."
	<br><br>
	<<endevent>>
	<<if $whitneyromance is 1>>
		<<set $whitneymaths to "seat">>
		<<npc Whitney>><<person1>> Whitney rolls <<his>> eyes, and instead takes a seat beside you. <<He>> tucks just the front of <<his>> shirt into <<his>> <<if $pronoun is "f">>skirt<<else>>trousers<</if>> as <<he>> sits.
		<br><br>
	<<else>>
		<<set $whitneymaths to "active">>
		<<npc Whitney>><<person1>> Whitney rolls <<his>> eyes, and instead takes a seat at the back. <<He>> tucks just the front of <<his>> shirt into <<his>> <<if $pronoun is "f">>skirt<<else>>trousers<</if>> as <<he>> sits.
		<br><br>
	<</if>>
	<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
	<br>
<<elseif ($NPCName[$NPCNameList.indexOf("Whitney")].state is "active" or $NPCName[$NPCNameList.indexOf("Whitney")].state is "rescued") and _whitney_maths_chance gte random(1, 100)>>
	<<if $whitneyromance is 1>>
		<<set $whitneymaths to "seat">>
		<<npc Whitney>><<person1>>Whitney enters the class. <<He>> shoves past a <<generatey2>><<person2>><<person>> and sits beside you.
		<br><br>
	<<else>>
		<<set $whitneymaths to "active">>
		<<npc Whitney>><<person1>>Whitney enters the class. <<He>> takes a seat at the back.
		<br><br>
	<</if>>
	<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
	<br>
<<else>>
	<<if $whitneymaths isnot undefined>>
		<<set $whitneymaths to "absent">>
	<</if>>
	<br>
	<<link [[Next|Maths Lesson]]>><<endevent>><</link>>
	<br>
<</if>>
<</nobr>><</widget>>