<<effects>>

<<set $livestock_horse to 2>>
<<generate1>><<person1>>
<<set $livestock_horse_rescued to $pronoun>>
<<endevent>>
The staggers away from you. It holds its leg in the air, but then presses it against the ground. It starts walking, tentative at first, then with more confidence. The walk becomes a trot, then a run, then a gallop. It runs around the field, startling the cattle.
<br><br>

The other horses seem much less agitated. One of them licks your neck from behind.
<br><br>

<<link [[Next|Livestock Field]]>><</link>>
<br>