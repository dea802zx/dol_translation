<<set $outside to 0>><<set $location to "town">><<effects>>

You walk over. The <<person1>><<person>> holds an arm out. "Don't get in the way <<girlcomma>>" <<he>> says.
<br><br>

"But I want to see," you say, ignoring <<himstop>> You approach the oven, and bend over to look inside. "I said-" the <<person>> begins, but <<he>> cuts off when you start wiggling your <<bottomstop>> The others stop working. You can feel their eyes on you.
<<exhibitionism1>><<fameexhibitionism 6>>
<br><br>

<<if $promiscuity gte 55>>
<span class="lewd">An idea strikes you.</span>
<br><br>

<<link [[Ask for their Cream|Photography Chef Cream]]>><</link>><<promiscuous4>>
<br>
<</if>>
<<link [[Leave them to work|Photography Chef Leave]]>><</link>>
<br>