<<effects>>

You lay down on your bed and hope you can endure the experience. Doing anything productive right now is out of the question. It's a struggle to even think straight, but you do your best to distract yourself by counting things. It's going well until you glance at the clock and realise only a couple of minutes have passed.
<br><br>

Your muscles ache from the tension by the time the aches fade. You realise that your teeth have been clenched too. You're still aroused and frustrated. At least you can think straight again.
<br><br>

<<link [[Next|Bedroom]]>><</link>>
<br>