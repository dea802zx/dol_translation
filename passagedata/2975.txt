<<set $outside to 0>><<set $location to "town">><<effects>>
<<if $worn.lower.skirt is 1>>
	You feel a tug on your $worn.lower.name. You look down just in time to see it flare up, pulled by some invisible force. You push down the front of the hem fast enough to prevent your <<undies>> being revealed, but you feel cool air on your <<bottomstop>>
	<br><br>
	You hear giggles and murmuring from the audience. <<npc River>><<person1>>River puts <<his>> hands over <<his>> eyes. One of the suited guests raises a hand to their mouth. The other looks away, blushing. Leighton is smiling.
	<br><br>
	You realise how little you managed to keep down. Enough to keep your privates concealed, but everyone can see what you're wearing beneath. You feel blood rush to your face.
	<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
	<br><br>
	<<endevent>>
	<<npc Whitney>><<person1>>
	<<if $worn.under_lower.name is "naked">>
		"No underwear," Whitney says. "Perfect." <<Hes>> tugging at the air, as if holding something.
		<br><br>
	<<else>>
		"Nice underwear," Whitney says. "And you're so eager to show it off." <<Hes>> tugging at the air, as if holding something.
		<br><br>
	<</if>>
	You feel around with your free hand and find it. A wire is attached to your skirt. It must be looped around something in the rafters. The tugging at the back and sides suggests there are more than one.
	<br><br>
	Whitney's friends line up behind <<him>> and grasp at the air, trying to find the wire. They are preparing for a mighty pull.
	<br><br>
	<<link [[Protect your dignity|Maths Competition Protect Skirt]]>><<stress 6>><</link>><<gstress>>
	<br>
	<<link [[Finish the presentation|Maths Competition Finish Skirt]]>><<stress 12>><<trauma 6>><<set $mathschance += 10>><</link>><<gtrauma>><<ggstress>> | <span class="gold">+ Success chance</span>
	<br>
<<else>>
	You feel a tug on your $worn.lower.name. You look down just in time to see it lurch down, pulled by some invisible force. You grab the front of the hem fast enough to prevent your <<undies>> being revealed, but you feel cool air on your <<bottomstop>>
	<br><br>
	You hear giggles and murmuring from the audience. <<npc River>><<person1>>River puts <<his>> hands over <<his>> eyes. One of the suited guests raises a hand to their mouth. The other looks away, blushing. Leighton is smiling.
	<br><br>
	You realise how little you managed to keep up. Enough to keep your privates concealed, but everyone can see what you're wearing beneath. You feel blood rush to your face.
	<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
	<br><br>
	<<endevent>>
	<<npc Whitney>><<person1>>
	<<if $worn.under_lower.name is "naked">>
		"No underwear," Whitney says. "Perfect." <<Hes>> tugging at the air, as if holding something.
		<br><br>
	<<else>>
		"Nice underwear," Whitney says. "And you're so eager to show it off." <<Hes>> tugging at the air, as if holding something.
		<br><br>
	<</if>>
	You feel around with your free hand and find it. A wire is attached to your bottoms. It must be looped around something beneath the stage. The tugging at the back and sides suggests there are more than one.
	<br><br>
	Whitney's friends line up behind <<him>> and grasp at the air, trying to find the wire. They are preparing for a mighty pull.
	<br><br>
	<<link [[Protect your dignity|Maths Competition Protect Shorts]]>><<stress 6>><</link>><<gstress>>
	<br>
	<<link [[Finish the presentation|Maths Competition Finish Shorts]]>><<stress 12>><<trauma 6>><<set $mathschance += 10>><</link>><<gtrauma>><<ggstress>> | <span class="gold">+ Success chance</span>
	<br>
<</if>>