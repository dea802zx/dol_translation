<<set $outside to 1>><<set $location to "town">><<effects>>
<<pass 20>>
"We're going to Connudatus Street," <<he>> continues. "Got a plan I thought up, and you're going to be a part of it."
<br><br>
They bring you to the back alleys of Connudatus Street, out of sight from the public.
<br><br>
"You're gonna distract some poor rich sod while I rob them. But you're going to need to be a bit more eye-catching for this to work. Strip."
<br><br>

<<link [[Go along with it|Bully Rob]]>><<trauma 6>><<fameexhibitionism 5>><<stress 6>><<npcincr Whitney dom 1>><</link>><<gtrauma>><<gstress>><<gdom>>
<br>
<<if $exhibitionism gte 75>>
<<link [[Strip Willingly|Willing Rob]]>><<fameexhibitionism 5>><<npcincr Whitney dom 1>><<npcincr Whitney love 1>><</link>><<exhibitionist5>><<glove>><<gdom>>
<br>
<</if>>
<<if $whitneyromance is 1>>
<<link [[Ask Whitney to Strip|Bully Rob Reversal]]>><<npcincr Whitney dom -1>><</link>><<ldom>>
<br>
<</if>>
<<link [[Refuse|Bully Strip Rob Refuse]]>><<set $fightstart to 1>><</link>>
<br>