<<set $outside to 0>><<set $location to "town">><<effects>>
You knock your paper on the floor. It slides along, and rests in front of Leighton. "Oops," you say. You saunter to the edge of the stage, turn around, and bend over. You pretend to have difficulty picking it up, and wiggle your <<bottom>> at the audience during your "attempts."
<<exhibitionism3>>
You hear giggling from the crowd. You look between your legs. River and the two guests avert their eyes. River looks pale. Leighton leans forward and stares.
<br><br>
You pick up the paper and return to the microphone.
<br><br>
<<link [[Next|Maths Competition Proceed]]>><</link>>
<br>