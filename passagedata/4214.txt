<<set $outside to 0>><<set $location to "forest_shop">><<effects>>
"It suits you," you say. "You look like a scary witch."
<br><br>
"Really?" <<he>> asks, smiling. <<He>> turns back to the mirror and swishes the skirt around.
<br><br>
<<if $money gte 8000>>
	<<link [[Buy the costume for Robin|Robin Forest Witch Buy]]>><<npcincr Robin love 3>><<set $money -= 8000>><<set $halloween_robin_costume to "witch">><</link>><<gglove>>
	<br>
<<else>>
	You don't have enough money to buy the costume. (£80)
	<br><br>
<</if>>
<<link [[Ask Robin to try on the boy's vampire costume|Robin Forest Vampire]]>><</link>>
<br>
<<link [[Walk home|Robin Forest Shop Home]]>><</link>>
<br>