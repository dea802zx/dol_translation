<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
You sit down and enjoy breakfast with the other patients.
<<if $leftarm is "bound" and $rightarm is "bound">>
	Your arms are bound, so you find yourself pressing your face against your plate.
<</if>>
The food is good, and leaves you feeling warm and happy. It's not just you. The other patients seem more social.
<br><br>
<<if $exposed is 2>>
	Your <<lewdness>> on display makes you the centre of attention.
	<<exhibitionism5>>
<<elseif $exposed is 1>>
	Your <<lewdness>> on display makes you the centre of attention.
	<<exhibitionism3>>
<<else>>
<</if>>
<<asylumevents>>