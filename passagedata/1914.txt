<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "domus">><<set $lock to 400>>
The safe doesn't look too fancy.
<br><br>
<<if $skulduggery gte $lock>>
	<span class="green">The lock looks easy to pick.</span>
	<br><br>
	<<link [[Break in (0:05)|Domus House Safe Open]]>><<pass 5>><<crimeup 1>><</link>><<crime>>
	<br>
<<else>>
	<span class="red">The lock looks beyond your ability to pick.</span><<skulduggeryrequired>>
	<br><br>
<</if>>
<<link [[Leave|Domus Street]]>><</link>>
<br>