<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You mock the teacher. "A white lab coat is a good idea <<sircomma>> it'll hide the stains." Laughter erupts among the seated students.
<br><br>

Sirris looks exasperated. "Right. I'll have to inform the head of your behaviour, you know. Please, just go."
<<gdelinquency>>
<br><br>
<<detention 1>><<npcincr Sirris love -1>>
<<link [[Next|Hallways]]>><<endevent>><</link>>