<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<if $submissive gte 1150>>
	"You could have burned me," you say.
<<elseif $submissive lte 850>>
	"Someone as clumsy as you shouldn't work here," you say.
<<else>>
	"You did that on purpose," you say.
<</if>>
<<He>> bows <<his>> head while the audience whisper and chuckle amongst themselves. Sam gives you a disapproving look.
<br><br>

The rest of the shift passes uneventfully. You earn £5.
<<set $money += 500>><<pass 1 hour>>
<br><br>

<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>