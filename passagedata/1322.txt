<<effects>><<set $rescued += 1>>
The dumpster opens fully, someone heard your cry. <<generate2>><<generate3>>A <<person2>><<person>> and <<person3>><<person>> grab your shoulders and haul you out, slamming it shut behind you.
<br><br>
<<clothesontowel>>
The <<person1>><<person>> looks concerned. "Good thing we heard you. Are you OK?" <<tearful>> you nod and thank them for the rescue, before parting ways.
<br><br>
<<endcombat>>
<<residentialeventend>>