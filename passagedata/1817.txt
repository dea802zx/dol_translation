<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "danube">>

You are led to the sprawling back garden, the extent of which is concealed by several poorly-kept hedgerows. "Just do what you can. Don't be lazy though," <<He>> points at a kennel surrounded by bones. "Brutus is
<<if $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
	... Wait. What? What's going on with your <<if $leftarm is "bound" or $rightarm is "bound">>arms<</if>><<if ($leftarm is "bound" or $rightarm is "bound") and $feetuse is "bound">> and <</if>><<if $feetuse is "bound">>legs<</if>>? Are they tied up?"
	<br><br>
	<<He>> <<if $rightarm is "bound" or $leftarm is "bound">>spins you around to look at your bound arms<<elseif $feetuse is "bound">>crouches to look at your bound legs<</if>> and
	<<if $rng gte 25 and $bestialitydisable is "f">>
		growls with irritation. "Is this a joke to you? Wasting my time? What work can you do like that?"
		<br><br>

		<<He>> grabs your shoulder and marches you toward the exit. <<He>> stops when a <<person2>><<person>> appears at the side door. <<He>> beckons the <<person1>><<person>> over, and they exchange a few whispered words.
		<br>
		The <<person>> grabs your shoulder, and leads you back into the garden.
		<br><br>

		"Since you're being paid to work, the <<if $NPCList[1].pronoun is "m">>Lord<<else>>Lady<</if>> of the House has thought of one job you're perfectly equipped for."
		<br>
		By the kennel, <<he>> suddenly shoves you to the floor. "Brutus!" <<He>> whistles. You hear a heavy bounding run. A nose sniffs at your butt. "Hey Brutus! We've brought you a new friend!"
		<br><br>

		<<link [[Next|Danube Garden Bitch]]>><<set $molestationstart to 1>><<pass 10>><</link>>
		<br>
	<<else>>
		sighs with exasperation. "Well you're not going to get any work done like that are you?"
		<br><br>
		<span class="green">Your limbs fall free.</span>
		<br>
		<<unbind>>
		"I don't know what's wrong with young people nowadays. As I was saying..."
		<br><br>
		<<link [[Continue|Danube Garden]]>><</link>>
		<br>
	<</if>>
<<else>>
	watching."
	<br><br>

	You kneel in front of one of the hedges and get to work removing the weeds.

	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
		<<set $rng to random(1, 100)>>
		<<if $rng gte 81 and $hallucinations gte 2 and $tentacledisable is "f">>

			<<pass 2 hours>>
			You work at it for two hours, and barely seem to make a dent. It's almost as if the weeds grow back as fast as you remove them. You reach in to try to remove a large shrub at its roots, but your hand gets stuck. You try to free it with your other hand, but that gets stuck too. Green tendrils emerge, animated by some force.
			<br><br>

			<<link [[Struggle|Danube Tentacles]]>><<set $molestationstart to 1>><</link>>
			<br>
			<<link [[Soothe|Danube Garden Soothe]]>><</link>><<tendingdifficulty 1 1200>>
			<br>

		<<elseif $rng gte 61 and $bestialitydisable is "f">>

			<<pass 2 hours>>
			You work at it for two hours, and barely seem to make a dent. It's almost as if the weeds grow back as fast as you remove them. You're reaching in to remove a large shrub at its roots when something prods your <<bottomstop>> You turn to look, but it has already mounted you.
			<br><br>

			<<link [[Next|Danube Dog]]>><<set $molestationstart to 1>><</link>>
			<br>

		<<else>>

			<<pass 2 hours>>
			You work at it for two hours, and barely seem to make a dent. It's almost as if the weeds grow back as fast as you remove them. You notice movement above, and see a <<person2>><<person>> at the window, watching you. It's pretty creepy.
			<<gstress>><<stress 6>>
			<br><br>

			<<pass 2 hours>>
			Over the next two hours you barely seem to make a dent. It's almost as if the weeds grow back as fast as you remove them. Regardless, the <<person1>><<person>> seems satisfied, and hands you £50.
			<<physique 6>>
			<br><br>

			<<link [[Next|Danube Street]]>><<set $money += 5000>><<endevent>><</link>>
			<br>

		<</if>>

	<<else>>

		<<pass 4 hours>>
		You work at it for four hours, and barely seem to make a dent. It's almost as if the weeds grow back as fast as you remove them. Regardless, the <<person>> seems satisfied, and hands you £50.
		<<physique 6>>
		<br><br>

		<<link [[Next|Danube Street]]>><<set $money += 5000>><<endevent>><</link>>
		<br>
	<</if>>
<</if>>