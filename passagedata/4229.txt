<<set $outside to 0>><<set $location to "home">><<effects>>

You stand back and watch the destruction. There's no sign of the <<person6>><<personcomma>> but the front of <<his>> house is reduced to a ruin.
<br><br>

Satisfied, or bored, at last, Whitney grasps your arm and pulls you from the garden. <<person1>><<His>> friends fall in behind you. "Where to next?" <<he>> muses. <<His>> gaze fixes on a house with a particularly extravagant display.
<<if $halloween_trick_NPC>>
The one you visited with Robin
<</if>>
<br><br>

<<link [[Next|Whitney Trick 5]]>><<endevent>><</link>>
<br>