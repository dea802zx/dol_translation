<<set $outside to 0>><<effects>>
You squeeze <<him>> back, and hold <<him>> in your arms for a few moments. <<He>> buries <<his>> face in your shoulder.
<br><br>
The rest of the journey is uneventful. Robin stays close to you the whole way.
<br><br>
<<link [[Next|School Front Playground]]>><<endevent>><</link>>
<br>