<<set $outside to 0>><<set $location to "pub">><<effects>><<set $bus to "harvest">>
<<if $blackmoney lt 100>>
	You put the goods on the table. Landry appraises them with the relaxed demeanour of someone accustomed to their work. "Good job. You know where to find me if you get anything else."
	<br><br>
<<elseif $blackmoney lt 500>>
	You put the goods on the table. Landry appraises them with the relaxed demeanour of someone accustomed to their work. "Quite the haul. You know where to find me if you get anything else."
	<br><br>
<<else>>
	You put the goods on the table. Landry appraises them with the relaxed demeanour of someone accustomed to their work. "This is quite the haul, I hope you haven't bitten off more than you can chew. You know where to find me if you need help covering your tracks."
	<br><br>
<</if>>
You make £<<print $blackmoney>>.
<<set $money += $blackmoney * 100>><<set $blackmoney to 0>>
<br><br>
<<link [[Leave|Pub]]>><<endevent>><</link>>
<br>