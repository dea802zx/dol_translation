<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<ejaculation>>
	Someone shoves you face-first into the water. They scurry from the room.
	<br><br>
	<<tearful>> you climb from the bath.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Bathroom]]>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	The <<person1>><<person>> backs away from you, nursing an injury. The others step back too, looking unsure. <<He>> opens <<his>> mouth as if to speak, but instead turns and walks from the room. The others follow.
	<br><br>
	<<tearful>> you climb from the bath.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Bathroom]]>><</link>>
	<br>
<<else>>
	<<set $rescued += 1>>/*Unnecessary if rescue is impossible*/
	<<endevent>><<npc Bailey>><<person1>>
	Heavy footsteps respond to your cry, getting closer. The trio recoil and scramble for the door handle. They flee into the hallway outside, leaving the door wide open. Bailey enters. <<His>> eyes sweep the room before resting on you. "Did you see a spider?" <<he>> sneers. <<He>> steps closer and smacks you with the back of <<his>> hand. "Don't bother me again." <<He>> marches from the room, slamming the door behind <<himstop>>
	<<gpain>><<gtrauma>><<gstress>><<pain 20>><<trauma 6>><<stress 6>>
	<br><br>
	<<tearful>> you climb from the bath.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Bathroom]]>><</link>>
	<br>
<</if>>