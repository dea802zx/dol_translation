<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "domus">>
You take <<his>> arm and hoist yourself up and against <<himcomma>> forcing <<him>> to catch you in an embrace to stop you falling. You look <<him>> in the eyes. "I feel safe now," you say.
<<promiscuity1>>
<br><br>
<<He>> blushes. "I-I'm glad you're alright," <<he>> says as <<he>> slowly withdraws <<his>> arms from you, making sure you're steady. <<He>> glances around. "Here," <<he>> hands you a small cylinder. "Be discreet. It's not legal, and there's only enough for one use."
<<gspraymax>><<set $spraymax to 1>><<spray 5>>
<br><br>
"I need to get going. You be careful."
<br><br>
<<link [[Next|Domus Street]]>><<endevent>><<set $eventskip to 1>><</link>>