<<set $bus to "elk">><<set $outside to 0>><<effects>>

You throw one end of the rope over the edge of the hole, and tie the other around a sturdy girder. You tug it to make sure it's safe, then descend.
<br><br>
You hold the rope in your hands, and press your weight against the building with your feet. You stop to wait for the wind to die down now and then.
<br><br>
You soon reach the mist, and continue through it. The rope just about reaches the ground.
<br><br>
You follow one of the alleys in the direction of traffic, and soon emerge on Elk Street.
<br><br>

<<link [[Next|Elk Street]]>><<set $eventskip to 1>><</link>>
<br>