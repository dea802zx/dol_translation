<<set $outside to 1>><<set $location to "beach">><<effects>>

You decide you've had enough fun for now and pick up your clothes.

<<clotheson>>

"Thank you," says the lifeguard, sounding relieved. "I didn't want to have to get the police involved. Try to remain dressed from now on."
<br><br>

<<endevent>>

<<link [[Next|Beach]]>><</link>>
<br>