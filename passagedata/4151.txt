<<set $outside to 0>><<set $location to "town">><<effects>>
<<if $submissive gte 1150>>
	"Would you like to go to the cinema?" you ask.
<<elseif $submissive lte 850>>
	"Let's see a film," you say.
<<else>>
	"Want to go to the cinema?," you ask.
<</if>>
<br><br>
<<if $hour lt 12 and $schoolday is 1 or $hour lt 12 and $weekday is 7 or $hour lt 12 and $weekday is 1>>
	<<His>> eyes light up, but then looks at the clock on the wall. "I can't," <<he>> says. "I need to get ready soon. I can go in the evening."
	<br><br>
	<<robinoptions>>
<<else>>
	<<set $robinwalk to 1>><<set $money -= 1500>>
	<<His>> eyes light up and <<he>> jumps to <<his>> feet. "What will we see?" <<he>> asks. <<His>> smile falters a moment. "Nothing scary I hope."
	<br><br>
	<<if $robinromance is 1>>
		You walk hand in hand to the High Street. The cinema stands beside the shopping centre. Three films are showing.
		<br><br>
	<<else>>
		You walk together to the High Street. The cinema stands beside the shopping centre. Three films are showing.
		<br><br>
	<</if>>
	<<link [[Superhero|Robin Cinema Superhero]]>><<pass 1 hour>><<pass 30>><<stress -12>><</link>><<lstress>>
	<br>
	<<link [[Romance|Robin Cinema Romance]]>><<pass 1 hour>><<pass 30>><<stress -12>><</link>><<lstress>>
	<br>
	<<link [[Horror|Robin Cinema Horror]]>><<pass 1 hour>><<stress 6>><</link>><<gstress>>
	<br>
<</if>>