<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>><<npcincr Whitney lust -20>>

The audience applaud. Whitney gasps and climbs off you.

<<clotheson>>

<<tearful>> you climb to your feet. Whitney takes your hand. "Let's leave before these perverts get any funny ideas." <<He>> drags you from the pub.
<br><br>

<<He>> pulls a cigarette out of <<his>> shirt pocket once outside. "And they say I'm not romantic," <<he>> says. "Fuck off home or wherever, I've got things to do." <<He>> walks away from you.
<<llust>>
<br><br>

<<endcombat>>

<<link [[Next|Harvest Street]]>><<set $eventskip to 1>><</link>>
<<elseif $enemyhealth lte 0>>

You shove Whitney off you. <<tearful>> you flee the pub. The audience cheer at the spectacle.
<<llove>><<ldom>><<npcincr Whitney love -1>><<npcincr Whitney dom -1>>
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Harvest Street]]>><<set $eventskip to 1>><</link>>

<<else>>

"You're showing me up in front of all these people," Whitney says. "You should treat your <<if $pronoun is "m">>boy<<else>>girl<</if>>friend better." <<He>> shoves you away, adjusts <<his>> clothes and leaves the pub.
<<llove>><<ldom>><<npcincr Whitney love -1>><<npcincr Whitney dom -1>>
<br><br>

<<clotheson>>
<<endcombat>>

<<tearful>> you leave the same way as Whitney, but don't see them anywhere.
<br><br>

<<link [[Next|Harvest Street]]>><<set $eventskip to 1>><</link>>

<</if>>