<<effects>>

The <<person>> holds out <<his>> phone once at the base of the stairs on the other side. "I'm a <<if $pronoun is "m">>man<<else>>woman<</if>> of my word," <<he>> says, deleting the footage. "See you around school, perv."
<br><br>

You dart into the relative safety of a nearby alley. You run around another corner, than lean against a wall, only now realising how fast your pulse races.
<<garousal>><<arousal 6>>
<br><br>

<<link [[Next|Industrial alleyways]]>><<endevent>><</link>>
<br>