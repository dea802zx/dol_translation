<<effects>>

You disregard Remy's warning, and bite into the purple fruit. It's sweet. So sweet. The delicate flesh melts in your mouth, leaving a soothing trail as it slides down your throat.
<br><br>

You eat until only a large seed remains. You throw it aside.
<br><br>

A few moments later you feel lightheaded. An unnatural warmth builds within you.
<br><br>

<<link [[Next|Riding School Lesson]]>><</link>>
<br>