<<effects>>

<<if $worn.under_lower.exposed lte 0>>
	You tug off your $worn.lower.name, exposing your $worn.under_lower.name. The <<person>> ogles you, and snatches the bottoms from your hands.
	<<exhibitionism2>>
<<else>>
	You tug off your $worn.lower.name, exposing your $worn.under_lower.name. The <<person>> ogles you, and snatches the bottoms from your hands.
	<<exhibitionism3>>
<</if>>
<<lowerstrip>>

<<if $rng gte 81>>
	"Thanks," <<he>> says. "See ya." <<He>> drives away, still holding your bottoms. You run after <<him>>, but <<he>> doesn't stop. <<if $leftarm is "bound" and $rightarm is "bound">><<else>>You cover your <<undies>> with your hands.<</if>><<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
	<br><br>
	<<endevent>><<lowerruined>>
	<<destinationfarmroad>>
	<br>
<<else>>
	"Thanks," <<he>> says. "See ya." <<He>> drives away, still holding your bottoms. You run after <<him>>, until <<he>> slows to a stop. You hear <<him>> laughing. "Get in," <<he>> says, opening the door to the seat beside <<himstop>><<gstress>><<stress 6>>
	<br><br>

	<<link [[Next|Farm Hitchhike Exposed Journey]]>><</link>>
	<br>
<</if>>