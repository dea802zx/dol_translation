<<set $outside to 0>><<effects>>

<<if $clothes_choice and $clothes_choice_previous>>
	<<if $clothes_choice is $clothes_choice_previous>>
		<<shopbuy "lower">>
	<<else>>
		<<shopbuy "lower" "reset">>
	<</if>>
<<else>>
	<<shopbuy "lower" "reset">>
<</if>>
<<clothingShop "lower">>
<br>

<<link [[Back to shop|Clothing Shop]]>><<unset $clothes_choice>><</link>>