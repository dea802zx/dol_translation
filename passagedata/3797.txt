<<effects>>

<<if $NPCName[$NPCNameList.indexOf("Avery")].rage gte random(20, 100)>><<set $averyragerevealed to 1>>

<<He>> stares at you a moment while squeezing the steering wheel. <<He>> steps onto the pavement and opens one of the passenger doors. "Get in," <<he>> says.
<<gggarage>><<npcincr Avery rage 15>>
<br><br>

<<link [[Get in|Street Avery Rape Drive]]>><<npcincr Avery rage -5>><</link>><<larage>>
<br>
<<link [[Refuse|Street Avery Refuse]]>><<pain 5>><</link>><<gggarage>><<lllove>><<gpain>>
<br>

<<else>>
"Suit yourself," <<he>> says. <<He>> drives away.
<br><br>

<<endevent>>
<<destinationeventend>>

<</if>>