<<set $outside to 0>><<set $location to "arcade">><<effects>>

You awaken on something hard and bumpy. Just in time to see a door slam shut above you, plunging you into darkness. You drift back into unconsciousness.
<br><br>

<<link [[Next|Trash Intro]]>><</link>>
<br>