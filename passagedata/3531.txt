<<set $outside to 0>><<set $location to "strip_club">><<effects>>

<<npc Darryl>><<person1>>

<<if $stripclubmanagementintro isnot 1 and $phase2 isnot 1 and $phase2 isnot 2>><<set $phase2 to 1>>
You are led to the back of the club, then up a flight of stairs. A wooden door sits open at the top. The bouncer knocks twice and motions you through.
<br><br>
You enter what looks like a personal office. Shelves overflowing with books line the walls. There's a desk on the far side of the room. More books lay open on top of one another, a few lie on the ground at its base. Instead of a wall, one side of the room has a large window overlooking the club below. A <<if $pronoun is "m">>man<<else>>woman<</if>> stands facing away from you, looking for something on one of the shelves.
<br><br>

	<<if $pronoun is "m">>
	"I'll be right with you." <<he>> says, sounding flustered. <<His>> shapely behind and dark trousers wiggle as <<he>> searches a lower shelf. "It's no use. I'd lose my head if it wasn't screwed on." <<He>> turns to face you, revealing an attractive face in its mid-thirties. <<He>> wears a professional black outfit and a pair of glasses rest on top of <<his>> short brown hair.

	<<elseif $pronoun is "f">>
	"I'll be right with you." <<he>> says, sounding flustered. <<His>> shapely behind and long black skirt wiggle as <<he>> searches a lower shelf. "It's no use. I'd lose my head if it wasn't screwed on." <<He>> turns to face you, revealing an attractive face in its mid-thirties. <<He>> wears a professional black outfit and a pair of glasses rest on top of <<his>> brown hair, which is tied into a neat bun.

	<</if>>
<br><br>

"My name is Darryl, I run this establishment. I hear you're looking for work." <<He>> doesn't give you the opportunity to question how <<he>> knew that already. "Splendid! We're always on the look out for new talent."
<br><br>

<<elseif $phase is 2>>
<<He>> pauses for a moment before reaching up and feeling around. As <<he>> grasps <<his>> glasses a look of relief lights up <<his>> face. "Oh. This is embarrassing." <<His>> face does seem to be turning red. "Thank you. Don't tell anyone though, please?" <<He>> places the lenses over <<his>> eyes and starts reading one of the books sprawled on <<his>> desk.
<br><br>
<<npcincr Darryl love 5>><<npcincr Darryl dom -5>>
<<elseif $phase is 3>>
You decide to save <<him>> the embarrassment. <<He>> will no doubt find them soon anyway. You give <<him>> a nod and make your way back down to the main floor.
<br><br>
<<elseif $phase2 isnot 2>>
Darryl looks up from <<his>> desk as you enter, <<his>> fringe and glasses obscuring <<his>> eyes. "Welcome back! You want to know what other work is available?"
<br><br>
<</if>>

<<if $phase2 is 1>>
<<link [[Next|Strip Club Management]]>><<endevent>><<set $phase2 to 2>><</link>>
<</if>>

<<if $phase2 isnot 1>>

<<if $phase is 0>><<set $stripclubbartendingintro to 1>>
"The bartending job is exactly what it sounds like. You won't be expected to do anything lewd, but you might make more in tips if you dress provocatively. If the patrons give you trouble, just alert security. Pay is £5 an hour, plus any tips you receive."
<br><br>
"You can start whenever, we always need more hands."
<br><br>
<</if>>

<<if $phase is 1>><<set $stripclubdancingintro to 1>>
"You want to dance? Good! You probably saw some stages were empty. You can dress how you want, but you should dress appropriately if you want decent tips. We operate a strict no-touching policy in the main room. If anyone gives you trouble, don't hesitate to call security. We take the safety of our staff very seriously."
<br><br>

"Any tips you get are yours to keep, but if it's serious money you're after you'll want to sell private dances to patrons. We provide rooms for such a purpose. What happens in there, and how much it will cost them, is between you and your client. Officially there's not supposed to be any physical contact, so be discreet. We take an eighty percent cut of anything you earn in a private room. That might seem high, but trust me, it isn't."
<br><br>

<<He>> pauses a moment before continuing, "Almost forgot, we have a dressing room of course. Use it as you need."
<br><br>
<</if>>

<<if $stripclubmanagementintro isnot 1>><<set $stripclubmanagementintro to 1>>
As you leave the room Darryl calls out to you. "Oh, by the way. If you see a pair of spectacles laying around. Would you let me know? I seem to have misplaced mine." Do you tell <<him>> or not?
<br><br>

<<link [[Yes|Strip Club Management]]>><<endevent>><<set $phase to 2>><</link>>
<br>
<<link [[No|Strip Club Management]]>><<endevent>><<set $phase to 3>><</link>>
<br>

<<else>>

<<link [[Leave|Strip Club]]>><<endevent>><</link>>

<</if>>

<</if>>