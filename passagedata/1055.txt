<<effects>>

"Me?" you ask, standing upright and wiggling your <<bottomstop>> "I'm just doing cow things. Naked in this field." <<His>> eyes move over your body, trying to glimpse you behind the long grass. <<He>> shifts to get a better angle, <<his>> original question forgotten.
<<exhibitionism1>>

You hide low within the grass and continue digging.
<br><br>

<<link [[Next|Livestock Field]]>><<endevent>><</link>>
<br>