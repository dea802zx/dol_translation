<<set $outside to 0>><<set $location to "town">><<effects>>

The interior is more modern. You follow Niki up a flight of stairs, and enter a large room dominated by a white screen. Several lights and a single camera wait.
<br><br>

"You done this before?" Niki asks, fiddling with the camera. <<He>> has another white stick in <<his>> mouth. You shake your head. <<He>> nods without looking at you. "Clothes are over there. Should be done in a few hours."
<br><br>

You walk behind a screen, and find a chef's apron and hat. You put them on.
<br><br>

<<link [[Next|Photography Chef 3]]>><</link>>
<br>