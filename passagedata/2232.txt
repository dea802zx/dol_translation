<<set $location to "landfill">><<set $outside to 1>><<effects>>

The gate is barred by a chain and padlock. You wonder if there's a key somewhere nearby.
<br><br>

<<set $lock to 800>>

	<<if $skulduggery gte $lock>>
	<span class="green">The lock looks easy to pick.</span>
	<br><br>

	<<link [[Pick lock (0:05)|Trash Lock Leave]]>><<pass 5>><<crimeup 1>><</link>><<crime>>
	<br>
	<<else>>
	<span class="red">The lock looks beyond your ability to pick.</span> <<skulduggeryrequired>>
	<br><br>
	<</if>>

<<link [[Leave|Trash]]>><</link>>