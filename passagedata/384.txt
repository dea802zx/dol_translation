<<set $outside to 0>><<set $location to "cabin">><<effects>>

You prepare the food and place it on the table in front of <<himstop>> <<He>> takes a bite, then smiles and gently pats your head. <<He>> shares some with you and you eat together. Eden talks a lot about the minutiae of life here, pausing occasionally to allow you a response. You nod politely each time, which seems enough to satisfy <<himstop>>
<br><br>

<<His>> plate empty, <<he>> stands and looks out the window.

<<if $weather is "clear">>
"It's a lovely day! We'll be able to get lots of work done."
<<elseif $weather is "rain">>
"We've got lots of work to do today. A little rain won't stop us."
<<elseif $weather is "overcast">>
"The clouds are grumbling. Hopefully they don't burst before we're done for the day."
<</if>>
<br><br>

<<link [[Next|Forest Cabin]]>><<endevent>><</link>>
<br>