<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You are outside the school changing rooms. You need to pass through them to access the pool.
<br><br>

<<if $exposed gte 1>>

	A stack of towels sits on a trolley beside the door.
	<br><br>
	<<link [[Grab some towels|School Pool Entrance]]>><<towelup>><</link>>
	<br><br>

<</if>>

<<link [[Boy's Changing Room|School Boy Changing Room]]>><</link>>
<br>

<<link [[Girl's Changing Room|School Girl Changing Room]]>><</link>>
<br><br>

<<if $schoolstate isnot 0 and $schoollesson is 1>>
	<<if $time gte ($hour * 60 + 5) and $time lte ($hour * 60 + 57)>>
	<<else>>
		<<if $player.gender_appearance is "m">>
		<<link [[Wait for the girls to leave (0:05)|School Pool Entrance]]>><<pass 5>><</link>>
		<br><br>
		<</if>>
		<<if $player.gender_appearance is "f">>
		<<link [[Wait for the boys to leave (0:05)|School Pool Entrance]]>><<pass 5>><</link>>
		<br><br>
		<</if>>
	<</if>>
<</if>>

<<link [[Leave|Hallways]]>><</link>>
<br>