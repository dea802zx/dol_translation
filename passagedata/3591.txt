<<set $outside to 1>><<set $location to "temple">><<temple_effects>><<effects>>
You put up with it. You continue picking fruit off the tree, now with the <<monk>> helping. <<He>> grasps your shoulder with one hand while reaching over with the other.
<br><br>
<<if $rng gte 51>>
	It's subtle, but <<his>> pelvis is a little more animated than it needs to be. You feel <<him>> grind against your <<bottomcomma>> becoming rougher, until <<he>> grasps both your shoulders and bends you over. It's all you can do to not fall off the ladder. You feel <<him>> shake and shudder behind you.
	<<trauma 6>><<stress 6>><<gtrauma>><<gstress>>
	<br><br>
	<<set $temple_harassed += 1>>
	"All done," <<he>> says, stepping down from the ladder. "Thank you for helping." <<He>> chuckles as <<he>> walks back to the temple.
	<br><br>
	<<tearful>> you step down as well.
	<br><br>
	<<endevent>>
	<<link [[Next|Temple Garden]]>><</link>>
	<br>
<<else>>
	"All done," <<he>> says, stepping down from the ladder. "Thank you for helping." <<He>> walks back to the temple.
	<br><br>
	You step down as well.
	<br><br>
	<<endevent>>
	<<link [[Next|Temple Garden]]>><</link>>
	<br>
<</if>>