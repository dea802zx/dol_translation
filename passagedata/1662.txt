<<set $outside to 0>><<set $location to "cafe">><<effects>>
<<set $NPCName[$NPCNameList.indexOf("Sam")].love = Math.clamp($NPCName[$NPCNameList.indexOf("Sam")].love, -50, 50)>>
<<set $rng to random(1, 100)>>

<<if $rng gte 81 and $weather isnot "rain">>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
		<<if $worn.lower.skirt is 1>>
		As you're serving tables outside the cafe, a strong breeze blows in from the ocean, lifting your skirt and exposing your <<undies>> for anyone who looks.
		<<undiestrauma>>
		<br><br>

			<<if !$worn.under_lower.type.includes("naked")>>
			<<link [[Own it|Ocean Breeze Own]]>><<set $phase to 0>><</link>><<exhibitionist1>>
			<br>
			<<elseif $exhibitionism gte 35>>
			<<link [[Own it|Ocean Breeze Own]]>><<set $phase to 1>><</link>><<exhibitionist3>>
			<br>
			<</if>>
			<<link [[Cover yourself|Ocean Breeze Cover]]>><</link>>
			<<if !$worn.under_lower.type.includes("naked")>>
			<<fameexhibitionism 5>>
			<<else>>
			<<fameexhibitionism 10>>
			<</if>>
		<<else>>
		As you're serving tables outside the cafe, a strong breeze blows in from the ocean, pleasantly caressing your skin.
		<<lstress>><<stress -4>>
		<br><br>

		The rest of the shift passes uneventfully. You earn £5.
		<<set $money += 500>><<pass 1 hour>>
		<br><br>

	<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>
		<</if>>
	<<else>>
	You spend most of the shift serving the tables outside, overlooking the ocean.
	<br><br>

	The rest of the shift passes uneventfully. You earn £5.
	<<set $money += 500>><<pass 1 hour>>
	<br><br>

	<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>
	<</if>>

<<elseif $rng gte 81>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
	<<generate1>><<person1>>You serve a <<person>> who orders a bun. <<He>> licks the cream. "That's so good," <<he>> gasps. <<He>> pulls back a chair and slides the plate in front of it. "Please, sit and try some. My treat." The cafe isn't very busy.
	<br><br>

		<<link [[Sit|Ocean Breeze Sit]]>><</link>>
		<br>
		<<link [[Refuse|Ocean Breeze Sit Refuse]]>><</link>>
		<br>
	<<else>>
	<<generate1>><<person1>>You serve a <<person>> who orders a bun. <<He>> stares at you as <<he>> licks the cream.
	<br><br>

	The rest of the shift passes uneventfully. You earn £5.
	<<set $money += 500>><<pass 1 hour>>
	<br><br>

	<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>
	<</if>>

<<elseif $rng gte 71>>
	<<generate1>><<person1>>A <<person>> sits at a table alone with <<his>> head back and eyes closed.
	<<if $awareness gte 400>>
		<<generates2>>From the way <<hes>> grinding, it's quite obvious <<hes>> getting oral. <<Hes>> barely trying to hide it. Covertly checking, you notice a <<person2>><<person>>
		under the table, <<print either("gently","eagerly","energetically","sensually","clumsily","vigorously","dutifully","skilfully")>>
		<<person1>><<if $pronoun is "m">>sucking the man off.<<else>>eating the woman out.<</if>>
		<<garousal>><<arousal 200>>
	<<elseif $awareness gte 200>>
		<<if $weather is "clear">><<He>> seems to take almost sexual pleasure from sunbathing.
		<<else>>If not for the rhythmic squirming, you'd think <<he>> was asleep.
		<</if>>At one point <<he>> shudders in a way that seems almost orgasmic.
	<<else>>
		<<if $weather is "clear">>It's a beautiful day - <<hes>><<else>><<Hes>><</if>> probably dozed off.
	<</if>>

	<br><br>
	The rest of the shift passes uneventfully. You earn £5.
	<<set $money += 500>><<pass 1 hour>>
	<br><br>

	<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>

<<elseif $rng gte 61>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
	<<generate1>><<generate2>>You serve a young pair, a <<person1>><<person>> and a <<person2>><<personstop>>
		<<if $worn.lower.skirt is 1>>
			<<if $worn.under_lower.type.includes("naked")>>
			As you turn to leave, the <<person1>><<person>> lifts the hem of your $worn.lower.name, revealing your bare <<bottom>> to the cafe. "No underwear?" <<He>> says with incredulity. "You're a little slut, aren't you?"
			<<fameexhibitionism 10>><<stress 6>><<arousal 600>><<gstress>><<garousal>>
			<br><br>

			<<link [[Get angry|Ocean Breeze Angry]]>><</link>>
			<br>
			<<link [[Ignore|Ocean Breeze Ignore]]>><<trauma 6>><<stress 6>><<set $phase to 2>><</link>><<gtrauma>><<gstress>>
			<br>
			<<else>>
			As you turn to leave, the <<person1>><<person>> lifts the hem of your $worn.lower.name, revealing your $worn.under_lower.name to the cafe. <<He>> and the <<person2>><<person>> both laugh, drawing more attention to you.
			<<fameexhibitionism 1>><<stress 3>><<arousal 300>><<gstress>><<garousal>>
			<br><br>

			<<link [[Get angry|Ocean Breeze Angry]]>><</link>>
			<br>
			<<link [[Ignore|Ocean Breeze Ignore]]>><<trauma 3>><<stress 3>><<set $phase to 1>><</link>><<gtrauma>><<gstress>>
			<br>
			<</if>>
		<<else>>
			<<if $worn.under_lower.type.includes("naked")>>
			As you turn to leave, the <<person1>><<person>> pulls down the back of your $worn.lower.name, revealing your bare <<bottom>> to the cafe. "No underwear!" <<He>> says with incredulity. "Bet you regret that now."
			<<fameexhibitionism 5>><<stress 3>><<arousal 300>><<gstress>><<garousal>>
			<br><br>

			<<link [[Get angry|Ocean Breeze Angry]]>><</link>>
			<br>
			<<link [[Ignore|Ocean Breeze Ignore]]>><<trauma 3>><<stress 3>><<set $phase to 1>><</link>><<gtrauma>><<gstress>>
			<br>
			<<else>>
			As you turn to leave, the <<person1>><<person>> pulls down the back of your $worn.lower.name, revealing your $worn.under_lower.name to the cafe.
			<<fameexhibitionism 1>><<stress 2>><<arousal 200>><<gstress>><<garousal>>
			<br><br>

			<<link [[Get angry|Ocean Breeze Angry]]>><</link>>
			<br>
			<<link [[Ignore|Ocean Breeze Ignore]]>><<trauma 2>><<stress 2>><<set $phase to 0>><</link>><<gtrauma>><<gstress>>
			<br>
			<</if>>
		<</if>>
	<<else>>
	<<generate1>><<generate2>>You serve a young pair, a <<person1>><<person>> and a <<person2>><<personstop>> You think you catch the <<person1>><<person>> checking you out, but they don't say anything.
	<br><br>

	The rest of the shift passes uneventfully. You earn £5.
	<<pass 1 hour>><<set $money += 500>>
	<br><br>

	<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>
	<br>
	<</if>>

<<elseif $rng gte 51>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	A couple of tourists are sat at one of your tables.
	<<if $danger gte (9900 - $allure)>>
		<<generate1>><<generate2>>
		<<person1>>You overhear the <<person>> telling the <<person2>><<person>> about the town in a hushed voice.
		<br>
		<<person1>>"I swear," <<he>> says. "This town is a dream for people like us. You can do whatever you like here. No one stops you."
		<br>
		"Is it true that even the police won't-"
		<br>
		"The police? They're worse than the rest! Slip 'em a few quid, and they'll probably cuff a <<girl>> down for you."
		<br>
		"Wow. Like, anyone? It doesn't matter how-"
		<br>
		"Anyone. Hey," <<he>> looks at you. "You. <<if $beauty gte($beautymax / 7) * 2>>You're cute.<</if>> You've been raped, right?"
		<br><br>

		<<if ($rapestat + $beastrapestat + $tentaclerapestat) gt 0>>
			<<link [[Nod|Ocean Breeze Tourist]]>><<set $phase to 1>><</link>>
			<br>
			<<link [[Deny (lie)|Ocean Breeze Tourist]]>><<set $phase to 2>><</link>>
			<br>
			<<link [[Refuse to answer|Ocean Breeze Tourist]]>><<set $phase to 3>><</link>>
			<br>
		<<else>>
			<<link [[Nod (lie)|Ocean Breeze Tourist]]>><<set $phase to 1>><</link>>
			<br>
			<<link [[Deny|Ocean Breeze Tourist]]>><<set $phase to 2>><</link>>
			<br>
			<<link [[Refuse to answer|Ocean Breeze Tourist]]>><<set $phase to 3>><</link>>
			<br>
		<</if>>
	<<else>>
		They talk at length about
		<<print either("the beach.","surfing.","the museum.","boys.","girls.","the food.","the news.","the ultimate unity of life.","their plans for next weekend.", "the political situation.")>>
		<br><br>
		The rest of the shift passes uneventfully. You earn £5.
		<<pass 1 hour>><<set $money += 500>>
		<br><br>

		<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>
	<</if>>

<<elseif $rng gte 41>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
	<<generate1>><<person1>> A <<person>> enters the cafe and sits alone in the corner. <<He>> keeps stealing glances at you, and when you go to take <<his>> order <<he>> asks only for a glass of water. As you turn to leave <<he>> grabs your wrist. "You don't happen to be on the menu, do you?" <<He>> smiles. "How about you and I find somewhere quiet and have some fun? I'll make it worth your while." <<He>> taps <<his>> <<walletstop>> <<His>> intentions are clearly lewd in nature.
	<br><br>
		<<if $promiscuity gte 15>>
		<<link [[Accept the offer|Ocean Breeze Whore]]>><<set $sexstart to 1>><</link>><<promiscuous2>>
		<br>
		<<else>>
		You are not promiscuous enough to take up such an offer.
		<br><br>
		<</if>>
		<<link [[Refuse|Ocean Breeze Whore Refuse]]>><</link>>
	<<else>>
	<<generate1>><<person1>> A <<person>> enters the cafe and sits alone in the corner. <<He>> keeps stealing glances at you. However, <<he>> refuses to make eye contact when you take <<his>> order.
	<br><br>
	The rest of the shift passes uneventfully. You earn £5.
	<<set $money += 500>><<pass 1 hour>>
	<br><br>

	<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>
	<</if>>

<<elseif $rng gte 31>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
	<<generate1>><<person1>>A <<if $pronoun is "m">>waiter<<else>>waitress<</if>> collides with you as you exit the kitchen, spilling warm soup over your <<if $worn.upper.type.includes("naked")>>chest<<else>>$worn.upper.name.<</if>> "I am so sorry," <<he>> says, reaching for a towel. Customers turn to watch.
	<br><br>

		<<link [[Ignore|Ocean Breeze Soup Ignore]]>><<npcincr Sam love 2>><<stress 6>><<pain 20>><</link>><<glove>><<gpain>><<gstress>>
		<br>
		<<link [[Confront|Ocean Breeze Soup Confront]]>><<npcincr Sam love -1>><<stress -6>><<trauma -6>><</link>><<llove>><<ltrauma>><<lstress>>
		<br>
		<<if $player.gender_appearance is "m" and $breastsize lte 2 and !$worn.upper.type.includes("naked")>>
		<<link [[Remove top|Ocean Breeze Remove]]>><<npcincr Sam love 2>><</link>><<glove>>
		<br>
		<<elseif $exhibitionism gte 55 and !$worn.upper.type.includes("naked")>>
		<<link [[Remove top|Ocean Breeze Remove Lewd]]>><<npcincr Sam love 2>><</link>><<glove>><<exhibitionist4>>
		<br>
		<</if>>
	<<else>>
	<<generate1>><<person1>>A <<if $pronoun is "m">>waiter<<else>>waitress<</if>> almost collides with you as you exit the kitchen.
	<br><br>

	The rest of the shift passes uneventfully. You earn £5.
	<<set $money += 500>><<pass 1 hour>>
	<br><br>

	<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>
	<</if>>

<<elseif $rng gte 21>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
	<<if $danger gte (9900 - $allure)>>
	<<generate1>><<generate2>><<generate3>><<generate4>>
	A large group enters the cafe, taking up several tables by themselves. You try to ask for their order, but a <<person1>><<person>> sitting at the head interrupts you. "It's my birthday today, you know. How about something special?" <<He>> openly ogles your body. "Wanna climb up on the table and give us a dance?"
	<br><br>

		<<link [[Dance for them|Ocean Breeze Dance]]>><<danceinit>><<set $dancing to 1>><<set $audience to 4>><<set $venuemod to 2>><<set $timer to 12>><<set $dancelocation to "cafe">><</link>><<exhibitionist1>><<gstress>><<gtiredness>>
		<br><br>

		<<link [[Refuse|Ocean Breeze Dance Refuse]]>><</link>>
	<<else>>
	A large group enters the cafe, taking up several tables by themselves. They don't give you any trouble though.
	<br><br>
	The rest of the shift passes uneventfully. You earn £5.
	<<set $money += 500>><<pass 1 hour>>
	<br><br>

	<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>
	<</if>>

<<elseif $rng gte 11 and $chef_state is undefined>>
<<npc Sam>><<person1>>Sam rushes out of the kitchen, looking flustered.

	<<if $NPCName[$NPCNameList.indexOf("Sam")].love gte 10>>

	<<He>> glances in your direction, <span class="green">then hastens over.</span> "I'm in a bit of a pickle," <<he>> says. "Our chef's stormed off again. I need someone to fill in. Don't worry, it's not hard, and I'll pay you double. Will you help me out?"
	<br><br>

	<<link [[Accept|Chef Help]]>><<npcincr Sam love 2>><</link>><<glove>>
	<br>
	<<link [[Refuse|Ocean Breeze Chef Refuse]]>><<npcincr Sam love -1>><</link>><<llove>>
	<br>

	<<else>>
	<<He>> glances in your direction, <span class="red">but passes over you.</span> <<He>> approaches another member of staff, and offers them better pay to fill in for the chef this shift. <i>Perhaps Sam would be willing to give you a chance if <<he>> liked you more.</i>
	<br><br>

	The rest of the shift passes uneventfully. You earn £5.
	<<set $money += 500>><<pass 1 hour>>
	<br><br>

	<<link [[Next|Ocean Breeze]]>><<endevent>><</link>>
	<</if>>

<<else>>
	<<set $rng to random(1, 100)>>
	<<if $rng gte 51 and $bestialitydisable is "f">>
Sam asks you to take some trash to the dumpster beside the cafe. Stood beside the dumpster, however, is a stray dog. It growls as you approach.
<br><br>

<<link [[Continue regardless|Ocean Breeze Dumpster Dog]]>><<stress 6>><</link>><<gstress>>
<br>
<<link [[Forget your task and go back inside|Ocean Breeze Dumpster Refuse]]>><</link>>
	<<else>>
<<generate1>><<generate2>>Sam asks you to take some trash to the dumpster beside the cafe. Stood beside the dumpster, however, are a <<person1>><<person>> and a <<person2>><<personstop>> They have a dangerous look about them.
<br><br>

<<link [[Continue regardless|Ocean Breeze Dumpster]]>><<stress 6>><</link>><<gstress>>
<br>
<<link [[Forget your task and go back inside|Ocean Breeze Dumpster Refuse]]>><</link>>
	<</if>>

<</if>>