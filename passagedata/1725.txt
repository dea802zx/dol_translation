<<set $outside to 1>><<set $location to "compound">><<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>>
	Panting, the guard dog returns to where it came from.
	<br><br>
	<<tearful>> you rise to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Elk Compound]]>><<set $eventskip to 1>><</link>>
	<br>
<<elseif $enemyhealth lte 0>>
	The beast whimpers and hides behind one of the buildings.
	<br><br>
	<<tearful>> you rise to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Elk Compound]]>><<set $eventskip to 1>><</link>>
	<br>
<<elseif $alarm is 1 and $rescue is 1>>
	<<generate2>><<generate3>><<person2>>A <<person>> and <<person3>><<person>> arrive to investigate the noise.
	<<if $NPCList[0].stance is "top">>
		They see the dog humping you and laugh. "Good boy," the <<person2>><<person>> says.
		<br><br>
	<<else>>
		They see what the dog wants from you and laugh. "Good boy," the <<person2>><<person>> says.
		<br><br>
	<</if>>
	<<person3>>We should probably let the dog have its treat," says the <<person>> as <<he>> crouches for a closer look. <<He>> pulls out <<his>> phone and starts filming.
	<br><br>
	<<link [[Next|Elk Compound Bestiality 2]]>><</link>>
	<br>
<<else>>
	The beast turns and darts away, barking.
	<<crime>><<set $compoundalarm += 1>>
	<<crimeup 50>>
	<br><br>
	<<tearful>> you rise to your feet.
	<br><br>
	<<clotheson>>
	<<endcombat>>
	<<link [[Next|Elk Compound]]>><<set $eventskip to 1>><</link>>
	<br>
<</if>>