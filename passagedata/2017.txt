<<set $outside to 1>><<set $location to "home">><<effects>>
<<fameexhibitionism 1>>
<<if $leftarm isnot "bound" and $rightarm isnot "bound">>

You <<nervously>> move your hands, displaying yourself to the <<personstop>>

<<else>>

You <<nervously>> turn your body, displaying yourself to the <<personstop>>

<</if>>

You feel exposed and humiliated as you look at the clothes held in <<his>> hand, but <<he>> makes no move to give them back.
<br><br>

<<if $submissive gte 1150>>

"P-please..." you plead. <<He>> takes pity on you and throws you the clothing. You run behind a bush to get dressed. You hope <<he>> doesn't tell anyone about this, but you know better.
<br><br>
<<elseif $submissive lte 850>>

Realising how entranced <<he>> is by your body, you snatch the clothing from <<him>> and march behind a bush to get dressed. You hope <<he>> doesn't tell anyone about this, but you know better.
<br><br>

<<else>>

"I did what you asked." You say, hoping the ordeal is near over.
<br><br>
<<He>> looks at you thoughtfully for a moment, then throws the clothing at you. You run behind a bush to get dressed. You hope <<he>> doesn't tell anyone about this, but you know better.
<br><br>

<</if>>
<br><br>

<<clotheson>>
<<link [[Next|Garden]]>><<endevent>><</link>>
<br>