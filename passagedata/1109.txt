<<effects>>

<<if $malechance lte 0>>
	<<set _horse_gender to "f">>
<<elseif $malechance gte 100>>
	<<set _horse_gender to "m">>
<<else>>
	<<set _horse_gender to "b">>
<</if>>
<<if _horse_gender is "m">>
	<<generatem1>>
<<else>>
	<<generatef1>>
<</if>>
<<person1>>

<<if $livestock_horse gte 2>>
	You hold out the basket of apples, and soon find yourself surrounded. One tries to sneak a second, until an elder slaps their hand away.
	<br><br>

	<<link [[Next|Livestock Field]]>><<endevent>><</link>>
	<br>
<<else>>
	<<set $livestock_horse to 2>>
	You hold out the basket of apples. The centaur stare in shock at such a bounty. They trot closer, but carefully, as if afraid it's a trick. A younger centaur chances it first. "So sweet," <<he>> manages, catching the dripping juice in <<his>> fingers and licking it off.
	<br><br>
	The other centaur crowd around, none wanting to be left out. One of the elders takes charge, making sure they're evenly shared.
	<br><br>

	The apples soon vanish, but the centaur look at you in a different way. Less hostile. "Sorry if we were rude," one mumbles.
	<br><br>

	<<link [[Next|Livestock Field]]>><<endevent>><</link>>
	<br>

<</if>>