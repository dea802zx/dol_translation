<<set $outside to 1>><<set $location to "town">><<temple_effects>><<effects>>
You join the procession, falling in line behind a <<person2>><<personstop>>
<br><br>
The <<person1>><<monk>> leads you to Danube Street, and up to one of the mansion doors. <<He>> rings the bell, and <<person4>><<person>> answers.
<<if $rng gte 51>>
	"Salvation is bloody expensive," <<he>> mutters, pushing <<his>> card into a machine proffered by the <<monkstop>> <<He>> enters <<his>> pin, removes the card, and shuts the door without another word.
	<br><br>
	"<<Hes>> always like that," the <<person1>><<monk>> says as <<he>> herds you and the others back to the street.
<<else>>
	<<He>> takes one look at the assembled procession, then slams the door.
	<br><br>
	"Some people are like that," the <<person1>><<monk>> mutters as <<he>> herds you and the others back to the street.
<</if>>
<br><br>
<<pass 30>>
The rest of the street continues in much the same way. Some homes are more polite than others.
<<if $skulduggery gte 400>>
	<span class="purple">You notice the <<monk>> trace a holy symbol on each doorframe as <<he>> reaches to knock.</span>
	<<gawareness>><<awareness 1>>
<</if>>
<br><br>
<<link [[Next|Temple Donations 2]]>><</link>>
<br>