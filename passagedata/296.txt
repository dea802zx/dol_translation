<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
You approach the staff door with a keycard lock.
<<if $asylumkeycard is 1>>
	<<if $leftarm is "bound" and $rightarm is "bound">>
		You have the keycard, but you can't use it with your arms bound.
	<<else>>
		You could open it with the keycard if you wanted.
	<</if>>
<<else>>
	<<if $leftarm is "bound" and $rightarm is "bound">>
		With your arms bound, you couldn't reach it even if you had the keycard.
	<<else>>
		If you had the keycard you could open it.
	<</if>>
<</if>>
<br><br>
<<if $asylumkeycard is 1>>
	<<if $leftarm is "bound" and $rightarm is "bound">>
	<<else>>
		<<link [[Open|Asylum Open]]>><</link>>
		<br>
	<</if>>
<</if>>
<<link [[Back|Asylum]]>><</link>>
<br>