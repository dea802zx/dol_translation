<<set $outside to 0>><<set $location to "home">><<effects>>

<<if $NPCName[$NPCNameList.indexOf("Robin")].love gte 80 and $robincrycount gte 5>>
	<<if $robincrycount is undefined>><<set $robincrycount to 1>><<else>><<set $robincrycount += 1>><</if>>
Robin seems to already realise something is wrong and wraps <<his>> arms around you, <<he>> doesn't ask what's going on, but just quietly soothes you with gentle words as <<he>> rubs <<his>> hands on your back. As <<he>> tries to soothe you, you regain the ability to talk past your tears and begin unloading all of the troubles that have happened to you recently.
<<elseif $NPCName[$NPCNameList.indexOf("Robin")].love gte 60 and $robincrycount gte 1>>
	<<if $robincrycount is undefined>><<set $robincrycount to 1>><<else>><<set $robincrycount += 1>><</if>>
Robin seems stunned for a moment, but soon adapts to the situation and wraps <<his>> arms around you.
<<elseif $NPCName[$NPCNameList.indexOf("Robin")].love gte 40>>
Robin seems stunned for a moment when <<he>> notices you crying. After a little while <<he>> gently but uneasily hugs you back and lets you stay like that until you finish crying.
<<elseif $NPCName[$NPCNameList.indexOf("Robin")].love gte 20>>
"Umm…" Robin responds as <<he>> sees you cry on <<his>> shoulder. <<He>> seems uncertain about how to respond, but <<he>> strokes your hair.
<<else>>
You cry silently into Robin's shoulder. <<He>> seems uncomfortable with this, but doesn't stop you.
<</if>>
<br><br>

<<robinhug>>