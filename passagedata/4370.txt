<<effects>><<set $location to "sewers">><<set $outside to 0>>

You wait for Morgan to become distracted.
<br><br>

<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>

You toss the rats and pour the sludge down a drain, while making exaggerated chomping and sipping noises. You force a burp.
<br><br>

"I hope you enjoyed the meal," Morgan says. "It has been quite a while since I've been able to cook for somebody else, much less my own flesh and blood. Now, there are chores you can help me with, or maybe we'll take a nap."
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

<<link [[Next|Sewers Morgan]]>><</link>>
<br>

<<else>>

You try to toss the rats down a drain, but they splat against the bars. You lift the teacup to pour away, but the slimy handle slips from your hand and it shatters on the ground.
<br><br>

Morgan turns. "I made that for you!" <<he>> says. "You spoiled brat."
<br><br>

	<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
	<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
	<</if>>

<<link [[Next|Sewers Spank]]>><<set $molestationstart to 1>><</link>>
<br>

<</if>>