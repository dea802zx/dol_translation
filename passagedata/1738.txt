<<set $outside to 0>><<set $location to "compound">><<effects>>
<<if $compoundhonest is 1>>
	"I'm not working for anyone," you say. "I'm here alone."
	<br><br>
	<<He>> looks at a screen that you can only see the back of, then turns a dial. Soothing waves emanate from the pads attached to you, pleasuring every inch of your body. "Good. See, that wasn't so bad was it? Now then, why are you here?"
	<br><br>
<<elseif $compoundhonest is 2>>
	"I<<if $blackmoney is 0>> was trapped while robbing a car. It drove me here<<else>>'m here to steal things<</if>>," you say. "<<if $blackmoney is 0>>Honest<<else>>For money<</if>>."
	<<He>> turns the dial again, and you are once more assailed by the soothing waves. "Now," <<he>> says. "What did you <<if $blackmoney is 0>>try to<</if>>steal?"
	<br><br>
<<elseif $compoundhonest is 3>>
	"<<if $blackmoney is 0>>M<<else>>I steal m<</if>>oney or anything I can sell." you say.
	<br><br>
	<<He>> turns the dial again, and you are once more assailed by the soothing waves. "Where do you sell things?"
	<br><br>
<<elseif $compoundhonest is 4>>
	"<<if $produce_sold gte 1 or $pubintro is 1>>I sell <<if $produce_sold gte 1>>flowers in a stall I rent at Connudatus,<</if>><<if $produce_sold gte 1 and $pubintro is 1>> and <</if>><<if $pubintro is 1>>stolen things on Harvest street,<</if>><<else>>I've never sold anything<</if>>" you say.<<if $chef_state gte 1>> "I've <<if $produce_sold gte 1 and $pubintro is 1>>also <</if>>baked," you add.<</if>><<if $prostitutionstat gte 1 or $forcedprostitutionstat gte 1>> "... <<if $produce_sold gte 1 or $pubintro is 1 or $chef_state gte 1>>and <</if>>I've sold myself," you mumble quietly<<if $forcedprostitutionstat gte 1>>, "whether I wanted to or not."<</if>><<else>>.<</if>>
	<br><br>
	<<He>><<if !$pubintro>> looks uninterested as <<he>><</if>> turns the dial again, and you are once more assailed by the soothing waves. "<<if $pubintro is 1>>Who do you sell your stolen goods to?<<else>>Is that really all you do?<</if>>" <span class="red">If you're honest again, <<he>> will have everything <<he>> needs from you.</span>
	<br><br>
<<else>>
	"I<<if $pubintro is 1>> sell things to Landry<<else>>'ve told you everything<</if>>," you say<<if $pubintro is 1 or $prostitutionstat gte 1 or $forcedprostitutionstat gte 1>>, ashamed<</if>>.
	<br><br>

<</if>>
<<compoundoptions>>