<<set $outside to 1>><<set $location to "forest">><<effects>>
Each time you round a corner you see the mousy <<if $NPCName[7].gender is "m">>boy<<else>>girl<</if>> about to disappear around another, despite <<if $NPCName[7].gender is "m">>his<<else>>her<</if>> casual pace.
<br><br>
You arrive at Wolf Street just in time to see the <<if $NPCName[7].gender is "m">>boy<<else>>girl<</if>> disappear into the trees. Robin hesitates, then breaks into a run. You follow <<him>> into the forest.
<br><br>
<<link [[Next|Robin Forest Shop 4]]>><</link>>
<br>