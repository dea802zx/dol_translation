<<effects>><<set $outside to 0>>
<<pass 20>>
You smile at <<himstop>> <<He>> smiles back. "You're so sweet," <<he>> says as <<he>> opens the door beside <<himstop>> "Get in. It's not far."
<br><br>

<<if $avery_location is 1>><<set $avery_location to 0>>
	<<generate2>>
	<<He>> drives you to Danube Street, up to the front gates of a mansion. <<He>> flashes an invitation at a <<person2>><<person>> in servant's garb, who waves you through just as another car pulls up behind you.
	<br><br>
	<<endevent>><<npc Avery>><<person1>>

	<<link [[Next|Avery Party]]>><</link>>
	<br>
<<else>><<set $avery_location to 1>>
	<<He>> drives you to Connudatus Street and parks outside a restaurant. <<He>> takes your hand and leads you into the building.
	<br><br>

	<<link [[Next|Avery Date 2]]>><</link>>
	<br>
<</if>>