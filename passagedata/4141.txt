<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<if $RobinKiyoura isnot 1>><<set $RobinKiyoura to 1>><</if>>

<<if $robinromance is 1>>

<<npc Robin>><<person1>>You tell Robin exactly what happened, where it happened, and how much you enjoyed it. <<His>> fists clench and tears well in <<his>> eyes. <<He>> picks up <<his>> bag and leaves without a word.
<br><br>

<<endevent>>
<<link [[Next|Canteen]]>><</link>>
<br>

<<else>>

<<npc Robin>><<person1>>You tell Robin exactly what happened, where it happened, and how much you enjoyed it. <<He>> struggles to make eye contact or come up with a proper response.
<br><br>

"I-I should," <<he>> gulps. "I'm going to wait in the classroom."
<br><br>
<<endevent>>
<<link [[Next|Canteen]]>><</link>>
<br>

<</if>>