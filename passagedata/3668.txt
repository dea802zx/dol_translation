<<temple_effects>><<effects>>

You refuse to hand over the pendant. "Very well," the <<monk>> says. "On your soul be it. I can't let you access the chamber."
<br><br>
<<endevent>>
<<link [[Next|Temple]]>><</link>>
<br>