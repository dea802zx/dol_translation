<<effects>>

<<if $enemyarousal gte $enemyarousalmax>>

<<ejaculation>><<set $baileydefeatedlewd += 1>><<set $baileydefeatedchain += 1>><<earnFeat "Bailey's Trouble Maker">>

<<person1>>One of the watching orphans giggles. Bailey spins to find the culprit, but another laughs behind <<himstop>> Soon the whole room is filled with laughter. <<He>> looks back at you. "You'll pay for this," <<he>> says, before storming from the room.
<br><br>

	<<if $baileydefeatedchain gte 2>>
	The <<person2>><<person>> slinks out after <<person1>><<himstop>>
	<br><br>
	<</if>>

The orphans cheer your defeat of the tyrant. <<He>> will be back, but for now you're in the clear.
<<greb>><<ghope>><<reb 1>><<hope 1>><<ltrauma>><<lstress>><<trauma -6>><<stress -12>>
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Bedroom]]>><</link>>

<<elseif $enemyhealth lte 0>>
<<set $baileydefeated += 1>><<set $baileydefeatedchain += 1>>

<<person1>><<He>> backs off for a moment, surprised by your aggression and nursing a bruise. <<He>> notices the crowd formed around the hall. "You'll pay for this. Mark my words." <<He>> storms out.
<br><br>

	<<if $baileydefeatedchain gte 2>>
	The <<person2>><<person>> slinks out after <<person1>><<himstop>>
	<br><br>
	<</if>>

<<His>> departure lifts a weight from the watching orphans, who cheer your defeat of the tyrant. <<He>> will be back, but for now you're in the clear.
<<greb>><<ghope>><<reb 1>><<hope 1>><<ltrauma>><<lstress>><<trauma -6>><<stress -12>>
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Bedroom]]>><</link>>

<<else>>
<<set $baileydefeatedchain to 0>>

You fall to the ground, too hurt to move.
<<lreb>><<reb -1>>
<br><br>

<<set $upperoff to 0>>
<<set $loweroff to 0>>
<<set $underloweroff to 0>>
<<set $underupperoff to 0>>
<<endcombat>>

<<link [[Next|Rent Intro]]>><</link>>

<</if>>