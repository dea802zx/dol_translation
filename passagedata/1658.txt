<<set $outside to 0>><<effects>><<set $lock to 200>>

<<openinghours>>

	<<if $skulduggery gte $lock>>
	<span class="green">The lock looks easy to pick.</span>
	<br><br>

	<<link [[Pick it (0:10)|Ocean Breeze]]>><<pass 10>><<crimeup 1>><</link>><<crime>>
	<br>
	<<else>>
	<span class="red">The lock looks beyond your ability to pick.</span><<skulduggeryrequired>>
	<br><br>
	<</if>>

<<link [[Leave|Cliff Street]]>><</link>>
<br>