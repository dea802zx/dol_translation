<<set $outside to 1>>
<<set $leftarm to "bound">><<set $rightarm to "bound">><<effects>>
<<pass 10>>
Finally, they bind your arms and force you to stand with your legs apart, while they line up citizen volunteers to aid in their body-search. With your arms bound, you can't even protect your modesty as these stranger's eyes pore over your naked flesh. Just when you think it can't get worse, the 'good citizens' grow bolder. Hands start to poke, prod, fondle and grope. Some earlier volunteers return to 'double-check.' The two officers overseeing this 'search' note down any observations.
<br><br>
Humiliated beyond words, you want to be anywhere but here. So when they finally declare the search complete, scoop up your clothing and inform you and the crowd that they need it for evidence, all you can think is that your freedom is at hand. It isn't till a few moments later, as the officers disappear around a corner, that you realise you are standing naked and still helplessly bound in the middle of an agitated crowd. Horrified, you sprint away.
<<gtrauma>><<ggstress>><<ggarousal>><<trauma 6>><<stress 20>><<arousal 2500>>
<br><br>
<<fameexhibitionism 50>>
<<endevent>>
<<destinationeventend>>