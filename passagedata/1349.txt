<<effects>>

You drop to your knees and prostrate yourself on the grass. "P-please help," you begin, before explaining your situation within a broken stream of sobs.
<br><br>

The <<person>> hesitates, but lowers the camera. "Don't cry <<girl>>," <<he>> says. "I was only joking."
<br><br>

<<He>> gives you a towel to cover with, and shows you to the gate.
<<lstress>><<stress -6>>
<br><br>
<<towelup>>
<<endevent>>
<<destinationeventend>>