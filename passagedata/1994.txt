<<effects>>

<<if $submissive gte 1150>>
"Th-thank you," you say "It's nice to feel wanted."
<<elseif $submissive lte 850>>
"Don't worry," you say. "Everyone does."
<<else>>
"You're very sweet," you say. "Thank you for flattering me."
<</if>>
<br><br>
The <<person2>><<person>> laughs again. The <<person1>><<person>> is blushing, but smiling.
<br><br>

<<link [[Next|Orphanage]]>><<endevent>><</link>>
<br>