<<set $outside to 0>><<effects>>
<<set $seductiondifficulty to 8000>>
<<seductioncheck>>
<br><br>
<<if $seductionskill lt 1000>><span class="gold">You feel more confident in your powers of seduction.</span><</if>>
<<seductionskilluse>>
<br><br>
"Perhaps we can make a trade," you croon. "I know what you were doing back there, in the back of the building. I can smell it on you."
<<promiscuity4>>
<<if $seductionrating gte $seductionrequired>>
	The <<person>> blushes and steps away from you. You walk closer, flash <<him>> a mischievous grin and wrap your arms around <<his>> waist.
	<br><br>
	<<link [[Next|Skulduggery Shop Sex]]>><<set $sexstart to 1>><</link>>
	<br>
<<else>>
	"Nice try, harlot," the <<person>> growls. "You're lucky I'm in a good mood. Get out, and don't let me catch you around here again." <<He>> watches you leave the building.
	<br><br>
	<<endevent>>
	<<destinationeventend>>
<</if>>