<<set $outside to 0>><<set $location to "town">><<effects>><<set $bus to "danube">>

<<skulduggerycheck>>
<<if $skulduggerysuccess is 0>>
<<pass 2>>

You stand your ground. A <<person>> walks in and freezes on the spot upon seeing you.

	<<if $submissive gte 1150>>
	"I-I'm the fairy of the fountain. I'm here to grant you a wish," you say, although it sounds less clever than it did in your head.
	<<elseif $submissive lte 850>>
	"I'm the spirit of the fountain, here to punish you for your sins," you say, although it sounds less clever than it did in your head.
	<<else>>
	"I seem to have taken a wrong turn and become lost," you say, although it sounds less clever than it did in your head.
	<</if>>

<br><br>

"Do you think I was born yesterday?" <<he>> says, recovered from <<his>> initial shock. "Stay there, I'm calling the police." You take that as your cue to flee the scene.
<<crimeup 50>><<crime>>
<br><br>

<<if $skulduggery lte 999>>
	<<skulduggeryskilluse>>
<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
<</if>>

<<link [[Next|Danube Street]]>><<endevent>><</link>>
<br>

<<else>>
<<pass 10>>

You dunk yourself in the fountain, drenching you, then sit on the edge. A <<person>> walks in and freezes on the spot upon seeing you. You look at <<himcomma>> giving the best impression of confusion and hopelessness you can muster. "I fell down a drain," you say. "It was dark and scary. I just swam up, now I'm here. Where am I?"
<br><br>

The <<person>> runs over and wraps <<his>> dressing gown around you. "Oh you poor thing." <<he>> says. "We've had animals turn up that way, I never thought..." You keep your head bowed to conceal your smirk as <<he>> continues apologising and fussing over you.
<br><br>

"Are you sure you'll be okay?" <<he>> says once at the front door.
<br><br>

"I will now, thanks to you," you reply. <<He>> let you keep the gown. It looks valuable.
<br><br>

<<set $blackmoney += 60>>

<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
	<<skulduggeryskilluse>>
<<else>>
	<span class="blue">That was too easy. You didn't learn anything.</span>
	<br><br>
<</if>>

<<link [[Next|Danube Street]]>><<endevent>><</link>>
<br>

<</if>>