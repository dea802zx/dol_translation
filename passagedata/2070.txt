<<set $outside to 0>><<set $location to "home">><<effects>>
You throw a broom head at the <<person>> and spring from your hiding place. <<His>> eyes shut and <<he>> holds <<his>> arms to <<his>> face to shield it.
<<if $rng gte 51>>
	One of the other brooms falls in your path, <span class="green">but you leap over it</span>. Heart thumping, you run down the corridor and into the safety of your room before the <<person>> recovers.
	<br><br>
	<<link [[Next|Bedroom]]>><<endevent>><</link>>
	<br>
<<else>>
	One of the other brooms falls in your path, <span class="red">tripping you and leaving you sprawled on the ground</span>.
	<br><br>
	<<He>> recovers before you, and drops the handle in shock when <<he>> notices your <<lewdnessstop>>
	<<if $rng gte 81>>
		<<He>> gasps and leans back against the wall.
	<<elseif $rng gte 61>>
		<<His>> jaw drops as <<he>> ogles you, completely transfixed.
	<<elseif $rng gte 41>>
		<<He>> smiles and whistles at you.
	<<elseif $rng gte 21>>
		<<He>> blushes and covers <<his>> eyes.
	<<else>>
		<<He>> holds <<his>> hands to <<his>> eyes, but peeks between <<his>> fingers.
	<</if>>
	<<if $orphanageexposed is undefined>><<set $orphanageexposed to 1>><<else>><<set $orphanageexposed += 1>><</if>>
	Face red and heart thumping, you dart past <<himcomma>> down the corridor and into your room.
	<br><br>
	<<if $orphanageexposed gte 10 and random(1, 100) gte 51>>
		You're still breathing heavily when quiet footsteps approach, followed by a soft knocking.
		<br><br>
		You open the door and peek around the gap, careful to keep your body hidden. The <<person>> stands in front, but there are others. Many have caught you naked in the halls. Those at the back stand on tiptoes.
		<br><br>
		"Hi," the <<person>> gulps. "I told my friends about what I saw. C-could you do that again for us?"
		<br><br>
		<<if $exhibitionism gte 75>>
			<<link [[Flaunt|Orphanage Flaunt 2]]>><</link>><<exhibitionist5>>
			<br>
		<</if>>
		<<link [[Comply nervously|Orphanage Flaunt Nervous]]>><<trauma 6>><<stress 6>><</link>><<gtrauma>><<gstress>>
		<br>
		<<link [[Refuse|Orphanage Flaunt Refuse]]>><</link>>
		<br>
	<<else>>
		<br><br>
		<<link [[Next|Bedroom]]>><<endevent>><</link>>
		<br>
	<</if>>
<</if>>