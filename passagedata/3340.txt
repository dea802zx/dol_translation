<<set $bus to "elk">><<set $outside to 0>><<effects>>

You approach the creature, and kneel down beside it. You make soothing noises. It seems to have little effect at first, but you continue until its fidgeting calms, and it sleeps soundly.
<br><br>

<<link [[Next|Monster Tower]]>><</link>>
<br>