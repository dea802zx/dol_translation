<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
You rest a hand on their thigh as they explain something to you. Their voice wavers. You ignore their glance in your direction. You keep your focus on the book in front of you, as if nothing were amiss.
<<promiscuity1>>
<<link [[Thank|Asylum Study Thank]]>><</link>>
<br>
<<if $promiscuity gte 35>>
	<<if $pronoun is "m">>
		<<link [[Open fly|Asylum Study Hand]]>><<set $phase to 0>><</link>><<promiscuous3>>
		<br>
	<<else>>
		<<link [[Lift skirt|Asylum Study Hand]]>><<set $phase to 1>><</link>><<promiscuous3>>
		<br>
	<</if>>
<</if>>