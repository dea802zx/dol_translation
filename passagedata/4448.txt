<<effects>><<set $location to "sewers">><<set $outside to 0>>

<<if $slimedisable is "f" and $hallucinations gte 1 and $sewerscountdown isnot 3>>
	You peel off the slime, run and hide around the corner.
<<else>>
	You run and hide around the corner.
<</if>>
You wait. Nothing happens. You peek, then walk back to the safe. It's still locked, and the numbers won't reappear. The ticking hasn't stopped.
<br><br>
<<link [[Next|Sewers Workshop]]>><</link>>
<br>