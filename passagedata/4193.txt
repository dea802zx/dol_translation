<<set $outside to 1>><<set $location to "forest">><<effects>>
<<outergoo>><<outergoo>><<outergoo>><<outergoo>>
"I have an idea to get us out of here," you say. "If we struggle together, we might be able to make the snake throw up." You can't see <<himcomma>> but you think the movement you feel above you is Robin nodding. "Is it going to digest us?" <<He>> whimpers.
<br><br>

The two of you start to stretch and push outwards with your arms and legs as hard as you can, while matching the timing of your movements. The stomach tightens around you, spewing fluids all over you. Your clothing seems to degrade from the fluid, though you're unharmed. Robin doesn't seem to be getting squeezed or drenched the same way you are, maybe because <<he>> isn't quite in the stomach of the snake.
<br><br>

The more you move, the more you are massaged and soaked in fluids. You start having trouble keeping your mouth above the fluid as it fills up the stomach. You cry out in fear. Before you scream however, the squeezing motions of the stomach change and start pushing you upwards, jamming your cheek against Robin's. The two of you are squished against each other as the snake forces you out of its mouth. You and Robin are left laying in a puddle of slime as the ill snake slithers away.
<br><br>

Robin hugs you but doesn't say anything. The ordeal must have been as traumatising for <<him>> as it was for you. You help each other clean off as much slime as you can, but the stickiness makes it hard to remove. At least you found the picnic basket.
<<if $exposed gte 1>>
Robin gives you the rug to cover with and together you return to the orphanage.
<br><br>

<<link [[Next|Bedroom]]>><<endevent>><</link>>
<br>
<<else>>
Together you return to the orphanage.
<br><br>

<<robinoptions>>
<</if>>