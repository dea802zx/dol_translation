<<effects>><<pass 1 hour>><<set $hunger -= 500>><<set $thirst -= 500>><<set $stress -= 500>>
You feel someone shaking your shoulder. You look around and see an old woman smiling at you. She pours you a cup of tea from a flask and gives you a chocolate bar, letting you know that it's important to not let your blood-sugar levels drop too low. You thank her and shortly after the bus arrives at your destination.
<br><br>
She waves from the window as the bus continues on.
<br><br>
<<destination>>
<<displayLinks>>