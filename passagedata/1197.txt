<<effects>>

The barn doors open, and a farmhand enters, holding a leash. The leash tightens. Whatever it's attached to refuses to enter.
<br><br>

With some tugging, and help from a couple of other farmhands, the beast is coerced into the building. It's a pig.
<br><br>

"You've worked with animals before?" Remy asks.
<br>
Niki hesitates. "Yes."
<br>
"Then this should be straightforward. Our animals are well-behaved. Even our cows and pigs get on well with each other." <<He>> scratches behind your ear. "They should be able to stay still for however long it takes."
<br><br>

<<link [[Cooperate|Livestock Job Niki Cooperate]]>><<transform cow 1>><<livestock_obey 5>><<sub 1>><</link>><<ggobey>>
<br>
<<link [[Defy|Livestock Job Niki Defy]]>><<livestock_obey -5>><<def 1>><</link>><<llobey>>
<br>