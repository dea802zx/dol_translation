<<effects>>
You search among the trees at the edge of the clearing, looking for seeding orchids. You find several hiding beneath a bough.
<br><br>
<span class="gold">You can now grow orchids.</span>
<br><br>
<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
<br>