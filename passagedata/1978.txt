<<effects>>

<<if $submissive gte 1150>>
"You need to be able to make money on your own," you say. "People will pay lots for your body."
<br><br>
<<Hes>> taken aback by your bluntness. "I couldn't do that!" <<he>> says.
<br><br>
"You'll be safer that way," you say.
<br><br>
<<elseif $submissive lte 850>>
"You need to learn to stand up for yourself," you say. "Have you considered prostitution?"
<br><br>
<<Hes>> taken aback by your bluntness. "I couldn't do that!" <<he>> says.
<br><br>
"So you'll just beg for help?" you ask. "You're not the only one in trouble."
<br><br>

<<else>>
"You need to be able to make money on your own," you say. "People pay good money for whores."
<br><br>
<<Hes>> taken aback by your bluntness. "I couldn't do that!" <<he>> says.
<br><br>
"You need a more reliable source of money," you say.
<br><br>
<</if>>

<<He>> doesn't look happy, but <<he>> understands.
<br><br>

<<link [[Next|Orphanage]]>><<endevent>><</link>>
<br>