<<effects>>

<<if $malechance lte 0>>
	<<set _horse_gender to "f">>
<<elseif $malechance gte 100>>
	<<set _horse_gender to "m">>
<<else>>
	<<set _horse_gender to "b">>
<</if>>

<<if $livestock_horse gte 2>>
	You brush the centaurs' hair with your fingers. They jostle for their turn. They try to make you scratch their backs, despite having little hair there.
<<else>>
	You brush the centaurs' hair with your fingers. They're wary, but don't turn you down. You get the feeling they enjoy it more than they let on.
<</if>>
<br><br>

<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $danger gte (9900 - $allure)>>
	<<if $rng gte 51>>
		<<set $rng to random(1, 100)>>
		<<if $rng gte 51>>
			<<if $livestock_horse_rescued is undefined>>
				<<generate1>><<person1>>
				An older centaur limps by. Unlike the others, <<he>> ignores you. You look at <<his>> leg. A large, painful-looking splinter is lodged in the skin above <<his>> foot.
				<br><br>

				"Just ignore <<him>>," another centaur says. "Grumpy bastard. Won't let anyone near that wound of <<hers>>. Not even Remy."
				<br><br>

				<<link [[Try to help|Livestock Field Centaur Help]]>><</link>><<tendingdifficulty 700 1000>>
				<br>
				<<link [[Ignore|Livestock Field]]>><<endevent>><</link>>
				<br>
			<<else>>
				<<if $livestock_horse_rescued is "m">>
					<<generatem1>>
				<<else>>
					<<generatef1>>
				<</if>>
				<<person1>>The old centaur you helped gallops by, making the most of <<his>> healed leg. <<He>> smiles and waves at you as <<he>> passes.
				<<ltrauma>><<trauma -6>>
				<br><br>

				<<link [[Next|Livestock Field]]>><<endevent>><</link>>
				<br>

			<</if>>
		<<else>>
			<<generate1>><<person1>>"How cute," says a voice behind the fence. It's a <<personstop>> <<Hes>> dressed as a farmhand. "Cows and horses being best friends. I should film this. Might go viral." The centaur bristle at <<his>> mocking tone, but don't say anything.
			<br><br>

			<<He>> takes a bite of an apple. The centaur stare longingly at the fruit. "What's the matter?" the <<person>> asks. "Bored of grass? Well too bad. No sweet, juicy apples for you." To drive home the point, <<he>> discards the uneaten apple some way from the fence. Just out of reach.
			<br><br>

			A couple of younger centaur rush over when the farmhand is gone. They try to reach through a gap, but the electric fence painfully thwarts them.
			<br><br>

			<<link [[Next|Livestock Field]]>><<endevent>><</link>>
			<br>
		<</if>>
	<<else>>
		<<if $malechance lt random(1, 100)>>
			One centaur seems frustrated. She stomps at the others, and is rude and demanding.
			<br><br>

			<<if $bestialitydisable is "f">>
				<<if $dgchance gte random(1, 100)>>
					<<link [[Look beneath her|Livestock Field Centaur Female Vagina]]>><</link>><<deviant1>>
					<br>
				<<else>>
					<<link [[Look beneath her|Livestock Field Centaur Female]]>><</link>><<deviant1>>
					<br>
				<</if>>
			<</if>>
			<<link [[Ignore|Livestock Field]]>><</link>>
			<br>
		<<else>>
			One centaur seems frustrated. He stomps at the others, and is rude and demanding.
			<br><br>

			<<if $bestialitydisable is "f">>
				<<if $cbchance gte random(1, 100)>>
					<<link [[Look beneath him|Livestock Field Centaur Male Vagina]]>><</link>><<deviant1>>
					<br>
				<<else>>
					<<link [[Look beneath him|Livestock Field Centaur Male]]>><</link>><<deviant1>>
					<br>
				<</if>>
			<</if>>
			<<link [[Ignore|Livestock Field]]>><</link>>
			<br>
		<</if>>
	<</if>>
<<else>>
	<<link [[Next|Livestock Field]]>><</link>>
	<br>
<</if>>