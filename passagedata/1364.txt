<<widget "parkex1">><<nobr>>

<<generate1>>You move on, keeping to the bushes as much as you can. As you peek around a tree, a pair of arms wrap round you from behind and force you to the ground.
<br><br>

<<set $molestationstart to 1>>

<span id="next"><<link [[Next->Park Woman]]>><</link>></span><<nexttext>>

<</nobr>><</widget>>

<<widget "parkex2">><<nobr>>

You hear a growl beside you.
<br><br>

<<if $daystate isnot "night">>
There are people about. If you run, you'll be seen.
<br><br>
<</if>>

<<if $bestialitydisable is "f">>

<<link [[Run|Park Ex Run]]>><</link>>
<br>
<<link [[Stay put|Park Ex Dog Molestation]]>><<set $molestationstart to 1>><</link>>
<br>

<<else>>
<<link [[Run|Park Ex Run]]>><</link>>
<br>
<</if>>

<</nobr>><</widget>>

<<widget "parkex3">><<nobr>>
You come to a bridge over a river. It's quite long, and if someone were to arrive while you crossed there'd be nowhere to hide.
<br><br>

<<link [[Risk the bridge|Park Ex Bridge]]>><<stress 6>><</link>><<gstress>>
<br>
<<link [[Swim the river|Park Ex River]]>><</link>><<swimmingdifficulty 1 500>>
<br>
<</nobr>><</widget>>