<<set $outside to 1>><<set $location to "forest">><<effects>>
<<if $foresthunt gte 1>>
<<set $foresthunt += 1>>
<</if>>
<<if $phase is 1>>
<<set $phase to 0>>
You search for plants long and sturdy enough to build an improvised garment. You find some suitable specimens and tie them together around your chest. It's fragile and revealing, and you don't think you could take it off without breaking it, but it's better than nothing.
<<plantupper>>
<br><br>

<<elseif $phase is 2>>
<<set $phase to 0>>
You search for plants long and sturdy enough to build an improvised garment. You find some suitable specimens and tie them together around your waist. It's fragile and revealing, and you don't think you could take it off without breaking it, but it's better than nothing.
<<plantlower>>
<br><br>
<</if>>

<<link [[Next|Forest]]>><<set $eventskip to 1>><</link>>
<br>