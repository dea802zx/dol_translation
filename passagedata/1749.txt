<<effects>>

<<flaunting>> you step outside. You're not wearing normal outdoor attire. A few people glance over and look you up and down, examining your contours.
<<if $ex_studio isnot 1>>
<<set $ex_studio to 1>><<exhibitionism3>>
<<else>>
<br><br>
<</if>>

Your heart beats faster as you walk away from the dance studio, and away from relative safety.
<br><br>

<<link [[Next|Barb Street]]>><</link>>
<br>