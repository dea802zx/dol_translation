<<set $outside to 0>><<set $location to "pool">><<schooleffects>><<effects>>

<<if $delinquency gte 400>>
<<endevent>><<npc Mason>><<person1>>You shove the pair aside. They cry out in surprise, attracting Mason's attention. "I knew it would have something to do with you. Stop harassing the other students." <<He>> doesn't give your protests any heed.
<<gdelinquency>><<detention 1>>
<br><br>
<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
<br>
<<else>>
<<endevent>><<npc Mason>><<person1>>You shove the pair aside. They cry out in surprise, attracting Mason's attention. <<He>> looks at the pair. "You two again. Stop harassing the other students." <<He>> herds them away from you.
<br><br>
<<link [[Next|Swimming Lesson]]>><<endevent>><</link>>
<br>
<</if>>