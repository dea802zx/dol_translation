<<set $outside to 1>><<set $location to "beach">><<effects>>
<<danceeffects>>
<<danceaudience>>
<<danceactions>>

<<if $danceevent is 0 and $exhibitionism lte 74 and $exposed gte 2>>
	There's no way you can continue dancing while so exposed! Face reddening, you flee the scene.
	<br><br>
<<elseif $danceevent is 0 and $exhibitionism lte 34 and $exposed gte 1>>
	There's no way you can continue dancing while so exposed! Face reddening, you flee the scene.
	<br><br>
<</if>>

<<if $danceevent is "rape">>
	<<link [[Next|Beach Party Dance Rape]]>><<set $molestationstart to 1>><</link>>
<<elseif $danceevent is "finish">>
	<<link [[Next|Beach Party]]>><<clotheson>><<endevent>><</link>>
<<elseif $danceevent is 0>>
	<<if $exposed gte 2 and $exhibitionism lte 74>>
		<<link [[Flee|Changing Room]]>><<clotheson>><<endevent>><</link>>
	<<elseif $exposed gte 1 and $exhibitionism lte 34>>
		<<link [[Flee|Changing Room]]>><<clotheson>><<endevent>><</link>>
	<<else>>
		<<link [[Stop|Beach Party]]>><<clotheson>><<endevent>><</link>>
	<</if>>
<</if>>