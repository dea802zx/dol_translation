<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

"Here you go." <<He>> tosses your clothes to the side. You swim over and restore your dignity. When you turn, the <<person>> is gone.
<br><br>

<<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Swimming Lesson]]>><</link>>

<<elseif $enemyhealth lte 0>>

The <<person>> recoils in pain and glares at you. <<He>> tries to swim away, but you grab the clothes still held in <<his>> hand. You win the ensuing battle, and quickly dress. When you turn, the <<person>> is gone.
<br><br>

<<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Swimming Lesson]]>><</link>>

<<elseif $alarm is 1 and $rescue is 1>>
<<set $rescued += 1>>

	<<if $delinquency gte 200>>
	Your cry attracts Mason's attention. "I knew it would have something to do with you. Stop harassing the other students. And put your clothes back on, for heaven's sake," the <<person>> is nowhere to be seen, they left your clothes floating on the water.
	<<gdelinquency>><<detention 1>>
	<<else>>
	Your cry attracts Mason's attention. The teacher glares at the <<person>> "Give those clothes back, now. You're in serious trouble, get changed and go to the head's office." You dress before any more attention is attracted to your predicament.
	<</if>>
	<br><br>

<<tearful>> you gather yourself.
<br><br>
<<clotheson>>
<<endcombat>>

<<link [[Next|Swimming Lesson]]>><</link>>

<</if>>