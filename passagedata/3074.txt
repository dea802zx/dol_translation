<<effects>>

<<if $submissive gte 1150>>
	"W-wait," you say. "I can dance for you. I can entertain you that way."
<<elseif $submissive lte 850>>
	"Not letting scum like you touch me," you say. "Sorry. You can look though. How about I give you a dance?"
<<else>>
	"I can dance for you instead," you say.
<</if>>
<br><br>

The <<person1>><<person>> doesn't look impressed, but the <<person2>><<person>> chimes in. "Come on captain. We'll be port soon, with whores a-plenty. How many of them can dance for shit though?"
<br><br>

The <<person1>><<person>> grumbles, but is convinced. They clear a space for you in the ship interior, open some drinks, and sit on a bench opposite. The <<person3>><<person>> turns on an old radio, and fiddles with the signal until <<he>> finds music.<<person1>>
<br><br>

<<if $exposed gte 2>>
	You're conscious of your <<nuditystop>> It'll be difficult to dance without exposing yourself further. There's no way they'll give you towels before they're entertained.
	<br><br>

	<<link [[Stay covered|Sea Boat Dance 2]]>><<set $phase to 2>><</link>><<dancedifficulty 1 1000>>
	<br>
	<<link [[Expose yourself to make dancing easier|Sea Boat Dance 2]]>><<set $phase to 1>><<fameexhibitionism 3>><<trauma 6>><<stress 6>><</link>><<dancedifficulty 1 600>><<gtrauma>><<gstress>>
<<elseif $exposed gte 1>>
	You're conscious of your <<nuditystop>> It'll be difficult to dance without exposing yourself further. There's no way they'll give you towels before they're entertained.
	<br><br>

	<<link [[Stay covered|Sea Boat Dance 2]]>><<set $phase to 2>><</link>><<dancedifficulty 1 1000>>
	<br>
	<<link [[Expose yourself to make dancing easier|Sea Boat Dance 2]]>><<set $phase to 1>><<fameexhibitionism 3>><<trauma 6>><<stress 6>><</link>><<dancedifficulty 1 600>><<gtrauma>><<gstress>>

<<else>>
	<<link [[Dance|Sea Boat Dance 2]]>><<set $phase to 0>><</link>><<dancedifficulty 1 600>>
<</if>>