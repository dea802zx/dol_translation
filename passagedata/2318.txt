<<effects>>

Winter rushes over once your feet touch the earth. <<He>> steadies the chair and removes the straps binding you. "That ends our demonstration," <<he>> says. "Thank you for attending. We hope you found it informative, and we invite you to visit the museum. The address is on the booklets we handed out. It's right over there." <<He>> points. "Last but not least, let's have a round of applause for our star." <<He>> gestures at you. The applause becomes deafening. Heads turn around the park, perplexed at the celebration.
<br><br>

<<link [[Next|Museum Duck Extreme Antique 2]]>><</link>>
<br>