<<effects>>

You close your eyes and brace yourself for the pain. The sensation stings, but there's nothing you can do but endure it.
<br><br>

Over time, though, you grow accustomed to the sting, and even a bit bored as it drags on. At last, Remy steps away from your <<bottom>> to examine <<his>> handiwork.
<<add_bodywriting left_bottom cattle_brand tattoo>>
<br><br>

"Hopefully the ink won't fade this time," Remy says. "Or was it removed by poachers? No matter."
<br><br>

<<link [[Next|Livestock Return 3]]>><</link>>
<br>