<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<npc Kylar>><<person1>>

You sit beside Kylar and say hello.

	<<if $NPCName[$NPCNameList.indexOf("Kylar")].rage gte 95>>

	<<Hes>> quiet, only speaking to point out something behind you. It's gone by the time you look.
	<br><br>

	You finish the food.
	<br><br>

	<span class="red">You stagger as you rise to your feet.</span> You feel light-headed. "Are you okay?" you hear Kylar say through thickening fog. "You need the nurse." <<He>> puts an arm around your shoulder and steers you towards the door. You black out.
	<br><br>
	<<endevent>>
	<<link [[Next|Kylar Basement]]>><</link>>
	<br>

	<<elseif $NPCName[$NPCNameList.indexOf("Kylar")].love gte 50>>
		<<if $rng gte 81 and $kylarcanteen isnot 1>><<set $kylarcanteen to 1>>
		"Th-thanks," <<he>> begins. "Thanks for sitting with me."
		<<elseif $rng gte 81>>
		<<Hes>> distracted by <<his>> cutlery. <<His>> knife in particular.
		<<elseif $rng gte 61>>
		<<He>> rests <<his>> head on your shoulder.
		<<elseif $rng gte 41>>
		"The food is awful," <<he>> says, playing with it. "I usually buy something straight after school."
		<<elseif $rng gte 21>>
		"English was fun," <<he>> says. "I'm glad we share a class."
		<<else>>
		<<Hes>> eager to chat, and keeps touching your thigh.
		<</if>>
	<br><br>
	You finish your food. Kylar carries your trays away, then walks in the direction of the <<if $weather is "rain">>library<<else>>rear playground<</if>>.
	<br><br>

	<<link [[Next|Canteen]]>><<endevent>><</link>>
	<br>

	<<else>>

	<<He>> stops spearing <<his>> food, but doesn't respond. You try to make small talk. You think <<hes>> listening.
	<br><br>

	You finish your food and depart. Kylar walks in the direction of the <<if $weather is "rain">>library<<else>>rear playground<</if>>.
	<br><br>

	<<link [[Next|Canteen]]>><<endevent>><</link>>
	<br>

	<</if>>