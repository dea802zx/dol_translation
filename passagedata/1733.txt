<<set $outside to 0>><<set $location to "compound">><<effects>>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $danger gte (9900 - $allure) and $eventskip is 0>>
	<<if $rng gte 51 and $tentacledisable is "f" and $hallucinations gte 2>>
		As you cross the walkway several tentacles rise from the fluid beneath you.
		<br><br>
		<<link [[Next|Elk Compound Tentacles]]>><<set $molestationstart to 1>><</link>>
		<br>
	<<else>>
		As you cross the walkway the fluid beneath you becomes agitated. Some splashes you, eating through your clothing.
		<<set $worn.upper.integrity -= 10>><<set $worn.lower.integrity -= 10>>
		<br><br>
		You continue to the centre and take the crystal. It's warm to the touch.
		<br><br>
		<<set $compoundpipes to 1>>
		<<set $blackmoney += 50>>
		<<set $compoundalarm += 1>>
		<<crimeup 50>>
		<<link [[Next|Elk Compound]]>><</link>>
		<br>
	<</if>>
<<else>>
	You cross the walkway and take the crystal. It's warm to the touch.
	<br><br>
	<<set $compoundpipes to 1>>
	<<set $blackmoney += 50>>
	<<set $compoundalarm += 1>>
	<<crimeup 50>>
	<<link [[Next|Elk Compound]]>><</link>>
	<br>
<</if>>