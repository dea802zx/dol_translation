<<effects>>

<<if $schoolday is 1>>
	<<if $schoolstate is "lunch">>
		You open your eyes and see that few people were watching you all the time. You smile and say nothing.
		<br><br>
	<<elseif $schoolstate is "early">>
		You begin thinking that every day at school should start this way.
		<br><br>
	<<elseif $schoolstate is "late">>
		Who may thought that studying late could be so fun?
		<br><br>
	<<else>>
		You look around and try to stay unnoticed. Or maybe you're not?
		<br><br>
	<</if>>
<<else>>
	You think it may be interesting to do that again someday with all the people around.
	<br><br>
<</if>>

<<endmasturbation>>
<<clotheson>>
<<endcombat>>

<<link [[Next|School Library]]>><</link>>
<br>