<<set $outside to 0>><<set $location to "town">><<effects>>

You enter the town hall. Rows of chairs face the stage opposite you. There's a projector set up. You recognise others from your class, holding their solutions close to their chests. Leighton and River stand beside the stage, talking with two people wearing suits. You don't recognise them.
<br><br>

You can enter your solution, or just watch. Taking part will make the teachers think better of you whether you win or not.
<br><br>
<<set $mathschance = Math.clamp($mathschance, 0, 100)>>
<<link [[Enter the competition (3:00)|Maths Competition Enter]]>><<pass 1 hour>><<pass 2 hours>><</link>> <span class="gold">$mathschance% chance of winning.</span>
<br>
<<link [[Just watch (3:00)|Maths Competition Watch]]>><<set $mathschancestart to $mathschance>><<pass 1 hour>><<pass 2 hours>><</link>>
<br>
<<link [[Leave (0:05)|Cliff Street]]>><<pass 5>><<endevent>><</link>>
<br>