<<set $outside to 1>><<set $location to "lake">><<effects>><<lakeeffects>>
<<set $forest to 40>><<set $bus to "lakewaterfall">>

<<if $phase is 1>>
You take a stroll around the Lake.

	<<if $weather is "rain">>
		The water is alive with motion as rain breaks its surface.
	<<else>>
		The water is calm.
	<</if>>

	<<if $laketeenspresent is 1>>
		You hear students are playing in the waterfall and sitting in the pond above.
	<<elseif $daystate is "night">>
		Moonlight reveals the area around you and turns the falling water into a curtain of silver.
	<<else>>
		A calming wind gently caresses you.
	<</if>>
	<br>
	You think about all the things that happened lately, the stillness around you helps digest it.
	<br><br>
	You feel refreshed.

	<<set $eventskip to 0>>
	<br><br>
	<<link [[Return|Lake Shore]]>><<clotheson>><</link>>

<<elseif $phase is 2>>

<<flaunting>> you walk around the edge of the Lake. There are people scattered around, but you don't think they can tell how exposed you are.
<<exhibitionism5>>

<<if $vaginasemen gte 2>> A bit of semen drips from your <<pussy>> onto the ground.<</if>>

<<if $beauty gte (($beautymax / 7) * 3)>>
<<generate1>>As you walk further you notice a <<person1>><<person>> lying on <<his>> blanket trying to peek at your <<genitalsstop>> You face <<him>> and wave. <<He>> blushes and looks away.
<br><br>
Amused by <<his>> response, you continue your walk.
<br><br>
<<endevent>>
<</if>>

	<<if $rng gte 70 and $lakestealgold is 0>>
	You spot a couple swimming together. One points at the shore, then they both dive.
	<br>
	You peer towards the spotted they pointed at, and see a small camp between the trees.
	<br><br>

	<<link [[Check it out |Lake CampGold]]>><<set $phase to 1>><</link>>
	<br>
	<<link [[Continue your walk |Lake CampGold]]>><<set $phase to 2>><</link>>
	<br>
	<<else>>

	You finish your walk. You feel refreshed.
	<br><br>
	<<endevent>>
	<<set $eventskip to 0>>

	<br><br>
	<<link [[Return |Lake Shore]]>><<clotheson>><</link>>
	<</if>>

<<elseif $phase is 3>>

<<flaunting>> you walk around the edge of the lake. You don't see anybody nearby, but being so exposed is thrilling.
<<exhibitionism4>>
<br><br>

<<if $vaginasemen gte 2>>
A bit of semen drips from your <<pussy>> onto the ground as you walk.
<</if>>

	<<if $rng gte 80 and $deviancy gte 15 and $vaginaexist is 1 and $swarmdisable is "f">>

	After a few minutes you find a nice glade. The soft wind feels nice. You decide to lie down.
	<br><br>
	You close your eyes and listen to the sound of the forest.
	<br><br>
	Your crotch tickles. You look down
		<<if $arousal gte 4000>>
		and see a few bugs and insects crawling around your <<genitalscomma>> mesmerised by the leaking fluid.
		<br><br>
		<<else>>
		and see a few bugs and insects crawling on your <<genitalsstop>>
		<br><br>
		<</if>>

		<<if $deviancy gte 35>>
		<br>
		<<link [[Let them explore|Lake Insect]]>><<set $phase to 1>><</link>><<deviant3>>
		<</if>>
		<<if $deviancy gte 55>>
		<br>
		<<link [[Masturbate|Lake Insect]]>><<set $phase to 2>><</link>><<deviant4>>
		<</if>>
		<br>
		<<link [[Brush them away|Lake Insect]]>><<set $phase to 3>><</link>>
		<br>
	<<else>>

		You finish your walk. You feel refreshed.

		<<endevent>>
		<<set $eventskip to 0>>
		<br><br>
		<<link [[Return |Lake Shore]]>><<clotheson>><</link>>
	<</if>>
<</if>>