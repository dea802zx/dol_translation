<<set $outside to 0>><<set $location to "museum">><<effects>>

Winter nods. <<He>> peeks around the door. "Everyone's in. It's time. Wait for you cue." <<He>> returns to the museum proper.
<br><br>

"Ladies and gentlemen," Winter begins. <<He>> stands on a raised platform beside the horse, addressing an audience of around fifty people. "Our town has a rich history. Of great deeds and heroism yes, but also of tragedy and brutality. Take this <<girlcomma>>" <<He>> gestures at the door, and you <<nervously>> emerge into the museum. A <<person2>><<person>> stands closest to you. <<He>> licks <<his>> lips.
<br><br>

<<person1>>"Caught stealing bread for <<pher>> starving sibling," Winter continues. "<<pShe>> must now face the lord's justice." You climb atop the horse and ease yourself down. You feel it press into your crotch like before.<<masopain 5>><<arousal 1200>>
<br><br>

Winter continues <<his>> speech, talking about the specifics of the horse. And how it was used. <<He>> steps behind you while doing so, and begins to tie your arms. <<He>> works fast, and before long your arms are bound and immobile.
<br><br>

The audience is enraptured by the spectacle. You feel their eyes on your barely-covered <<genitalsstop>> "Of course, humiliation was part of the punishment. They would often have an audience, much like this. And when it was over, the victim would be given to the audience to do with as they please." A <<person3>><<persons>> eyes light up and <<he>> steps forward, but Winter blocks <<his>> path.<<person1>>
<br><br>

"That ends our demonstration. Thank you for attending. We hope you found it informative, and we invite you to appreciate the many other antiques we have on display. Last but not least, let's have a round of applause for our star." <<He>> gestures at you. The audience applaud politely, though many look like they wish it had gone further.
<br><br>

"You did very well," Winter says once you're back in the small room. "I'm blessed to have your assistance. Thank you. I'd like to hold another demonstration next weekend if it suits you." <<He>> peeks around the door. "I think we've earned some new interest." <<He>> leaves you to get dressed.
<br><br>
<<endevent>>
<<set $museuminterest += 20>>
<<link [[Next|Museum]]>><<unbind>><<clotheson>><</link>>
<br>