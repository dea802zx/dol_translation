<<effects>>

You twist around and shove the <<person1>><<person>> away from you. <<He>> grasps the stall's support and almost brings the roof down as <<he>> tumbles. <<He>> shoots you a glare before climbing to <<his>> feet and walking away.
<br><br>

<<tearful>> you turn back to your display.
<br><br>

<<stall_actions>>