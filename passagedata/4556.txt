<<effects>>

<<if $phase gte 8>>
	The fluid laps at your face. You're preparing to hold your breath when the hose retracts. You hear the pair walking away.
	<br><br>

	You pull yourself from the barrel with shaking arms, climbing free of the fluid. You're inside a factory. Large machines work all around you, fed by conveyor belts carrying twisted purple plants. There's no one around.
	<br><br>

	<<tearful>> you stagger towards the exit. You don't meet anyone, but the lewd thoughts haven't dissipated. You imagine they'll linger for a while.
	<br><br>
	<<endevent>>
	<<earnFeat "Neck Deep">>
	<<link [[Next|Elk Street]]>><<set $eventskip to 1>><</link>>
	<br>
<<elseif $arousal gte 10000>>
	<<orgasmpassage>> Your moans echo within the barrel. The pair outside fall quiet, then two pairs of arms reach in. They haul you out, leaving you sprawled and panting on the floor.
	<br><br>

	"Well what do we have here?" says a <<person1>><<personstop>> "The perfect opportunity to show you just what I mean."
	<br><br>

	<<link [[Next|Industrial Ex Aphrodisiac Rape]]>><<set $molestationstart to 1>><</link>>
	<br>

<<elseif $pain gte 100>>
	You cry out in pain, unable to keep it in any longer. The pair outside fall quiet, then two pairs of arms reach in. They haul you outside, leaving you sprawled and panting on the floor.
	<br><br>

	"Well what do we have here?" says a <<person1>><<personstop>> "The perfect opportunity to show you just what I mean."
	<br><br>

	<<link [[Next|Industrial Ex Aphrodisiac Rape]]>><<set $molestationstart to 1>><</link>>
	<br>

<<else>>
	<<if $phase is 0>>
		The pair continue their discussion. "What if I'm caught taking it?" The pink fluid spills over the floor of the barrel.
	<<elseif $phase is 1>>
		"No one will notice a little missing." The pink fluid rises to your ankles.
	<<elseif $phase is 2>>
		"Don't they sell it in pill form? That sounds safer." The pink fluid rises to your knees.
	<<elseif $phase is 3>>
		"That shit's watered down. They send the good stuff somewhere else." The pink fluid laps at your <<bottomstop>>
	<<elseif $phase is 4>>
		"Where?" The pink fluid reaches your waist.
	<<elseif $phase is 5>>
		"Fuck if I know. Someone must be buying it." The pink fluid engulfs your <<breastsstop>>
	<<elseif $phase is 6>>
		"Stop asking questions. I don't know much more than you." The pink fluid reaches your neck.
	<<else>>
		"Shift's over anyway." The pink fluid almost engulfs you completely. You point your nose up in order to breathe.
	<</if>>
	<<set $phase += 1>><<set $drugged += 60>>
	<br><br>
	<<if $arousal gte 8000>>
		<span class="red">The liquid seeps into your skin, confusing your senses and assaulting your mind with thoughts of unspeakable lust. You're on the verge of climax.</span>
	<<elseif $arousal gte 6000>>
		<span class="pink">Your thoughts become harder and harder to control. You can't keep this up much longer.</span>
	<<elseif $arousal gte 4000>>
		<span class="purple">Your heart beats faster as the fluid works its wickedness on your senses.</span>
	<<elseif $arousal gte 2000>>
		<span class="blue">An unnatural lewd warmth builds within you.</span>
	<<else>>
		<span class="lblue">You feel calm and collected, considering the circumstances.</span>
	<</if>>
	<br><br>
<<link [[Try not to think lewd thoughts|Industrial Ex Aphrodisiac Willpower]]>><</link>><<willpowerdifficulty>>
<br>
<<link [[Distract yourself with pain|Industrial Ex Aphrodisiac Pain]]>><<pain 12>><</link>><<ggpain>>
<br>
<</if>>