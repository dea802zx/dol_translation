<<set $outside to 0>><<set $location to "docks">><<dockeffects>><<effects>>

"Attention everybody," you shout. The passengers fall silent. "This ship is on a collision course with another. You need to steer away from the docks. Or prepare to swim ashore." You turn and walk away. The crowd erupts into anxious babbling the moment you disappear from view. You descend to your ship and steer away.
<br><br>

Soon after, the larger vessel starts to turn.
<br><br>

You arrive back at the docks. A manager greets you. <<generate2>><<person2>>"You did it," <<he>> says. "Didn't expect that. Good job."
<br><br>

<<dockoptions>>