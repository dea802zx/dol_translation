<<effects>>
<<He>> directs you towards the exit.
<<if random(0,100) gte 50>>
	"You know what?" <<He>> questions, "There aren't any cameras in this spot."
	<br><br>
	You look at <<him>> with a worried expression just before <<he>> grabs hold of you.
	<br><br>
	<<link [[Next|Clothing Shop Night Guard Molestation]]>><<set $molestationstart to 1>><</link>>
<<else>>
	You walk down the stairs in silence and as you reach the main entrance to the Shopping Centre,
	the guard opens the door for you.
	<br><br>
	"I hope this is a good lesson for you," <<He>> smiles, "You should look for more honest ways of
	earning money. Less trouble that way for everyone."
	<br><br>
	<<He>> locks the door as you leave.
	<br><br>
	<<link [[Leave|High Street]]>><<endevent>><</link>>
<</if>>