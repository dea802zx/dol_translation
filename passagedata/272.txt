<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
"That's not true," you say. "People have been exposing me with force."
<br><br>
<<He>> shakes <<his>> head and writes something on <<his>> clipboard. "That's okay," <<he>> says. "But one day you'll need to admit your lewdity." <<He>> glances at the clock. "That's all for today. Be well."
<br><br>
<<endevent>>
<<link [[Next|Asylum]]>><</link>>
<br>