<<effects>>
<<earnFeat "Cornered Rogue">>
Your eyes were so drawn by the water that it takes you a moment to notice the <<fox_text>>. <<fox_He>> stares at you, your clothes at <<fox_his>> feet. <<fox_He>> gives the entrance a furtive glance.
<br><br>

<<if $fox is "male" or $fox is "female">>

	<<if $museumAntiques.antiques.antiquestonetalisman isnot "found" and $museumAntiques.antiques.antiquestonetalisman isnot "talk" and $museumAntiques.antiques.antiquestonetalisman isnot "museum">>
		"I-I'm sorry," <<fox_he>> says, fearful. "I didn't expect you to follow me here." <<fox_He>> glances around, then <<fox_his>> ears perk up. "I know!" <<fox_he>> continues. "I'll give you a present if you don't hurt me." <<fox_He>> lifts your clothes. Beneath them is just another stone. As you look closer though, you see the stone is engraved. It looks intricate. It might be valuable to a collector.
		<br><br>
		"I found it in this cave. Fair?"
		<br><br>

		<<link [[Accept|Meadow Cave Accept]]>><</link>>
		<br>
	<<else>>
		"I-I'm sorry," <<fox_he>> says, fearful. "I didn't expect you to follow me here." <<fox_He>> glances around. "Please don't hurt me."
		<br><br>
		<<link [[Forgive|Meadow Cave Accept]]>><</link>>
		<br>
	<</if>>


	<<if $submissive lte 500 and $promiscuity gte 55>>
		<<link [[Punish|Meadow Cave Punish]]>><<def 1>><<trauma -6>><<stress -6>><</link>><<promiscuous4>><<defianttext>><<ltrauma>><<lstress>>
		<br>
	<</if>>
<<else>>
	It runs for the tunnel, leaving your clothes behind. <<clotheson>>
	<br><br>


	<<if $museumAntiques.antiques.antiquestonetalisman isnot "found" and $museumAntiques.antiques.antiquestonetalisman isnot "talk" and $museumAntiques.antiques.antiquestonetalisman isnot "museum">>
		You're about to leave when you notice something strange about one of the stones. It's engraved. It might be worth something to a collector.
		<<set $antiquemoney += 1200>><<museumAntiqueStatus "antiquestonetalisman" "found">>
		<br><br>
	<<else>>

	<</if>>

	You turn away from the water, and walk back down the tunnel.
	<br><br>

	<<link [[Next|Meadow Cave Exit]]>><<unset $fox>><</link>>
	<br>
<</if>>