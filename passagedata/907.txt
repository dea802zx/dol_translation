<<effects>>

<<person2>>
Alex seems less tense now the <<person>> is gone. <<person1>>"Sorry," <<he>> says, turning to you. "You weren't the first to respond to my sign. There are nasty types around. Stay clear of them."
<br><br>

<<He>> leans against the fence and watches the lane leading away. "I was relieved when you showed up." <<He>> laughs. "Come on, let's get back to work."
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>