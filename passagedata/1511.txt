<<effects>><<set $location to "brothel">><<set $outside to 0>>

<<if $daystate isnot "day" and $daystate isnot "dawn" and $rng % 3>>The Brothel has been busy and so have the showers. The shower head blasts you with ice cold water that takes your breath away. You figure a cold shower is better than no shower, and make do.
<<larousal>>
<br><br>
<<elseif $daystate isnot "night" and $rng % 2>>The shower head blasts you with ice cold water that takes your breath away. Seems like someone's used all the hot water. You figure a cold shower is better than no shower, and make do.
<<larousal>>
<br><br>
<<else>>You're relieved to find there is still hot water.<</if>>

You wash until you're squeaky clean.<<wash>>
<br><br>

<<link [[Get out->Brothel Bathroom]]>><<clotheson>><</link>>
<br>