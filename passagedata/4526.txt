<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "danube">>
<<if $deviancy gte 75>>
You grasp the top of the crate and haul yourself up, pushing the lid off as you do. Worms cascade from you as you come out of your writhing cell, but many remain on you which you actually don't mind.
<br><br>
Fortunately whoever freed you didn't stick around to see you in this state; you're alone. You glance around at your surroundings as you brush off reluctantly the remaining worms. You appear to be on Danube Street, at the delivery entrance of a stately manor. You hear voices in front of the truck that delivered you, and realise you should leave. You suspect worms will be turning up in strange places for a while. You thought doesn't bother you.
<<else>>
<<tearful>> you grasp the top of the crate and haul yourself up, pushing the lid off as you do. Worms cascade from you as you struggle out of your writhing cell, but many remain on you. Fortunately whoever freed you didn't stick around to see you in this state; you're alone. You glance around at your surroundings as you brush off the remaining worms. You appear to be on Danube Street, at the delivery entrance of a stately manor. You hear voices in front of the truck that delivered you, and realise you should leave. You suspect worms will be turning up in strange places for a while.
<</if>>
<br><br>
<<endcombat>>
<<danubequick>>