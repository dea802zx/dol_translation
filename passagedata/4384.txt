<<effects>><<set $location to "sewers">><<set $outside to 0>>
Morgan inundates you with facts and formulae.
<br><br>
<<if $rng gte 75>>
	<<His>> take on things seems strange.
	<<lmaths>><<set $maths -= 1>><<set $school -= 1>>
	<br><br>
<<elseif $rng gte 50>>
	<<His>> take on things seems strange.
	<<ggmaths>><<mathsskill>><<mathsskill>>
	<br><br>
<<else>>
	<<He>> rambles a lot, but has moments of greater lucidity.
	<<gmaths>><<mathsskill>>
	<br><br>
<</if>>
<<link [[Next|Sewers Morgan]]>><</link>>
<br>