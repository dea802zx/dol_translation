<<effects>>
<<set $police_high to 1>><<earnFeat "A Thunderous Response">>
You drop to the ground and crawl between the legs of the bystanders, now focused on each other. You weave your way to the edge of the fight, dodging footfalls and making it through unharmed.
<br><br>

You climb to your feet once clear of the crowd, and dash the rest of the way to the park. No one stops you. You make it to the safety of the trees and high bushes. You hear the heavy clop of hooves as mounted police arrive to break up the fight.
<br><br>

<<cream_end>>
<<link [[Next|Park]]>><<endevent>><</link>>
<br>