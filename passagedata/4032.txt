<<set $outside to 0>><<effects>>
<<if $enemyarousal gte $enemyarousalmax>><<hope 1>>
	<<ejaculation>>
	"I love Halloween," <<he>> gasps. "Take your sweets, slut." <<He>> staggers further into the building.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>

	<<tearful>> you pick up the sack of sweets and step outside. The orphans are overjoyed by your success. They crowd around as you pass out the treats. Robin seems oblivious to what you just did.
	<br><br>

<<elseif $enemyhealth lte 0>><<hope 1>>
	You shove <<him>> back. <<He>> trips over the sack and crashes to the floor.
	<br><br>

	<<clothesontowel>>
	<<endcombat>>
	<<tearful>> you snatch up the treats and dash outside. The orphans are overjoyed by your success. They crowd around as you pass out the treats. Robin seems oblivious to what you just did.
	<br><br>

<<elseif $rescue is 1 and $alarm is 1>>
	You hear a tap at the door. "Are you okay?" It's Robin.
	<br><br>
	The <<person>> pulls away from you. "Fucking slut," <<he>> snarls. "No sweets for you." <<He>> lifts the sack and walks deeper into the building.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<tearful>> you stagger from the building. The orphans are disappointed to see you empty-handed.
	<br><br>
	Robin tries to cheer everyone up, and points out that there are still many houses to go.
<br><br>

<<else>><<hope -1>>
	"Fine," the <<person>> snarls. "No sweets for you." <<He>> lifts the sack and walks deeper into the building.
	<br><br>
	<<clothesontowel>>
	<<endcombat>>
	<<tearful>> you stagger from the building. The orphans are disappointed to see you empty-handed.
	<br><br>
	Robin tries to cheer everyone up, and points out that there are still many houses to go.
	<br><br>
<</if>>

<<link [[Next|Robin Trick 4]]>><<pass 30>><</link>>
<br>