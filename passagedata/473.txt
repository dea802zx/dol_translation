<<set $outside to 1>><<set $location to "forest">><<effects>>

<<if $rng gte 51>>
You creep forward. The deer remains still. You step within arm's reach, and hold out your hand. It sniffs you, then goes back to grazing.
<<ltrauma>><<lstress>><<trauma -6>><<stress -6>>
<br><br>

<<link [[Next|Forest]]>><</link>>
<br>
<<else>>
You creep forward. Unfortunately, the deer turns and runs in the other direction. It disappears into the trees.
<br><br>

<<link [[Next|Forest]]>><</link>>
<br>

<</if>>