<<effects>>

You wait a moment, looking carefully from side to side, waiting for a gap in the traffic.
<br><br>
<<skulduggerycheck>>
<<if $skulduggerysuccess is 1>>
	<<if $rng gte 51>>
		<span class="green">Confident</span> there's no one around, you run out from behind the tree and into the street. Reaching the top of the staircase can't have taken more than a dozen seconds, but it feels much longer.
		<br><br>

		You crouch once at the top, hidden from the street by the short barrier on either side. Your heart thunders in your chest.<<if $ex_flyover is undefined>><<exhibitionism5>><<set $ex_flyover to 1>><<else>><br><br><</if>>

			<<skulduggeryuse>>

		<<link [[Next|Flyover Ex Top]]>><</link>>
		<br>

	<<else>>
		You're about to move, <span class="green">when you notice a plume of smoke waft between you and the stairs.</span> You wait an extra moment, and a <<generatey1>><<person1>><<person>> walks from behind the tree, stomping out <<his>> cigarette as <<he>> does.
		<br><br>

		Heart thudding, you wait for the <<person>> to disappear around a corner before making your move. You run out from behind the tree and into the street. Reaching the top of the staircase can't have taken more than a dozen seconds, but it feels much longer.
		<br><br>

		You crouch once at the top, hidden from the street by the short barrier on either side. Your heart thunders in your chest.<<if $ex_flyover is undefined>><<exhibitionism5>><<set $ex_flyover to 1>><<else>><br><br><</if>>

			<<skulduggeryuse>>

		<<link [[Next|Flyover Ex Top]]>><<endevent>><</link>>
		<br>
	<</if>>
<<else>>

	<<if $rng gte 51>>
		You're not confident, but you take a deep breath, then run out from behind the tree and into the street. Reaching the top of the staircase can't have taken more than a dozen seconds, but it feels much longer.
		<br><br>

		You crouch once at the top, hidden from the street by the short barrier on either side. Your heart thunders in your chest.<<if $ex_flyover is undefined>><<exhibitionism5>><<set $ex_flyover to 1>><<else>><br><br><</if>>

			<<skulduggeryuse>>

		<<link [[Next|Flyover Ex Top]]>><</link>>
		<br>

	<<else>>
		<<generatey1>><<person1>>
		You take a deep breath, then run out from behind the tree and into the street. <<if $ex_flyover is undefined>><<exhibitionism5>><<set $ex_flyover to 1>><<else>><br><br><</if>>

		You're halfway up the staircase <span class="red">when you hear a voice behind you.</span> "Nice view," <<he>> says. <<covered>> You look over your shoulder, and see a <<person>> leaning against the same tree you hid behind. <<He>> stomps on a cigarette as <<he>> walks closer, looking up at you.
		<<gstress>><<stress 6>><<garousal>><<arousal 6>>
		<br><br>

		<<skulduggeryuse>>

		<<link [[Next|Flyover Ex Naked Caught]]>><</link>>
		<br>

	<</if>>

<</if>>