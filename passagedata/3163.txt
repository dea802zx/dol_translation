<<effects>>

The officer scribbles in <<his>> notepad as <<he>> listens. You're uncomfortable talking about it, but it does give you a catharsis.
<br><br>
Once finished <<he>> confirms that you're unhurt, then continues on <<his>> way.
<<lpain>><<pain -6>>
<br><br>
<<endevent>>
<<destinationeventend>>