<<effects>>

You let <<him>> continue. <<He>> adds <<his>> other hand and starts massaging your scalp. Your body reacts of its own accord, and you shiver with pleasure. <<He>> grips your hair and pulls your face close to <<his>> own, "Don't forget who owns you." <<He>> licks your cheek before shoving you to the ground.
<br><br>

<<tearful>> you climb to your feet. <<He>> leaves with <<his>> friends.
<br><br>

<<endevent>>

<<link [[Next|Hallways]]>><<set $eventskip to 1>><</link>>
<br>