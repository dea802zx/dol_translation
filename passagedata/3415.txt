<<effects>>


<<if $rng gte 91>>
	You smile and greet the <<person>> as you walk by. You realise too late that <<he>> wasn't talking to you, but to a <<generate2>><<person2>><<person>> behind you.
	<br><br>
	
	You hasten your steps.<<gstress>><<gtrauma>><<stress 6>><<trauma 6>>
	<br><br>
<<else>>
	You smile and greet the <<person>> as you walk by.
	<br><br>
<</if>>
<<endevent>>
<<destinationeventend>>