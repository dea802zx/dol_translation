<<set $location to "wolf_cave">><<effects>>

<<if $phase is 0>>
	You rush over to the young wolf and stroke its head. It rears up and licks your face. It's fine, just upset by the fall.
	<<gharmony>><<lferocity>><<ltrauma>><<lstress>><<trauma -6>><<stress -12>>
	<br><br>

	<<destinationwolfcave>>
<<else>>
	You ignore the young wolf. It soon starts playing again. It's fine.
	<<lharmony>><<gferocity>>
	<br><br>

	<<destinationwolfcave>>
<</if>>