<<effects>>

You remain still, and look away from Alex. <<He>> frowns and pulls <<his>> hand away. "Sorry," <<he>> says. "I misread the situation." <<He>> climbs to <<his>> feet and helps you up before returning to work.
<br><br>

<<link [[Next|Farm Work]]>><<endevent>><</link>>
<br>