<<set $outside to 0>><<effects>>
You slide down the chute to the park.
<br>
<<if $worn.upper.name is "naked" and $worn.lower.name is "naked" and $worn.under_lower.name is "naked">>
<<else>>
	Your clothing snags on a rut.
	<br><br>
	<<set $worn.upper.integrity -= 20>>
	<<set $worn.lower.integrity -= 20>>
	<<set $worn.under_lower.integrity -= 20>>
	<br><br>
<</if>>
<<set $danger to random(1, 10000)>><<set $dangerchute to 0>>
<<if $danger gte (9900 - ($allure * 3))>>
	<<set $dangerchute to random(1)>>
<</if>>
<<if $dangerchute is 1>>
	As the chute exit comes into view you see someone has left a cement mixer in front of it!
	<br><br>
	<<link [[Dodge it at any cost!|Molestation Chute]]>><<set $molestationstart to 1>><</link>>
	<br>
	<<link [[Brace for impact|Cement Mixer]]>><</link>>
	<br><br>
<<else>>
	<<parkquick>>
<</if>>
<<set $dangerchute to 0>>