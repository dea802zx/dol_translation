<<set $outside to 0>><<set $location to "school">><<effects>>

There's a thud from up above. Then a crash. Several uniformed police march down the stairs "There <<person1>><<he>> is," a <<generate3>><<person3>><<person>> announces. "The cake thief."
<br><br>

Kylar backs away, clutching the axe. "Put the toy down, <<person1>><<if $pronoun is "m">>boy<<else>>girl<</if>>." the <<person3>><<person>> warns. "You're in deep enough shit. I recognise you. You're the one we took in for breaking into chemical plants. There's been similar crimes lately." <<He>> chuckles. "We'll find plenty of dirt once we get leave to snoop around your home."
<br><br>

Kylar drops the axe and weeps.
<br><br>

<<link [[Remain silent|Kylar Basement Police Silent]]>><<set $mouthuse to 0>><<unbind>><</link>>
<br>
<<link [[Save Kylar|Kylar Basement Police Heroics]]>><<set $mouthuse to 0>><<unbind>><<crimeup 2000>><</link>><<crime>>
<br>