<<set $outside to 0>><<set $location to "home">><<effects>>

You demand Robin give you the money,
<<if $submissive gte 850>>
playing all sorts of emotionally manipulative games with <<himstop>>
<<else>>
and you even threaten <<himstop>>
<</if>>

Finally, <<he>> relents and gives you the money. You make £<<print $robinmoney>>. <<He>> looks worried.
<<set $money += ($robinmoney * 100)>><<set $robinmoney to 0>>
<br><br>

<<robinoptions>>