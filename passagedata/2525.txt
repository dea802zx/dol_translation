<<set $outside to 0>><<schooleffects>><<effects>>

<<if $player.gender is "h">>
	<<if $submissive gte 1150>>
		"Th-this is just the way I am," you say. "Please don't hate me."
	<<elseif $submissive lte 850>>
		"I'm just great like this," <<he>> says. "You lot are the freaks."
	<<else>>
		"This is just how I am," you say.
	<</if>>
	<br><br>
	The boys laugh. "What should we do with it lads?" the <<person>> asks.
	<br><br>
<<else>>
	<<if $submissive gte 1150>>
		"I like dressing as a girl," you say. "It makes me feel pretty."
	<<elseif $submissive lte 850>>
		"I dress how I want," you say. "And I like dressing as a girl."
	<<else>>
		"I like dressing as a girl," you say.
	<</if>>
	<br><br>
	The boys laugh. "You really are a pervert," the <<person>> says. "What should we do with <<phim>> I wonder."
	<br><br>
<</if>>
The other boys shout suggestions.
<<if $worn.neck.collared is 1>>
	The <<person>> ignores them, and picks up your leash. "Perfect," <<he>> says. "Time for walkies. Get on your knees like a good dog."
	<br><br>
	<<link [[Get on knees (0:10)|School Boy's Knees]]>><<pass 10>><<status -10>><<trauma 6>><<stress 6>><</link>><<lcool>><<gtrauma>><<gstress>>
	<br>
	<<link [[Refuse|School Boy's Naked Refuse]]>><</link>>
	<br>
<<else>>
	"Everyone get your phones," <<he>> says to the boys around <<himstop>> "Make sure you get lots of evidence." <<He>> looks back at you. "Time for your punishment. Bend over."
	<br><br>
	<<link [[Bend over (0:10)|School Boy's Bend]]>><<pass 10>><<status -10>><<trauma 6>><<stress 6>><</link>><<lcool>><<gtrauma>><<gstress>>
	<br>
	<<link [[Refuse|School Boy's Naked Refuse]]>><</link>>
	<br>
<</if>>