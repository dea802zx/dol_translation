<<set $outside to 0>><<set $location to "town">><<effects>>

You put your arms around Robin and ease <<him>> back into <<his>> seat. "It's only a film," you say. "It can't hurt you."
<br><br>

"I know," <<he>> says. <<He>> doesn't sound convinced.
<br><br>

The film starts. Robin screams and wraps <<his>> arms around you whenever something scary happens. By the climax <<he>> is glued to your side, <<his>> head buried in your shoulder. <<He>> doesn't seem relieved even when the film is over.
<br><br>

<<He>> stays close beside you on the way home, asking if you think the monster is real. <<He>> almost sounds excited by the time you get back to <<his>> room.
<br><br>

<<link [[Next|Robin Options]]>><</link>>
<br>