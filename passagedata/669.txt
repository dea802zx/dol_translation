<<set $outside to 0>><<set $location to "wolf_cave">><<effects>>

<<if $enemywounded gte 2 and $enemyejaculated is 0>>
In a display of power, you defeat the pack. <<tearful>> you escape from the cave.
<br><br>

<<clotheson>>
<<endcombat>>

<<link [[Next|Forest]]>><<set $stress -= 1000>><<set $eventskip to 1>><<set $eventskip to 1>><</link>>

<<elseif $enemywounded is 0 and $enemyejaculated gte 2>>
Finished with you, the wolves leave you be. However, they rest between you and the cave entrance, and could easily block your escape. <<tearful>> you settle down.
<br><br>

<<if $phase is 1>>
The other wolves soon return from their hunt.
<</if>>

<<clotheson>>
<<endcombat>>

<<link [[Next|Forest Wolf Cave]]>><<set $eventskip to 1>><</link>>

<<elseif $enemywounded gte 1 and $enemyejaculated gte 1>>
Finished with you, the wolves leave you be. However, they rest between you and the cave entrance, and could easily block your escape. <<tearful>> you settle down.
<br><br>

<<if $phase is 1>>
The other wolves soon return from their hunt.
<</if>>

<<clotheson>>
<<endcombat>>

<<link [[Next|Forest Wolf Cave]]>><<set $eventskip to 1>><</link>>
<</if>>