<<effects>>

You step forward.

<<if $submissive gte 1150>>
"S-stop!" you stammer. "Leave <<person2>><<him>> alone!"
<<elseif $submissive lte 850>>
"You bully," you say. "Someone should teach you a lesson."
<<else>>
"Stop," you say. "This is wrong."
<</if>>
<br><br>

The <<person2>><<person>> gapes at you. Bailey laughs.<<person1>> "You want to pay the <span class="gold">£200</span> instead?" <<he>> asks. "It's all the same to me."
<br><br>

<<if $money gte 20000>>
<<link [[Pay|Home Intervene Pay]]>><<hope 1>><<set $money -= 20000>><</link>><<ghope>><<ltrauma>><<trauma -6>>
<br>
<</if>>
<<link [[Fight|Home Intervene Fight]]>><<set $fightstart to 1>><</link>>
<br>
<<link [[Step away|Home Intervene Step]]>><</link>>