<<effects>>

<<if $submissive gte 1150>>
	"P-please help me," you say, bowing your head.
<<elseif $submissive lte 850>>
	"Don't just stand there," you say. "Get me something to wear."
<<else>>
	"Could you help me please?" you say.
<</if>>
<br><br>

<<if random(1, 2) is 2>>
	The <<person>> drops back beneath the fence. A few moments pass, <span class="green">then a towel flies over.</span>
	<<lstress>><<towelup>>
	<br><br>
<<else>>
	The <<person>> drops back beneath the fence. A few moments pass, then <<he>> reappears. <span class="red">A <<person1>><<person>> appears next to <<himcomma>> along with several others.</span>
	<<fameexhibitionism 9>>
	<br><br>
	They burst into excitement as you turn and run.
	<<gstress>><<stress 6>>
	<br><br>
	"I told you there was a naked <<girl>>."
	<br>
	<<if random(1, 2) is 2>>
		"Hot."
	<<else>>
		"I can see everything."
	<</if>>
	<br>
	<<if random(1, 2) is 2>>
		"Is <<pshe>> a pervert?"
	<<else>>
		"What is <<pshe>> thinking?"
	<</if>>
	<br>
	<<if random(1, 2) is 2>>
		"I'm gonna tell everyone."
	<<else>>
		"Wait until the others hear."
	<</if>>
	<br>
	<<if $worn.face.type.includes("mask")>>
		"I wonder who <<pshe>> is."
	<<else>>
		"I think I recognise <<phim>> from school."
	<</if>>
	<br><br>

	Their voices fade behind you.
	<br><br>
<</if>>

<<endevent>>
<<destinationeventend>>