<<set $outside to 0>><<set $location to "underground">><<effects>>

You are led to the stage where four <<victimgirls>> stand in a line, eyes downcast. Each wears a collar similar to your own. As soon as you take your place among them, a pair of large wooden doors opens and a <<generate1>><<person1>><<person>> strides into the room. <<He>> is flanked by a <<generate2>><<person2>><<person>> who struggles to keep up. "We have a nice selection for you today <<person1>><<sirstop>> Just the sort of thing you like." Says the <<person2>><<personstop>>
<br><br>

"I'll see for myself," the <<person1>><<person>> responds. <<He>> stops in front of the stage and eyes the <<victimgirls>> up, including you. <<covered>> "This one's all skin and bone," <<he>> says, pointing at the <<victimgirl>> on the far side from you. "<<pShe>> looks likely to faint at any moment."
<br><br>

"Our <<victimgirls>> are all fed a-"
<br><br>

<<if $attractiveness gte 3000>>

"Spare me," <<he>> turns to you and smiles. "This one will do nicely. Bring <<phim>> in."
<br><br>

<<link [[Next|Underground Presentation2]]>><</link>>
<br>

<<else>>

"Spare me," <<he>> turns to you, but looks away dismissively. "I've made my choice." <<He>> lunges for the <<person2>><<person>> and drags <<him>> to the ground.
<br><br>

"I-I'm not... Stop!"

The protestations are no use. You hear a loud smack and the <<person>> yelp in pain as you are led back to your cell.
<br><br>
<<endevent>>
<<link [[Next|Underground Cell]]>><</link>>
<br>

<</if>>