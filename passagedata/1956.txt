<<set $outside to 1>><<set $location to "town">><<effects>><<set $bus to "domus">><<pass 5>>
<<person1>>
<<if $phase is 0>>
	You head back to the house empty-handed and knock on the door.
	<br><br>
	The <<person>> looks at you blankly. "Where is it then?"
	<br>
	"They wouldn't serve me."
	<br>
	"Shit," <<he>> says.
	<<if $weather is "rain">>He takes in your wet clothes and bedraggled hair. "Well, you clearly tried."
	<br>
	With a look of pity, <<he>> hands you £5.
	<<set $money += 500>>
	<<elseif $rng % 2>>"I guess you tried."
	<br>
	<<He>> grudgingly hands you £5.
	<<set $money += 500>>
	<<else>>"So why are you here? You think I'm paying you to go on a walk?! Get lost!"
	<</if>>
<<elseif $phase is 1>>
	You carry the crate back to the house. It's heavier than it looks
	<<if $weather is "clear">>and with the sun beating down you work up quite a sweat.
	<<elseif $weather is "rain">>and the rain beating down on you really isn't helping.
	<<elseif $weather is "overcast">>but you manage.
	<</if>>
	<br><br>
	<<person1>>Arriving back the <<person>> hands you £5 and takes the crate.
	<<set $money += 500>>
	<<if $rng % 3 is 0>>
		At that moment, a loud roar comes from inside the house, followed by <<if $rng % 2>>loud cheers and clapping.<<else>>jeering, boos and some very crude cursing.<</if>>
		<br>
		"Fuck fuck fuck," <<he>> says, slamming the door and rushing back inside.
	<<else>>
	<br>
	"Thanks. Lifesaver," <<he>> says, closing the door.
	<</if>>
<<else>>You carry Schroedinger's beer back to the house. (This is an error, please report)
<</if>>
<br><br>
<<link [[Next|Domus Street]]>><<endevent>><</link>>
<br>