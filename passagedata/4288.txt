<<effects>>

<<flaunting>> you trot down the hallway on all fours with Whitney and <<his>> friends guiding you. You block their path once or twice, sitting down in front of them in a begging position<<if $leftarm is "bound" and $rightarm is "bound">> to the best of your ability<</if>>, barking and panting like an animal until they pat your head or scratch your chin and you purr.
<<exhibitionism5>>

"You can't decide if you're a dog or a cat," Whitney remarks. "Lets see you play fetch."

You're steered round a corner and into a corridor full of <<peopley>>. <<covered>> A nearby <<persony>><<person1>> looks at you and gasps in shock. Others have a similar reaction. As the noise grows more turn to look, until everyone is watching you.
<br><br>

Some look away, going red in the face. Most though are keen to take in the sight of the <<if $worn.under_lower.type.includes("naked")>>naked<<else>>nearly naked<</if>> <<girl>> crawling on the floor like an animal, barking and panting.
<br><br>

Whitney gathers your clothes and gestures for everyone to stand back. "Ready <<girl>>?" <<if $leftarm is "bound" and $rightarm is "bound">>You nod.<<else>>You give <<him>> a paw, eager to play.<</if>> <<He>> tosses the clothes down the hall, and you scramble after them. <<if $leftarm is "bound" and $rightarm is "bound">>You pick them up with your mouth<<else>>You gather them<</if>> and find somewhere secluded to dress. Your heart thuds.
<br><br>

<<fameexhibitionism 70>>
<<link [[Next|Hallways]]>><<clotheson>><<endevent>><<set $eventskip to 1>><</link>>
<br>