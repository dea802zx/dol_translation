<<effects>>

<<He>> leads you away from the sty, through an old wooden gate and onto a paved road. You see a stable up ahead.

<<if $monsterchance gte random(1, 100) and ($hallucinations gte 1 or $monsterhallucinations is "f")>>
	<<if $malechance lte $rng>>
		<<set _horse_gender to "f">>
	<<else>>
		<<set _horse_gender to "m">>
	<</if>>
	Centaur lean out the stalls, resting on their elbows. 
	<<if _horse_gender is "f">>
		"Hey Alex," one says as you draw close. She's unconcerned about her exposed breasts, but casts a wary eye over you. "When you letting us out?" 
	<<else>>
		"Hey Alex," one says as you draw close. He casts a wary eye over you. "When you letting us out?"
	<</if>>
	<br><br>
	
	"Sorry," Alex replies, scratching the centaur's nose. "I have a visitor." <<He>> opens the stall. "Someone I hope you come to like."
	<br><br>
	
<<else>>

	Horses watch from the stalls. One of them neighs as you draw close.
	<br><br>
	
	"Sorry," Alex says, scratching the horse's nose. "I have a visitor." <<He>> opens the stall. "Someone I hope you come to like."
	<br><br>
	
<</if>>

"The horses need lots of care," <<he>> says as you walk further along the track, towards another gate. "They need brushing every day, and feeding twice a day. They need lots of exercise too." <<He>> opens the gate, and the steeds thunder through to the field beyond.
<br><br>

<<link [[Next|Farm Intro 6]]>><</link>>
<br>