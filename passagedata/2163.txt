<<set $outside to 0>><<set $location to "hospital">><<effects>>

<<set $hunger to 0>><<set $thirst to 0>><<set $tiredness to 0>><<set $hygiene to 0>>
<<set $stress -= 2000>>

<<if $hospitalintro is 0>><<set $hospitalintro to 1>>

You awaken in a bed at the civic hospital.

<<generate1>><<person1>>
The nurse explains that you have suffered a severe stress response, but you are otherwise physically healthy. <<He>> asks you to wait a moment while <<he>> fetches the doctor.
<br><br>
<<endevent>>

<<npc Harper>><<person1>>You don't have to wait long. A <<if $pronoun is "m">>man<<else>>woman<</if>> wearing a white medical coat and carrying a clipboard enters.
<br><br>

<<He>> smiles at you. "Hello, I'm Doctor Harper." <<He>> takes a seat by your bedside and pulls <<his>> blond fringe away from <<his>> eyes. <<He>> looks young, can't be older than 25.
<br><br>

<<else>>
You awaken in a bed at the civic hospital.

<<npc Harper>><<person1>>Doctor Harper enters at that moment, carrying <<his>> usual clipboard. <<He>> assures you that there is no immediate concern, but <<he>> would like to you examine you further.
<br><br>

<</if>>

<<link [[Next|Hospital Bed Examination]]>><</link>>
<br><br>