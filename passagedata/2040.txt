<<effects>>


<<if $physique gte random(1, $physiquemax)>>

	<span class="green">You tear yourself free of the shoots binding you,</span> and climb to your feet. The plants fall to the ground, inert, freeing your body.
	<br><br>

	<<link [[Next|Garden Flowers]]>><</link>>
	<br>

<<else>>

	You try to tear yourself free of the plants binding you, <span class="red">but they're too tight.</span>
	<br><br>
	
	<<link [[Next|Garden Stem Tentacles]]>><<set $molestationstart to 1>><</link>>
	<br>
	
<</if>>