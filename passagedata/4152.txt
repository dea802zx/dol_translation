<<set $outside to 0>><<set $location to "town">><<effects>>

"I wanted to see this," Robin says as you buy the tickets. <<He>> bobs on <<his>> heels. "Can we get popcorn?"
<br><br>

<<He>> runs ahead of you into the theatre. You find <<him>> waiting by the attendant looking sheepish. You had the tickets. Once through <<he>> runs ahead again, finding two free seats near the middle of the room. <<He>> waves at you.
<br><br>

The film soon starts. It's pretty good. Robin enjoys it, and laughs along with the rest of the audience.
<br><br>

<<if $robinromance is 1>>
<<link [[Fondle|Robin Cinema Fondle]]>><<npcincr Robin lust 1>><</link>><<promiscuous1>><<glust>>
<br>
<</if>>
<<link [[Keep watching|Robin Cinema Watch]]>><<pass 15>><</link>>
<br>