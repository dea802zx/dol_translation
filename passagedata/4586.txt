<<effects>>
You crouch behind a box next to a small supermarket and peek out from an alley, looking down the High Street. You can see the green of the park up ahead.
<<if $police_high is 1>>
	You duck as a pair of police officers walk by. The street is full of them, concerned about another brawl. <span class="purple">You won't be able to cross while they're on such high alert.</span>
	<br><br>
<<else>>
	You duck as a couple walk by, shunting the box with a rattle. The street is busy. You're not getting over without being seen.
	<br><br>
	The box opened as you knocked it. Inside are numerous identical tins. Whipped cream. <span class="lewd">A lewd idea strikes you.</span>
	<br><br>
	<<link [[Cover yourself in whipped cream and walk to the park|Cream Ex Undies Walk]]>><<pass 5>><</link>>
	<br>
<</if>>
<<link [[Leave|Commercial alleyways]]>><</link>>
<br>