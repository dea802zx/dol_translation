<<set $outside to 0>><<effects>><<set $lock to 200>>
A standard cash register sits on a counter. It's locked tight.
<br><br>
<<if $skulduggery gte $lock>>
	<span class="green">The lock looks easy to pick.</span>
	<br><br>
	<<link [[Pick it (0:10)|Clothing Shop Register Sneak]]>><<pass 10>><<crimeup 1>><</link>><<crime>>
	<br>
<<else>>
	<span class="red">The lock looks beyond your ability to pick.</span><<skulduggeryrequired>>
	<br><br>
<</if>>
<<link [[Leave|Clothing Shop]]>><</link>>
<br>