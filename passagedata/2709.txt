<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<set $scienceattended to 1>>

<<if $schoolevent is 0 and $schooleventtimer lte 0>><<set $schoolevent += 1>><<set $schooleventtimer to 14>>
	<<npc Sirris>><<person1>>"The headteacher has asked to be involved with the lesson today," Sirris says. "I know you'll all be on your best behaviour." The door opens as <<he>> finishes, and Leighton steps in.
	<br><br>

	<<endevent>><<npc Leighton>><<person1>><<He>> strides to the front of the class. "Thank you Sirris," <<he>> says. "Why don't you take a break?"
	<br><br>

	Sirris looks perplexed. "I thought you'd want my help, I've prepared-"
	<br><br>

	"Don't you worry. I've prepared my own plan for the lesson. Now go relax in that cafe you like."
	<br><br>

	<<endevent>><<npc Sirris>><<person1>>

	Sirris opens <<his>> mouth to argue, but thinks better of it. "Be good," <<he>> whispers to the class on <<his>> way out.
	<br><br>
	<<endevent>><<npc Leighton>><<person1>>

	<<link [[Next|Penis Inspection]]>><<pass 5>><</link>>
	<br>
<<elseif $schoolevent is 1 and $schooleventtimer lte 0>><<set $schoolevent += 1>><<set $schooleventtimer to 14>>
	<<npc Sirris>><<person1>>"The headteacher has once again asked to take charge of the lesson," Sirris says.<<endevent>><<npc Leighton>><<person1>> "I don't know why <<hes>> taken an interest, but <<he>> seemed satisfied last time."
	<br><br>

	The door swings open and Leighton steps in, a smile on <<his>> face. "Thank you Sirris," <<he>> says. "I'll take it from here."
	<br><br>

	"Are you sure I can't assist?" Sirris asks.
	<br><br>

	"I'll be fine. You have a very cute class."
	<br><br>

	<<endevent>><<npc Sirris>><<person1>>
	"O-okay." Sirris turns to the class as <<he>> leaves. "Be good," <<he>> whispers.
	<br><br>

	<<endevent>><<npc Leighton>><<person1>>

	<<link [[Next|Pussy Inspection]]>><<pass 5>><</link>>
	<br>
<<elseif $schoolevent is 2 and $schooleventtimer lte 0>><<set $schoolevent += 1>><<set $schooleventtimer to 14>>
	<<npc Sirris>><<person1>>"The headteacher has asked to take over today's lesson," Sirris says. "I hope it's not because I'm doing my job improperly."
	<br><br>

	<<endevent>><<npc Leighton>><<person1>>

	"You're doing fine," Leighton says as <<he>> enters. "I'm just conducting government-mandated health checks."
	<br><br>

	"I can help with that, I have-"
	<br><br>

	Leighton raises <<his>> arm and Sirris fall quiet. "Go take a break. Your class will be in one piece when you return."
	<br><br>

	<<endevent>><<npc Sirris>><<person1>>
	Sirris looks at the class as <<he>> leaves. "I won't be long." <<he>> whispers.
	<br><br>

	<<endevent>><<npc Leighton>><<person1>>

	<<link [[Next|Breast Inspection]]>><<pass 5>><</link>>
	<br>
<<else>>
	<<if $time lte ($hour * 60 + 5)>>
		The science lesson begins, how do you want to conduct yourself?
		<br><br>
	<<else>>
		The science lesson continues, how do you want to conduct yourself?
		<br><br>
	<</if>>

	<<link [[Focus on the lesson|Science Lesson Focus]]>><</link>><<gstress>><<gscience>>
	<br>
	<<link [[Socialise with classmates|Science Lesson Socialise]]>><</link>><<gcool>><<ltrauma>><<lstress>><<gharass>>
	<br>
	<<link [[Daydream|Science Lesson Daydream]]>><</link>><<lstress>> <<lharass>>
	<br>
	<<if $exhibitionism gte 55>>
		<<link [[Masturbate|Science Lesson Masturbate]]>><<set $masturbationstart to 1>><</link>><<exhibitionist4>>
		<br>
	<</if>>
<</if>>