<<effects>><<set $outside to 0>>
Having come this far, you submit to the collar being placed round your neck. You hear a decisive click as it locks shut. <<person2>>The <<person>> attaches the leash, and tugs.
<br><br>
<<if $rng gte 51>>
	The pair stand back to admire your body, the <<person2>><<person>> holding your leash securely. "Very nice," the <<person1>><<person>> says, circling around you for a different angle. With your arms bound, there's nothing you can do to shield yourself from their scrutiny.
	<br><br>
	The <<person2>><<person>> approaches you, and you brace yourself for a more physical probing. Instead, <<he>> unties your bonds while the <<person1>><<person>> throws the fabric over your head. "Here you go. You can keep the collar." Laughing, they head in the direction of the beach. You try to cover up as quickly as possible, but you're still shaking from the ordeal.
	<br><br>
	<<unbind>>
	<<clothesontowel>>
	<<endevent>>
	<<set $stress -= 500>>
	<<link [[Next|Beach]]>><</link>>
<<else>>
	<<set $rng to random(1, 100)>>
	<<person1>>Smirking, the <<person>> speaks, "You're all dressed up now, but it would be a waste for no one else to see you looking so fine." <<person2>>The <<person>> tugs again, harder this time. "We'll help you as we said we would, but there's someone you need to meet first."
	<br><br>
	Bound and leashed as you are, you have little choice but to go with them<<if $submissive lte 850>>, though you seethe noiselessly at the humiliation<<else>> meekly<</if>>.
	<br><br>
	You are led deeper into the dunes. You walk for several minutes, conscious of just how exposed and vulnerable you are. Your captors make no attempt to hide how much they enjoy being in a position of power over you, and constantly leer at your body, knowing there's nothing you can do to stop them.
	<br><br>
	<<if $bestialitydisable is "f" and $rng gte 51>>
		You come to a relatively flat area, surrounded by dunes on all sides, shielding it from view. In the centre is a dog, its leash tied to a wooden post. At the sight of you it leaps to it's feet, straining the leash in a bid to reach you.
		<br><br>
		<<person1>> The <<person>> speaks in a high-pitched voice, "Who's a good dog! You are! We brought you a bitch, because you're such a good dog!"
		<br><br>
		<<link [[Try to run|Beach Abduction Dog]]>><<set $molestationstart to 1>><<set $phase to 0>><</link>>
		<br>
		<<link [[Allow yourself to be led over|Beach Abduction Dog]]>><<set $molestationstart to 1>><</link>>
		<br>
	<<else>>
		<<generatey3>><<generatey4>><<generatey5>><<generatey6>>You come to a relatively flat area, surrounded by dunes on all sides, shielding it from view. Four teenagers sit smoking in the centre. Your feelings of humiliation reach a new pitch as the new pairs of eyes see you in your shameful situation. Their initial shock quickly subsides, leaving a bare and primal lust.
		<br><br>
		The <<person2>><<person>> leads you into the middle of the group, their eyes feasting on every inch of your body. "We found this piece of trash near the beach. Can't believe anyone would just leave it laying around, no respect at all," the <<person1>><<person>> <<if $worn.genitals.type.includes("chastity")>>starts fondling<<else>>attempts to break<<set $worn.genitals.integrity -= 50>><</if>> your <<genitals>>in front of everyone, causing your breath to catch in your chest. The rest of the group take the cue. Arms reach out from all around you, each wanting their share.
		<br><br>
		<<link [[Next|Beach Abduction Molestation]]>><<set $molestationstart to 1>><</link>>
	<</if>>
<</if>>