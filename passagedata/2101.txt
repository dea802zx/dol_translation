<<set $location to "town">><<set $outside to 0>><<effects>>
<<generate1>><<generate2>><<person1>><<facewear 4>><<bind>>
You're shaken awake, and find yourself lying in the back of a car. Coils of rope hold your arms to your back, and your body to the chairs. A gag has been stuffed in your mouth.
<br><br>
"<<pShes>> awake," says a <<person>> in the front passenger seat.
<br>
"Good," responds the driver, a <<person2>><<personstop>> "Just in time."
<br><br>
You can't see much beyond the windows. Not from your position. Only the sky and a procession of street lights. The car pulls into a building, and you hear doors shut behind it.
<br><br>
<<link [[Next|Sold 2]]>><</link>>
<br>