<<effects>>
<<if $enemyarousal gte $enemyarousalmax>>
<<ejaculation>>

Their lust spent, they realise the weight of what they've done, and flee the scene lest they be caught. <<tearful>> you head towards the classroom.
<br><br>

<<clotheson>>
<<endcombat>>

	<<if !$worn.upper.type.includes("naked") and !$worn.lower.type.includes("naked")>>
	<<else>>
You make a detour to the toilets and find some towels to cover up with before anyone sees you.<<towelup>>
	<</if>>

You arrive back at the classroom and deliver your cargo, safe and sound. The rest of the class is busy dissecting frogs. Lacking a partner, you get one all to yourself.
<<gscience>><<scienceskill>>
<br><br>

<<link [[Next|Science Lesson]]>><</link>>

<<elseif $enemyhealth lte 0>>

Your assailants back off. "You'll get yours, <<bitchstop>>" They leave you alone, trying to conceal their injuries. <<tearful>> you head towards the classroom.
<br><br>

<<endcombat>>

	<<if !$worn.upper.type.includes("naked") and !$worn.lower.type.includes("naked")>>
	<<else>>
You make a detour to the toilets and find some towels to cover up with before anyone sees you.<<towelup>>
	<</if>>

You arrive back at the classroom and deliver your cargo, safe and sound. The rest of the class is busy dissecting frogs. Lacking a partner, you get one all to yourself.
<<gscience>><<scienceskill>>
<br><br>

<<link [[Next|Science Lesson]]>><<endevent>><</link>>

<<elseif $rescue is 1 and $alarm is 1>><<set $rescued += 1>>

<<endevent>><<npc Leighton>><<person1>>Leighton rounds a corner, having heard your scream. The assailants' demeanour changes immediately, and they are harried back to <<his>> office. <<tearful>> you head back to the classroom.
<br><br>

<<clotheson>>
<<endcombat>>

	<<if !$worn.upper.type.includes("naked") and !$worn.lower.type.includes("naked")>>
	<<else>>
You make a detour to the toilets and find some towels to cover up with before anyone sees you.<<towelup>>
	<</if>>

You arrive back at the classroom and deliver your cargo, safe and sound. The rest of the class is busy dissecting frogs. Lacking a partner, you get one all to yourself.
<<gscience>><<scienceskill>>
<br><br>

<<link [[Next|Science Lesson]]>><</link>>

<<else>>

With the classroom in sight, your assailants take their leave. You hear their laughter long after they pass from view. <<tearful>> you gather yourself.
<br><br>

<<clotheson>>
<<endcombat>>

	<<if !$worn.upper.type.includes("naked") and !$worn.lower.type.includes("naked")>>
	<<else>>
You make a detour to the toilets and find some towels to cover up with before anyone sees you.<<towelup>>
	<</if>>

You arrive back at the classroom and deliver your cargo, safe and sound. The rest of the class is busy dissecting frogs. Lacking a partner, you get one all to yourself.
<<gscience>><<scienceskill>>
<br><br>

<<link [[Next|Science Lesson]]>><</link>>
<</if>>