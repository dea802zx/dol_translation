<<widget "effectsdissociation">><<nobr>>

<<if $leftaction == "leftpoke" && $rightaction == "rightpoke">>
	<<set $leftaction to 0>><<set $rightaction to 0>><<set $leftactiondefault to "leftpoke">><<set $rightactiondefault to "rightpoke">>
	You poke yourself. Is this real? <span class="green"> + Focus</span>
	<<set $traumafocus += 2>>
	<<set _br to true>>
<</if>>

<<if $leftaction == "leftpoke">>
	<<set $leftaction to 0>><<set $leftactiondefault to "leftpoke">>
	You poke yourself with your left hand. <span class="green"> + Focus</span>
	<<set $traumafocus += 1>>
	<<set _br to true>>
<</if>>

<<if $rightaction == "rightpoke">>
	<<set $rightaction to 0>><<set $rightactiondefault to "rightpoke">>
	You poke yourself with your right hand. <span class="green"> + Focus</span>
	<<set $traumafocus += 1>>
	<<set _br to true>>
<</if>>

<<if $leftaction == "leftcurl" && $rightaction == "rightcurl">>
	<<set $leftaction to 0>><<set $rightaction to 0>><<set $leftactiondefault to "leftcurl">><<set $rightactiondefault to "rightcurl">>
	You hold your arms to the side and curl your fingers.
	<<meek 2>>
	<<set _br to true>>
<</if>>

<<if $leftaction == "leftcurl">>
	<<set $leftaction to 0>><<set $leftactiondefault to "leftcurl">>
	You hold your left arm to the side and curl your fingers.
	<<meek 1>>
	<<set _br to true>>
<</if>>

<<if $rightaction == "rightcurl">>
	<<set $rightaction to 0>><<set $rightactiondefault to "rightcurl">>
	You hold your right arm to the side and curl your fingers.
	<<meek 1>>
	<<set _br to true>>
<</if>>

<<if _br is true>>
	<br>
<</if>>

<<if $mouthaction == "speak">>
	<<set $mouthaction to 0>><<set $mouthactiondefault to "speak">>
	You make some noises that resemble words. <span class="green"> + Focus</span>
<</if>>

<<if $mouthaction == "noises">>
	<<set $mouthaction to 0>><<set $mouthactiondefault to "noises">>
	You make some soft noises. They sound nice.
<<meek 1>>
<</if>>

<</nobr>><</widget>>