<<set $outside to 0>><<set $location to "cafe">><<effects>>

<<if $submissive gte 1150>>
"Thank you," you say. "Avery can be scary."
<<elseif $submissive lte 850>>
"Thanks," you say. "You've made my evening slightly less unpleasant."
<<else>>
"Thank you," you say. "I needed some space."
<</if>>
<br><br>

Bailey leans in. "When you deliver your speech," <<he>> whispers. "Don't say anything you'll regret."
<br><br>

<<He>> pulls away as, for the first time since you arrived, attention is diverted elsewhere.
<br><br>

<<endevent>>
<<link [[Look|Chef Opening 7]]>><</link>>
<br>