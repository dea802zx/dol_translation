<<effects>>

You keep your pace consistent, but don't slow down. You march right up to the large horse, and rest your hand on its head.
<br><br>

The horse makes a gruff sound, but goes back to eating. The others follow suit.
<br><br>

<<link [[Next|Livestock Field Horse]]>><</link>>
<br>