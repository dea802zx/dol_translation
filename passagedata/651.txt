<<set $outside to 1>><<set $location to "forest">><<effects>>

<<if $phase is 0>><<set $wolfpackshroom to "none">>
You crumble the red mushroom onto the black wolf's food. It spots you and growls, but relents when it sees you carry nothing away. It returns to its place and continues eating. Over the next few hours it becomes more territorial, glaring at other wolves who approach the deer.
<<gferocity>>
<br><br>

<<elseif $phase is 1>><<set $wolfpackshroom to "none">>
You crumble the pink mushroom onto the black wolf's food. It spots you and growls, but relents when it sees you carry nothing away. It returns to its place and continues eating. Over the next few hours it becomes lethargic. The other wolves notice and try their luck at parts of the deer they wouldn't normally be allowed.
<<lferocity>><<set $wolfpackpoisoned to 1>>
<br><br>

<</if>>

You lie on the grass with your head resting on a wolf. All that running is tiring. You close your eyes and sleep. You're awoken when the wolf you were resting on stands up.
<br><br>

<<set $huntstate to "success">>
<<wolfhuntevents>>