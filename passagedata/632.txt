<<set $location to "wolf_cave">><<effects>>

<<if $enemyarousal gte $enemyarousalmax>>
	<<beastejaculation>><<wolfpacktrust>>
	<br>

	The black wolf calms down. The other wolves look grateful and relieved.
	<<gharmony>>
	<br><br>

	<<tearful>> you climb to your feet.
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<destinationwolfcave>>
<<elseif $enemyhealth lte 0>>
	The whole pack watches as the black wolf rolls onto its back in submission.
	<br><br>

	<<tearful>> you howl. The other wolves howl with you. The black wolf hobbles away to nurse its injuries. <span class="gold">You can now lead the pack on hunts.</span>
	<<set $wolfpackleader to 1>><<earnFeat "Head of the Pack">>
	<br><br>

	<<clotheson>>
	<<endcombat>>

	<<destinationwolfcave>>
<<else>>
	You fall to the ground, unable to fight any longer. The black wolf grabs your neck in its jaws and holds you to the floor for a moment, then releases you.
	<<gtrauma>><<gstress>><<trauma 6>><<stress 6>>
	<br><br>

		<<if $wolfpackpoisoned isnot 1>>
	<i>The black wolf is very strong. There must be a way to even things.</i>
	<br><br>
		<</if>>

	<<clotheson>>
	<<endcombat>>

	<<destinationwolfcave>>
<</if>>