<<set $outside to 0>><<set $location to "docks">><<dockeffects>><<effects>>

<<if $submissive gte 1150>>
"I'm just trying to work," you say. "Please let me."
<<elseif $submissive lte 850>>
"Fuck you," you say. "You're supposed to be working."
<<else>>
"This is cruel," you say. "I'll report you."
<</if>>
<br><br>

"We're just having fun," the <<person>> says. "Your kind rarely sticks with it anyway." The other dockers nod.
<br><br>

<<dockoptions>>