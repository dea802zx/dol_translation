<<effects>>

The <<person>> stomps out <<his>> cigarette and starts to walk. "You live at the orphanage, right?"
<br><br>

<<if $rng gte 51 and $per_npc.bartend>>
	<<loadNPC 1 bartend>>
	You're at the end of Connudatus Street when a figure looms out of the dark, blocking your path. It's the <<person2>><<person>> who harassed you earlier.
	<br><br>
	The <<person1>><<person>> steps between you and folds <<his>> arms. The <<person2>><<person>> stops in <<his>> tracks. <<He>> glares at the <<person1>><<personcomma>> but thinks better of starting a fight. <<person2>><<He>> slinks back into the dark.
	<br><br>
	"Probably the last we'll see of <<himcomma>>" the <<person1>><<person>> says. "But I'll stick around to be safe."
	<br><br>
	The rest of the journey is uneventful. The <<person1>><<person>> walks fast, but doesn't know the exact location of the orphanage so you end up taking the lead. <<He>> walks you up to the orphanage proper before bidding farewell.
	<br><br>

<<else>>
	The journey is uneventful. <<He>> walks fast, but doesn't know the exact location of the orphanage so you end up taking the lead. <<He>> walks you up to the orphanage proper before bidding you farewell.
	<br><br>
<</if>>

<<endevent>>
<<if $per_npc.bartend>>
<<clearNPC bartend>>
<</if>>

<<link [[Next|Orphanage]]>><</link>>
<br>