<<set $location to "forest">><<effects>>
<<pass 60>>

The horse carries you over fields and hills, until coming to a stop at a crest dotted with trees. You see the forest and town beneath you.
<br><br>
The horse will go no further. You climb from its back. It rubs its head against you, and you pet it back.
<<lltrauma>><<lllstress>><<trauma -24>><<stress -36>>
<br><br>

<<if $worn.face.type.includes("gag")>>
	It licks your neck, then you feel a tug. It <span class="lblue"> unties your $worn.face.name.</span>
	<<set $worn.face.type.push("broken")>>
	<<faceruined>>
	<br><br>
<</if>>

Satisfied, it turns back toward the farm. It dashes forward, disappearing behind the hill.
<br><br>

<<link [[Next|Livestock Escape Town]]>><<endevent>><</link>>
<br>