<<set $outside to 0>><<set $location to "underground">><<effects>>
You are in an underground cell. You can't see a thing but can tell that the walls and floor are made of cold stone.
<<if $undergroundwater is 0>>
	There's a tap in the corner which barely dribbles water.
<<else>>
	<<set $undergroundwater += 1>>
	There's a tap in the corner, which they turned off to punish your disobedience.
	<<if $undergroundwater gte 25>>
		<span class="red">You're dying of thirst!</span>
		<<set $pain += 60>><<stress 6>><<trauma 6>><<gtrauma>><<gstress>>
	<<elseif $undergroundwater gte 20>>
		<span class="pink">You really need water.</span>
		<<set $pain += 30>>
	<<elseif $undergroundwater gte 15>>
		<span class="purple">You're very thirsty.</span>
		<<set $pain += 20>>
	<<elseif $undergroundwater gte 10>>
		<span class="blue">You're thirsty.</span>
		<<set $pain += 10>>
	<<elseif $undergroundwater gte 5>>
		<span class="lblue">You feel a bit thirsty.</span>
	<</if>>
	<br><br>
<</if>>
<br><br>
<<set $undergroundtime += 1>>
<<if $undergroundtime is 2 and $skin.pubic.special isnot "slave" and $skin.pubic.pen isnot "brand" and $skin.pubic.pen isnot "magic" and $bodywritingdisable is "f">>
	Dim pink-hued lights turn on, bathing the room in light. Before your eyes can adjust, one of the doors opens and a number of people enter. You think there are four of them.<<generate1>><<generate2>><<generate3>><<generate4>><<person1>> They grab your arms and legs, and force you onto your back.
	<br><br>

	<<if $worn.lower.exposed is 0>>
	Someone gropes your $worn.lower.name, pulling <<if $worn.lower.plural is 1>>them<<else>>it aside<</if>>. Another, a <<personcomma>> sets up a machine beside the mattress. <<He>> examines what looks like a thick needle.
		<br><br>
	<<else>>
		A <<person>> sets up a machine beside the mattress. <<He>> examines what looks like a needle.
		<<gtrauma>><<gstress>>
		<br><br>
	<</if>>

		<<link [[Struggle|Underground Tattoo Struggle]]>><</link>>
		<br>
		<<link [[Lie still|Underground Tattoo Still]]>><</link>>
		<br>

<<elseif $undergroundtime is 4>>
	The lights turn on once more, and a <<generate1>><<person1>><<person>> enters. "Time for your performance," <<he>> says, yanking your leash. "Make sure you dance sexy. If you don't attract a customer we'll just let the whole audience have a piece." <<He>> tries to pull you from the room.
	<br><br>
	<<link [[Go quietly|Underground Dance Intro]]>><<endevent>><</link>>
	<br>
	<<link [[Resist|Underground Cell Resist]]>><<set $molestationstart to 1>><<set $phase to 1>><</link>>
	<br>
<<elseif $undergroundtime is 8>>
	The lights turn on once more, and a <<generate1>><<person1>><<person>> enters. "You're needed," <<he>> says, yanking your leash.
	<br><br>
	<<link [[Go quietly|Underground Presentation]]>><<endevent>><</link>>
	<br>
	<<link [[Resist|Underground Cell Resist]]>><<set $molestationstart to 1>><<set $phase to 2>><</link>>
	<br>
<<elseif $undergroundtime is 12>>
	The lights turn on once more, and a <<generate1>><<person1>><<person>> enters. "Time for another performance," <<he>> says, yanking your leash.
	<br><br>
	<<link [[Go quietly|Underground Stage Intro]]>><<endevent>><</link>>
	<br>
	<<link [[Resist|Underground Cell Resist]]>><<set $molestationstart to 1>><<set $phase to 3>><</link>>
	<br>
<<elseif $undergroundtime is 16>>
	The lights turn on once more, and a <<generate1>><<person1>><<person>> enters. "Time for another performance," <<he>> says, yanking your leash.
	<br><br>
	<<link [[Go quietly|Underground Film Intro]]>><<endevent>><</link>>
	<br>
	<<link [[Resist|Underground Cell Resist]]>><<set $molestationstart to 1>><<set $phase to 4>><</link>>
	<br>
<<elseif $undergroundtime is 20>>
	The lights turn on once more, and a <<generate1>><<person1>><<person>> enters. "Time for some fresh air," <<he>> says, yanking your leash.
	<br><br>
	<<link [[Go quietly|Underground Hunt Intro]]>><<endevent>><</link>>
	<br>
	<<link [[Resist|Underground Cell Resist]]>><<set $molestationstart to 1>><<set $phase to 5>><</link>>
	<br>
<<elseif $undergroundtime is 24>><<set $undergroundtime to 0>>
	<<if $trauma gte $traumamax>>
		The lights turn on once more, and a <<generate1>><<generate2>><<person1>><<person>> and <<person2>><<person>> enter. The <<person1>><<person>> crouches beside you, grabs your neck and turns you to face <<himstop>> "Yep," <<he>> says. "<<pHer>> lights are out. Shame, <<pshes>> a pretty little thing."
		<br><br>
		Together they lift you by the shoulders and carry you from the room. They take you down a flight of steps to a large hole in the ground. They dump you in.
		<br><br>
		You fall through darkness for several seconds before splashing into cold water.<<water>>
		<br><br>
		<<endevent>>
		<<link [[Next|Underground Lake]]>><</link>>
	<<else>>
		The lights turn on once more, and a <<generate1>><<person1>><<person>> enters, holding a hose. Water shoots from the tip, knocking you prone and drenching you. <<He>> walks closer and continues spraying you from multiple angles, before finally turning it off. "There we go," <<he>> says. "Nice and clean." The door slams shut behind <<himcomma>> plunging you into darkness.
		<br><br>
		<<wash>><<water>>
		<<endevent>>
		<<link [[Next|Underground Cell]]>><</link>>
		<br>
	<</if>>
<<else>>
	<<set $danger to random(1, 10000)>><<set $dangerevent to 0>><<set $allure /= 2>>
	<<if $danger gte (9900 - $allure) and $eventskip is 0>>
		Dim pink-hued lights turn on, bathing the room in light. One of the doors opens and a <<generate1>><<person1>><<person>> walks in, closing the door behind <<himstop>> <<covered>> <<He>> sizes you up as <<he>> approaches.
		<<set $rng to random(1, 100)>>
		<<if $rng gte 81>>
			"I paid good money for you. You better be worth it."
			<<set $phase to 1>>
		<<elseif $rng gte 61>>
			"You're adorable. Don't worry, I'll be gentle."
		<<elseif $rng gte 41>>
			"Good enough. Hold Still."
		<<elseif $rng gte 21>>
			"You better be worth it <<bitchstop>>"
			<<set $phase to 1>>
		<<else>>
			"Yes, nice and young."
		<</if>>
		<br><br>
		<<link [[Next|Underground Cell Molestation]]>><<set $molestationstart to 1>><</link>>
		<br>
	<<else>>
		<<link [[Rest (1:00)|Underground Cell Rest]]>><<pass 1 hour>><<set $tiredness -= 1000>><</link>><<ltiredness>>
		<br>
		<<if $undergroundescape is 0>>
			<<link [[Look for a way out|Underground Cell Look]]>><</link>>
			<br>
		<<elseif $undergroundescape gte 19>>
			<<link [[Escape|Underground Escape]]>><</link>>
			<br>
		<<elseif $undergroundescape gte 18 and $physiquesize lte 12000>>
			<<link [[Escape|Underground Escape]]>><</link>><<small_text>>
			<br>
		<<elseif $undergroundescape gte 17 and $physiquesize lte 10000>>
			<<link [[Escape|Underground Escape]]>><</link>><<small_text>>
			<br>
		<<elseif $undergroundescape gte 16 and $physiquesize lte 6000>>
			<<link [[Escape|Underground Escape]]>><</link>><<small_text>>
			<br>
		<<else>>
			<<link [[Dig (1:00)|Underground Cell Dig]]>><<pass 1 hour>><</link>>
			<br>
		<</if>>
		<<link [[Examine the door|Underground Cell Lock]]>><</link>>
		<br>
	<</if>>
<</if>>