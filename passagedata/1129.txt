<<effects>>

You stare them down. They look at each other, trying to goad one another into making the first move. None of them are keen.
<br><br>

"Screw this," the <<person1>><<person>> says. "I need a drink." <<He>> turns and walks away. The others soon follow.
<br><br>

<<tearful>> you catch your breath.
<br><br>

<<clotheson>>
<<endcombat>>
<<link [[Next|Livestock Field]]>><</link>>
<br>