<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

You enter the username and password you discovered in the note and log into Leighton's computer. You find nothing at first, just boring spreadsheets, until you find a folder simply titled "D".
<br><br>

<<npc Darryl>><<person1>>It contains thousands of images of a <<if $pronoun is "m">>boy<<else>>girl<</if>> with black hair in various states of undress. <<He>> looks distressed in some of the pictures but <<his>> expression is blank in others. The pictures are arranged in order of date and appear to be from several years ago. Whatever was happening went on for many months.
<br><br>

Leighton makes an appearance in the later pictures, sometimes posing with <<himcomma>> but other times groping and fondling. From <<his>> outfit, it looks like <<he>> was a student here.
<br><br>

<<link [[Copy to flash drive|Head's Computer Flash]]>><</link>>
<br>