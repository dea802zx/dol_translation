<<set $outside to 1>><<set $location to "town">><<effects>>

You crouch behind a dumpster and wait for the voices to pass. Unfortunately, they decide to stop right in front of your hiding place. You peek out and see a <<generate1>><<generate2>><<person1>><<person>> and <<person2>><<personstop>> You stay as still as possible, not wanting to be caught in such a state. They eventually move on, allowing you to continue.
<br><br>

<<endevent>>
<<industrialeventend>>