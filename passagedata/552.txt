<<set $outside to 1>><<set $location to "lake">><<set $bus to "lakeshallows">><<water>><<effects>><<lakeeffects>>
<<if $laketeenspresent is 1>>
	You are swimming in the lake near the shore. The beach is alive with activity. People play in the sand and water.
<<else>>
	<<if $weather is "rain">>
		You are swimming in the lake near the shore. The rain drowns out the sounds of the forest.
	<<else>>
		You are swimming in the lake near the shore. There is no one at the beach today. The sound of the forest fills the air.
	<</if>>
<</if>>
<<if $schoolday isnot 1 and $weather is "rain" and $daystate is "day">>
	Someone is swimming further out, past the dock.
<</if>>
<<if $science gte 400 and !$plants_known.includes("lotus")>>
	Lotus flowers float along the shore.
<</if>>
<br><br>
<<if $laketeenspresent is 1 and $exposed gte 1>>

	<<if $exhibitionism gte 75>>
	You keep low beneath the water. You really wish to show <<lewdness>>, but you restrain yourself from doing so and probably should find something to cover yourself with.
	<<else>>
	You keep low beneath the water to keep your <<lewdness>> from being seen. You'll need to find something to cover with.
	<</if>>
	<br><br>
<</if>>
<<if $lakeswim is 1>>
	<<set $lakeswim to 0>>
	You practise swimming in the lake.
	<<if $daystate is "night">>
		<<if $weather is "rain">>
			The dark water dances with the rain. It's exhilarating, if scary.
		<<else>>
			The black water is gentle.
		<</if>>
	<<elseif $daystate is "dusk">>
		<<if $weather is "rain">>
			Rain agitates the cool water.
		<<else>>
			The falls over the trees, casting its double in the water.
		<</if>>
	<<elseif $daystate is "dawn">>
		<<if $weather is "rain">>
			Rain agitates the cool water.
		<<else>>
			The morning sun rises over the trees, casting its double in the water.
		<</if>>
	<<else>>
		<<if $weather is "rain">>
			Rain agitates the warm water.
		<<else>>
			The water is warm and gentle.
		<</if>>
	<</if>>
	<<physique 3>><<swimmingskilluse>>
	<br><br>
<</if>>
<<if $phase is 1>>
	<<set $phase to 0>>
	You improvise a garment made from reeds and lilies and tie it around your chest. It's fragile and revealing, and you don't think you could take it off without breaking it, but it's better than nothing.
	<<plantupper>>
	<br><br>
	<<if $rng gte 91 and $parasite.nipples.name is undefined and $parasitedisable is "f">>
		Your satisfaction is interrupted by a pinching sensation on your nipples. Shocked, you pull down your top to find the culprit. Two small and soft invertebrates have latched to your chest. They massage and suck your nipples as if trying to nurse. They don't hurt at all, but the sensations are arousing you. You tug them in a bid to be free of their molestation, but they cling too tightly. What's more, touching them with any amount of force makes them suck more intensely. You'll need help to remove them.
		<<parasite nipples urchin>><<garousal>><<arousal 600>>
		<br><br>
	<</if>>
<</if>>
<<if $phase is 2>>
	<<set $phase to 0>>
	You improvise a garment made from reeds and lilies and tie it around your waist. It's fragile and revealing, and you don't think you could take it off without breaking it, but it's better than nothing.
	<<plantlower>>
	<br><br>
	<<if $rng gte 91 and $parasite.penis.name is undefined and $penisexist is 1 and $parasitedisable is "f" and !$worn.genitals.type.includes("chastity")>>
		Your satisfaction is interrupted by a pinching sensation on your <<penisstop>> Shocked, you lift your skirt to find the culprit. A small and soft invertebrate has latched on. It massages and sucks as if trying to nurse. It doesn't hurt at all, but the sensations are making you feel shamefully aroused. You tug it in a bid to be free of its molestation, but it clings too tightly. What's more, touching it with any amount of force makes it suck more intensely. You'll need help to remove it.
		<<parasite penis urchin>><<garousal>><<arousal 600>>
		<br><br>
	<<elseif $rng gte 91 and $parasite.clit.name is undefined and $vaginaexist is 1 and $parasitedisable is "f" and !$worn.genitals.type.includes("chastity") and $penisexist isnot 1>>
		Your satisfaction is interrupted by a pinching sensation on your <<pussystop>> Shocked, you lift your skirt to find the culprit. A small and soft invertebrate has latched onto your clit. It massages and sucks as if trying to nurse. It doesn't hurt at all, but the sensations are making you feel shamefully aroused. You tug it in a bid to be free of its molestation, but it clings too tightly. What's more, touching it with any amount of force makes it suck more intensely. You'll need help to remove it.
		<<parasite clit urchin>><<garousal>><<arousal 600>>
		<br><br>
	<</if>>
<</if>>
<<set $danger to random(1, 10000)>><<set $dangerevent to 0>>
<<if $stress gte 10000>>
	<<passoutlake>>
<<elseif $danger gte (9900 - ($allure / 2)) and $eventskip is 0>>
	<<eventlakewater>>
<<else>>
	<<link [[Practise swimming (0:30)|Lake Shallows]]>><<pass 30>><<stress -6>><<tiredness 6>><<set $lakeswim to 1>><<slimeEventEnd>><</link>><<gswimming>><<lstress>><<gtiredness>>
	<br>
	<<if $worn.upper.exposed gte 2 and $worn.under_upper.exposed gte 1
	or $worn.upper.exposed gte 2 and $underupperwetstage gte 3
	or $upperwetstage gte 3 and $worn.under_upper.exposed gte 1
	or $upperwetstage gte 3 and $underupperwetstage gte 3>>
		<<link [[Make a top out of plants (0:20)|Lake Shallows]]>><<pass 20>><<set $phase to 1>><</link>>
		<br>
	<</if>>
	<<if $worn.lower.exposed gte 2 and $worn.under_lower.exposed gte 1
	or $worn.lower.exposed gte 2 and $underlowerwetstage gte 3
	or $lowerwetstage gte 3 and $worn.under_lower.exposed gte 1
	or $lowerwetstage gte 3 and $underlowerwetstage gte 3>>
		<<link [[Make a skirt out of plants (0:20)|Lake Shallows]]>><<pass 20>><<set $phase to 2>><</link>>
		<br>
	<</if>>
	<<if $science gte 400 and !$plants_known.includes("lotus")>>
		<<link [[Search for seeds (0:30)|Lake Lotus]]>><<set $plants_known.push("lotus")>><<pass 30>><<tending 6>><</link>><<gtending>>
		<br>
	<</if>>
	<<if $exposed gte 1 and $laketeenspresent is 1>>
		<br>
	<<elseif $dev is 1>>
		<<link [[Swim to dock (0:10)|Lake Dock]]>><<pass 10>><</link>>
		<br>
	<</if>>
	<<link [[Swim further out (0:12)|Lake Depths]]>><<pass 12>><</link>>
	<br>
	<<if $exposed gte 1 and $laketeenspresent is 1>>
	<<else>>
		<<link [[Shore (0:02)|Lake Shore]]>><<pass 2>><</link>>
		<br>
	<</if>>
	<<set $eventskip to 0>>
<</if>>