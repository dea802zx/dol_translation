<<set $outside to 0>><<set $location to "pub">><<effects>><<set $bus to "harvest">>
<<npc Landry>><<person1>>You ask the bartender for Landry, who nods and beckons you over to your usual table. You've barely sat down when Landry arrives and sits opposite you.
<br><br>
"What have you got for me?" <<he>> asks.
<br><br>
<<landryoptions>>
<<link [[Leave|Pub]]>><<endevent>><</link>>
<br>