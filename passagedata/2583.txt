<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $schoolstate is "third">>
	<<if $worn.upper.type.includes("naked") and $worn.lower.type.includes("naked") and $rightarm isnot "bound">>
		/*Check for when the pc is only wearing and overcoat*/
		<<if $worn.under_upper.name is "naked" and $worn.under_lower.name is "naked">>
			Wearing nothing underneath over slot clothing comment
		<<elseif $worn.under_upper.name is "naked">>
			Wearing only under_lower under over slot clothing comment
		<<elseif $worn.under_lower.name is "naked">>
			Wearing only under_upper under over slot clothing comment
		<</if>>
		<br><br>
		<<link [[Next|Hallways]]>><<endevent>><</link>>
		<br><br>
	<<elseif $worn.upper.type.includes("school") and $worn.lower.type.includes("school") and $rightarm isnot "bound">>
		<<if $englishattended is 1>>
			You rejoin the English lesson,
			<<if $worn.over_upper.name isnot "naked" or $worn.over_lower.name isnot "naked" or $worn.over_head.name isnot "naked">>
				 hang your coat at the back of the class
				<<undressOverClothes "englishClassroom">>
			<</if>>
			and take your seat.
			<br><br>
			<<englishstart>>
		<<elseif $time lte ($hour * 60 + 5)>>
			You enter the English classroom.
			<<npc Doren>><<person1>>
			Doren is preparing at the front of the room while the seats fill.
			<<if $worn.over_upper.name isnot "naked" or $worn.over_lower.name isnot "naked" or $worn.over_head.name isnot "naked">>
				<<undressOverClothes "englishClassroom">>
				You hang your coat at the back of the class and take your seat.
			<</if>>
			<br><br>
			<<englishstart>>
		<<else>>
			You
			<<if $worn.over_upper.name isnot "naked" or $worn.over_lower.name isnot "naked" or $worn.over_head.name isnot "naked">>
				hang your coat at the back of the class and
				<<undressOverClothes "englishClassroom">>
			<</if>>
			enter the English classroom.
			<<npc Doren>><<person1>>
			Doren cuts off mid-sentence and smiles at you. "Better late than never! Sit down, you've got lost time to make up for."
			<<gdelinquency>>
			<br><br>
			<<detention 1>>
			You take a seat as Doren continues.
			<br><br>

			<<englishstart>>
		<</if>>

	<<elseif $rightarm is "bound" or $leftarm is "bound" or $feetuse is "bound">>
		Doren looks concerned. "La<<if $player.gender_appearance is "m">>d<<else>>ss<</if>>, who tied you up like that? Are you okay? Leighton should have something have something to cut you free. Go see the head."
		<br><br>

		<<link [[Next|Hallways]]>><<endevent>><</link>>
		<br><br>
	<<else>>
		<<npc Doren>><<person1>>
		Doren laughs. "You can't attend my lesson without a uniform. Not my rules. Leighton might have something spare."
		<br><br>

		<<link [[Next|Hallways]]>><<endevent>><</link>>
		<br><br>
	<</if>>

<<elseif $schoollesson is 1>>
	<<if $englishinterrupted is 1>>
		<<npc Doren>><<person1>>You enter the English classroom. Doren stops speaking abruptly and looks your way. "Hah! I'm glad you're eager to study in my class, but don't you have another lesson to be at? I have to mark you down for detention or Leighton'll give me another talking to." <<He>> shoos you out the door.
		<<gdelinquency>>
		<br><br>
		<<detention 1>>

		<<link [[Next|Hallways]]>><<endevent>><</link>>
	<<else>>
		<<npc Doren>><<person1>>You enter the English classroom. Doren stops speaking abruptly and looks your way. "Something I can do for you, <<lass>>?" It seems you've interrupted the lesson.
		<br><br>
		<<set $englishinterrupted to 1>>

		<<link [[Apologise|English Classroom Apology]]>><<trauma 1>><<stress 1>><</link>><<gstress>><<gtrauma>>
		<br>
		<<if $trauma gte 1>>
			<<link [[Mock|English Classroom Mock]]>><<status 1>><<stress -12>><</link>><<lstress>><<gcool>><<gdelinquency>>
			<br>
		<</if>>
	<</if>>
<<else>>
	You are in the English classroom. Bookshelves line the walls.
	<br><br>
	<<storeon "englishClassroom" "check">>
	<<if _store_check is 1>>
		<<storeon "englishClassroom">>
		You take your coat at the back of the class.
		<br><br>
	<</if>>
	<<exhibitionclassroom>>

	<<link [[Leave the room (0:01)|Hallways]]>><<pass 1>><</link>>
	<br>
<</if>>