<<effects>>

You refuse the drink. Avery takes two when offered the tray, and turns to you.
<br><br>

"It's impolite to refuse to drink at a function." <<He>> whispers, offering one of the glasses. "At least take one to hold."
<br><br>

<<link [[Take|Avery Party Drink Take]]>><<npcincr Avery love 1>><</link>><<glove>>
<br>
<<link [[Refuse|Avery Party Drink Refuse 2]]>><<npcincr Avery love -1>><</link>><<llove>>
<br>