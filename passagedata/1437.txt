<<pass 30>><<set $outside to 1>><<set $location to "beach">><<effects>>

You walk along the shore.
<<if $daystate is "night">>
	<<if $weather is "rain">>
		A downfall of rain wets you as you stroll.
	<<elseif $weather is "clear">>
		The sound of the waves fills your senses.
	<<elseif $weather is "overcast">>
		The night is cool and relaxes you.
	<</if>>
<<else>>
	<<if $weather is "rain">>
		The pouring rain wets the sandy ground.
	<<elseif $weather is "clear">>
		The sun's heat is too intense and you begin to tire.
		<<tiredness 3>><<gtiredness>>
	<<elseif $weather is "overcast">>
		The cool weather makes for an enjoyable stroll.
	<</if>>
<</if>>
<<tiredness 3>><<stress -5>>
<br><br>

<<link [[Next|Beach]]>><</link>>