<<set $outside to 0>><<set $location to "park">><<effects>>
<<He>> frowns for a moment, but covers it with a smile. "Of course," <<he>> says. I'm sure you're busy. I'll see you around."
<br><br>

<<if $scienceproject is "ongoing">>
	<span class="gold">You can add the lichen you found to your project in your room or the school library.</span>
	<br><br>

	<<link [[Next|Park]]>><<endevent>><<set $eventskip to 1>><</link>>
	<br>
<<else>>
	<<endevent>>
	<<destinationeventend>>
<</if>>