<<set $outside to 0>><<set $location to "town">><<effects>>

"Can we get popcorn?" Robin asks as you buy the tickets. "It's not proper without popcorn."
<br><br>

<<if $robinromance is 1>>
<<set $dateCount.Total++>><<set $dateCount.Robin++>>

You enter the theatre. Robin picks seats near the back. "I hope it's good," <<he>> says. You don't have to wait long.
<br><br>

You find the film too slow-paced, but Robin is engrossed. <<He>>doesn't take <<his>>eyes off the screen.
<br><br>

<<link [[Just watch|Robin Cinema Romance Watch]]>><<pass 15>><</link>>
<br>
<<link [[Get closer|Robin Cinema Closer]]>><</link>>
<br>

<<else>>
You enter the theatre. Robin picks seats near the middle. "I hope it's good," <<he>> says. You don't have to wait long.
<br><br>

You find the film too slow-paced, but Robin is engrossed. <<He>> doesn't take <<his>> eyes off the screen. <<He>> applauds when the credits roll, and talks about the film on the way home.
<br><br>

<<link [[Flirt|Robin Cinema Flirt]]>><<pass 15>><<npcincr Robin love 1>><</link>><<glove>>
<br>
<<link [[Listen|Robin Cinema Listen]]>><<pass 15>><</link>>
<br>

<</if>>