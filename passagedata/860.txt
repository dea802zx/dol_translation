<<effects>>

<<if $submissive gte 1150>>
	"St-stop that!" you say. "Or I'll call the police!"
<<elseif $submissive lte 850>>
	"Put the axes down," you say. "Or you'll be shitting them out later."
<<else>>
	"This is illegal," you say. "Stop right now, or you'll be sorry,"
<</if>>
<br><br>
<<if $english gte random(400, 1200)>>
	The pair look at each other. The <<person2>><<person>> speaks first. "Remy did say-"
	<br>
	"Don't say their name," the <<person1>><<person>> warns, throwing a glance in your direction.
	<br>
	"The boss said not to be seen."
	<br>
	"Well we've fucked up that part. Fine." <<He>> glares you. "We're gone, but this isn't over."
	<br><br>
	
	The pair walk away, leaving the fence intact.<<llaggro>><<farm_aggro -10>>
	<br><br>
	
	<<link [[Next|Farm Work]]>><<endevent>><</link>>
	<br>
<<else>>
	The pair look at each other, <span class="red">and burst into laughter.</span> "Sure thing kid," the <<person1>><<person>> says. "We'll get out of your hair once we've finished our job." <span class="red">Their axes continue to bite into the fence.</span><<set $farm_work.fence_damage += 1>>
	<br><br>
	
	<<link [[Call Alex|Farm Axe Alex]]>><<farm_aggro 5>><<npc Alex dom 1>><<npc Alex love 1>><<set $farm_work.fence_damage += 1>><</link>><<gaggro>> | <span class="purple">+ Alex's Dominance</span><<glove>>
	<br>
	<<link [[Attack|Farm Axe Attack]]>><<def 1>><</link>>
	<br>
<</if>>