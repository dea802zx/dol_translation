<<set $outside to 0>><<set $location to "asylum">><<asylumeffects>><<effects>>
<<if $phase is 0>>
	<<set $phase to 1>>
	You press the button. The <<person2>><<person>> squirms. You notice <<his>> arms are bound to the side of the chair. "Thank you," Doctor Harper says. "
	<<if $NPCList[1].penis isnot "none">>
		We have a device attached to <<his>> penis.
	<<else>>
		We have a device inserted in <<his>> vagina.
	<</if>>
	To aid in <<his>> treatment." You spot wires trailing across the floor and disappearing into the <<persons>> clothes.
	<br><br>
	"Press it again," the Doctor says.
	<br><br>
	<<person1>>
	<<link [[Press|Asylum Button Press]]>><<awareness -1>><<stress 3>><<arousal 600>><<suspicion -1>><</link>><<lsuspicion>><<lawareness>><<gstress>><<garousal>>
	<br>
	<<link [[Refuse|Asylum Button Refuse]]>><<suspicion 1>><<def 1>><</link>><<gsuspicion>>
	<br>
<<elseif $phase is 1>>
	<<set $phase to 2>>
	<<person2>>
	You press the button. The <<person>> squirms more this time. <<He>> strains against the bonds. You hear buzzing through the glass. "Very good," the Doctor says. "Any moment now."
	<br><br>
	Almost on cue, the <<person>> spasms, before slumping back in the chair. The buzzing ceases. "<<He>> will learn to enjoy it," the Doctor says. "Press it again."
	<br><br>
	<<person1>>
	<<link [[Press|Asylum Button Press]]>><<awareness -10>><<stress 3>><<arousal 600>><<suspicion -1>><</link>><<lsuspicion>><<llawareness>><<gstress>><<garousal>>
	<br>
	<<link [[Refuse|Asylum Button Refuse]]>><<suspicion 1>><<def 1>><</link>><<gsuspicion>>
	<br>
<<else>>
	<<person2>>
	The <<persons>> eyes widen as the buzzing starts up again. Louder this time. <<He>> shakes <<his>> head and rocks back and forth, as if trying to knock the chair over. It's not long before <<he>> spasms again. And again. Even the counter in front of you vibrates.
	<br><br>
	<<person1>>Doctor Harper writes on <<his>> clipboard, a small smile on <<his>> face. "Thank you," <<he>> says. "You've been very helpful. That's all for now. Be well." <<person2>>The <<person>> falls limp as Harper finishes speaking, and the buzzing stops.
	<br><br>
	<<endevent>>
	<<link [[Next|Asylum]]>><</link>>
	<br>
<</if>>