<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $phase is 1>>
You break the seal and open the envelope. There's a note inside, with the head's computer username and password. That might be useful, but you'll probably be in trouble for tampering with the letter.
<<set $headpasswordknown to 1>>
<br><br>
<<endevent>>
<</if>>

You deliver the note without incident, but on the way back to class you are hounded by a group of delinquents.

<<if $cool lt ($coolmax / 10) and $mathsnote gte 2>>
<<generatey1>><<generatey2>><<generatey3>><<generatey4>>

What starts with verbal teasing soon becomes physical.
<br><br>

<<link [[Next|Maths Note Molestation]]>><<set $molestationstart to 1>><<set $phase to 2>><</link>>
<br>

<<elseif $cool lt ($coolmax / 5) and $mathsnote gte 1>><<set $mathsnote to 2>>
<<generatey1>><<generatey2>><<generatey3>><<generatey4>>

What starts with verbal teasing soon becomes physical.
<br><br>

<<link [[Next|Maths Note Molestation]]>><<set $molestationstart to 1>><<set $phase to 1>><</link>>
<br>

<<else>>
<<set $mathsnote to 1>>
They tease you a little but fortunately don't take things too far. You arrive at the classroom.
<<gtrauma>><<gstress>><<trauma 2>><<stress 2>>
<br><br>

<<link [[Next|Maths Lesson]]>><</link>>
<br>
<</if>>