<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>

<<if $submissive lte 850>>
	You stare a blank expression. Without warning, you smack your hand into the pile of books <<he>> is holding, sending them flying everywhere.
	<br><br>

	<<skulduggerycheck>>
	<<if $skulduggerysuccess is 1>>
		<br><br>
		The <<person>> is standing, stunned and surrounded by books, as Winter walks in.

		"What are you doing?" Winter glares at <<him>> from the door. "Pick those books up, right now."
		<br><br>
		"But-"
		<br><br>
		"Explain it to the head in detention later. Right now I want those books handed out."
		<br><br>
		Other students smile at you as the <<person>> scrambles around picking books off the floor.
		<<gcool>><<status 1>>
		You smile sweetly as <<he>> hands you your book.
		<<lstress>><<stress -1>>
		<br><br>

			<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
			<<skulduggeryskilluse>>
			<<else>>
			<span class="blue">That was too easy. You didn't learn anything.</span>
			<br><br>
			<</if>>

		<<link [[Next|History Lesson]]>><<endevent>><</link>>
		<br>

	<<else>>
		<br><br>
		"What are you doing?" Winter glares at you from the door. "Pick those up, right now."
		<br><br>
		"But-"
		<br><br>
		"You can explain it to the head in detention later."
		<<detention 2>><<stress 2>><<gstress>><<gdelinquency>>
		<br><br>

			<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
			<<skulduggeryskilluse>>
			<<else>>
			<span class="blue">That was too easy. You didn't learn anything.</span>
			<br><br>
			<</if>>

		<<link [[Next|History Lesson]]>><<endevent>><</link>>
		<br>
	<</if>>

<<else>>
	You point behind <<himstop>> "I think Leighton saw that."
	<br><br>

	<<skulduggerycheck>>
	<<if $skulduggerysuccess is 1>>

		<br><br>
		The <<persons>> face pales.
		<br>
		As <<he>> looks over <<his>> shoulder toward the door, you grab another book from the top of <<his>> pile. "Thanks."
		<br><br>
		<<Hes>> still gaping in shock as Winter walks back in.
		<br>
		"You dropped a book," Winter tuts at <<himstop>> "Do be more careful."
		<br>
		The class smirk and whisper as the <<person>> has to crouch down and pick the book back up.
		<<stress -2>><<lstress>>
		<br>

			<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
			<<skulduggeryskilluse>>
			<<else>>
			<span class="blue">That was too easy. You didn't learn anything.</span>
			<br><br>
			<</if>>

		<<link [[Next|History Lesson]]>><<endevent>><</link>>
		<br>

	<<else>>
		<br><br>
		The <<person>> scoffs. "You think I was born yesterday? You better pick up your book."
		<br>

			<<if $skulduggery lte ($skulduggerydifficulty + 100)>>
			<<skulduggeryskilluse>>
			<<else>>
			<span class="blue">That was too easy. You didn't learn anything.</span>
			<br><br>
			<</if>>

		<<link [[Pick it up|History Events Pick]]>><<trauma 2>><<stress 2>><<status -10>><</link>><<gtrauma>><<gstress>><<lcool>>
		<br>
		<<link [[Leave it|History Events Leave]]>><<stress 2>><<detention 2>><</link>><<gdelinquency>><<gstress>>
		<br>
	<</if>>

<</if>>