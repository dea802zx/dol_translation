<<set $outside to 0>><<set $location to "school">><<schooleffects>><<effects>>
<<if $submissive gte 1150>>
	"Th-they're a growth on my head, <<sirstop>> Like horns,"
<<elseif $submissive lte 850>>
	"I'm a sinner, <<sirstop>> So I grow horns,"
<<else>>
	"I have horns, <<sirstop>> They grow out of my head,"
<</if>>
you say, giving your horns a tug. "See?"
<br><br>

Winter raises <<his>> eyebrows. "How unusual," <<he>> says. You can't tell if <<he>> believes you. "Carry on with your work, please."
<br><br>
<<endevent>>
<<link [[Next|History Lesson]]>><</link>>
<br>