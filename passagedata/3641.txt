<<effects>>

The cool earth feels wonderful on your feet. You examine your soles. They're covered in soot, but don't look burnt at all.
<br><br>
"The flames don't hurt you," Jordan says. "They merely burn away impurity. You have failed the trial, but may try again next week."
<br><br>
The <<person2>><<monk>> smothers the coals with a bucket of sand as Jordan leads you back to the temple's main hall.
<br><br>
<<clotheson>>
<<endevent>>
<<link [[Next|Temple]]>><</link>>
<br>